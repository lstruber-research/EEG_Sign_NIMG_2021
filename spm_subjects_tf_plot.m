% close all;
Manip = 'enfants'; % ou 'enfants'
generalPowerFolderName = './../Power data hilbert/pooled_baseline/';
Type = 'premovement'; % ou postmovement
measure = 'power';
PowerUnit = 'percent';
FolderType = strcat('/',Type,'_',PowerUnit,'/');

rois_names = {'FP';'SM';'RPM';'LPM';'RM';'LM';'RSS';'LSS';'RP';'LP';'OP'};
rois_elecs = {[72,73,74,78,79,80,81,82,83,91,92,93,94,95,96];...% FP
    [75,76,77,84,85,86,88,89,90];...                            % SM
    [67,68,69,70,71,59,60,61,62];...                            % RPM
    [99,100,101,102,103,104,105,106,107];...                    % LPM
    [1,33,65,66,52,53,54,55,63,64,87];...                       % RM
    [1,97,98,108,109,110,111,114,115,116,87];...                % LM
    [1,2,3,33,34,48,49,50,51,52];...                            % RSS
    [1,2,3,110,111,112,113,122,123,124];...                     % LSS
    [4,19,20,21,30,31,32,35,36,37,38,42,43,44,45];...           % RP
    [4,19,20,21,5,6,7,8,9,17,18,125,126,127,128];...            % LP
    [10,11,12,14,15,16,22,23,24,27,28,29,39,40,41]};            % OP

bands_names = {'theta';'alpha';'beta';'gamma'};
bands_freqs = {4:8;...  % theta
    9:12;...            % alpha
    13:30;...           % beta
    31:80};             % gamma

if strcmp(Manip,'adultes')
	subjects = {'01AC1';'02MG1';'03CC1';'04NB1';'05NF1';'06LL1';'07TJ1';'08CD1';'09MA1';'10NM1';'11LP1';'12KN1';'13LD1';'14NG1';'15OB1';'16SA1';'17GC1';'18AC1';'19BM1'};
    subjects_names = {'S01';'S02';'S03';'S04';'S05';'S06';'S07';'S08';'S09';'S10';'S11';'S12';'S13';'S14';'S15';'S16';'S17';'S18';'S19'};
% 	subjects = {'01AC1'};
	conditions = {'F','RR','R1'};
    conditions_names = {'Norm','RdmRot','Adapt'};
%     conditions = {'R1'};
%     conditions_names = {'Adapt'};
    if strcmp(Type(1:4),'post')
        timewin = [-200 2500]; % Global : -1500:5:5500
        timevec = timewin(1):5:timewin(2);
        yvec = (timevec+1700)/5 + 1; % Attention: 1700 pour r_tf...
    else
        timewin = [-2000 700]; % Global : -5500:5:1500
        timevec = timewin(1):5:timewin(2);
        yvec = (timevec+5700)/5 + 1; % Attention: 5700 pour r_tf...
    end
else
    subjects = {'01FE1';'02BN1';'03AN1';'04MC1';'05JK1';'06GC1';'07EG1';'08HM1';'09ED1';'10DT1';'11VH1';'13MC1';'14TG1';'15LG1';'16SB1';'17JL1';'18LF1';'19EV1';'20LB1';'21EF1';'22LK1';'23LM1'};
    subjects_names = {'S01';'S02';'S03';'S04';'S05';'S06';'S07';'S08';'S09';'S10';'S11';'S12';'S13';'S14';'S15';'S16';'S17';'S18';'S19';'S20';'S21';'S22'};
% 	subjects = {'01FE1'};
	conditions = {'F','RR','R1'};
    conditions_names = {'Norm','RdmRot','Adapt'};
%     conditions = {'R1'};
%     conditions_names = {'Adapt'};
    if strcmp(Type(1:4),'post')
        timewin = [-200 2500]; % Global : -2500:5:5500
        timevec = timewin(1):5:timewin(2); 
        yvec = (timevec+2700)/5 + 1; % Attention: 2700 pour r_tf...
    else
        timewin = [-2000 700]; % Global : -5500:5:2500
        timevec = timewin(1):5:timewin(2);
        yvec = (timevec+5700)/5 + 1; % Attention: 5700 pour r_tf...
    end
end

for cond = 1:length(conditions)
    for sub = 1:length(subjects)
        if strcmp(measure,'power')
            TF_File = strcat(generalPowerFolderName, Manip, FolderType, conditions{cond}, '/c_av_r_tf_spmeeg_', subjects{sub},'-epo.mat');
        else
            TF_File = strcat(generalPowerFolderName, Manip, FolderType, conditions{cond}, '/c_av_r_tph_spmeeg_', subjects{sub},'-epo.mat');
        end
        D = spm_eeg_load(TF_File);
        
        for r = 1:size(rois_names)
            for b = 1:size(bands_names)
                if strcmp(Manip,'adultes')
                    if strcmp(Type(1:4),'post')
                        TF_Matrices.Adults.(conditions_names{cond}).(subjects_names{sub}).post.(measure).(rois_names{r}).(bands_names{b}) = squeeze(mean(D(rois_elecs{r},bands_freqs{b}-3,yvec,:),1));
                    else
                        TF_Matrices.Adults.(conditions_names{cond}).(subjects_names{sub}).pre.(measure).(rois_names{r}).(bands_names{b}) = squeeze(mean(D(rois_elecs{r},bands_freqs{b}-3,yvec,:),1));
                    end
                else
                    if strcmp(Type(1:4),'post')
                        TF_Matrices.Children.(conditions_names{cond}).(subjects_names{sub}).post.(measure).(rois_names{r}).(bands_names{b}) = squeeze(mean(D(rois_elecs{r},bands_freqs{b}-3,yvec,:),1));
                    else
                        TF_Matrices.Children.(conditions_names{cond}).(subjects_names{sub}).pre.(measure).(rois_names{r}).(bands_names{b}) = squeeze(mean(D(rois_elecs{r},bands_freqs{b}-3,yvec,:),1));
                    end
                end
            end
        end
%         figure();
%         imagesc(timewin,4:81,permute(D(1,:,:,1),[2,3,1,4])); % 113 = D17
%         axis normal xy;
%         colormap jet;
%         colorbar;
%         ylabel('frequency (in Hz)');
%         xlabel('time (in ms after mvt offset');
%         title(strcat(conditions{cond},': channel D17'));
%         caxis([-80 80]);
    end
end

% for cond =1:length(conditions)
%     for sub = 1:length(subjects)
%         Beta_File = strcat(generalPowerFolderName, Manip, FolderType, conditions{cond}, '/beta_c_av_r_tf_spmeeg_', subjects{sub},'-epo.mat');
%         D = spm_eeg_load(Beta_File);
%         figure();
%         plot(timevec,permute(D(113,yvec,1),[2,3,1])); % 113 = D17
%         ylabel('power changes (in %)');
%         xlabel('time (in ms after mvt offset');
%         title(strcat(conditions{cond},': channel D17'));
%     end
% end