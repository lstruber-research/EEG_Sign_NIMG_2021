# -*- coding: utf-8 -*-
"""
Created on Fri Feb 22 12:33:55 2019
@author: baumontm
power"""

from Preprocessing import *
import mne
import numpy as np
from scipy.linalg import logm


#%% 
subject = '01AC1'
condition = 'F'
#%%
raw = open_eeg_file(subject, condition, 'adult')
#%%
find_bad_channels_eeg(raw, plot=True)
#%%
raw = preprocess_eeg(subject, condition, raw, plot=True, bads='excel',
                save=False, resample=200, 
               interpolate_bads=True, manip='adult')

#%% 
ica = ica_blink_removal(raw, plot=True, n_components=40)

#%%
ica_plot_bads(raw, ica)

#%%
# raw = ica_apply(raw, ica, subject, condition, manip='adult')