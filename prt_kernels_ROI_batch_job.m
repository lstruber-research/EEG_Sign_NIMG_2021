%-----------------------------------------------------------------------
% Job saved on 15-Apr-2020 10:00:35 by cfg_util (rev $Rev: 7345 $)
% spm SPM - SPM12 (7771)
% cfg_basicio BasicIO - Unknown
% prt PRoNTo - Unknown
%-----------------------------------------------------------------------
global Manip Type BandsSimple BandsPAC Channels Files
 
matlabbatch{1}.prt.fs.infile = '<UNDEFINED>';
matlabbatch{1}.prt.fs.k_file = strcat(Manip,'_',Type,'_kernels');

for file = 1:length(Files)
	if(contains(Files{file},'pac')), Bands = BandsPAC;
	else, Bands = BandsSimple; end
    for band = 1:length(Bands)
        for chan = 1:length(Channels)
            matlabbatch{1}.prt.fs.modality(chan + length(Channels)*(band - 1) + length(Channels)*length(Bands)*(file - 1)).mod_name = strcat(Type, '_', Files{file},'_', Channels{chan},'_', Bands{band});
            matlabbatch{1}.prt.fs.modality(chan + length(Channels)*(band - 1) + length(Channels)*length(Bands)*(file - 1)).conditions.all_scans = 1;
            matlabbatch{1}.prt.fs.modality(chan + length(Channels)*(band - 1) + length(Channels)*length(Bands)*(file - 1)).voxels.all_voxels = 1;
            matlabbatch{1}.prt.fs.modality(chan + length(Channels)*(band - 1) + length(Channels)*length(Bands)*(file - 1)).detrend.no_dt = 1;
            matlabbatch{1}.prt.fs.modality(chan + length(Channels)*(band - 1) + length(Channels)*length(Bands)*(file - 1)).normalise.no_gms = 1;
            matlabbatch{1}.prt.fs.modality(chan + length(Channels)*(band - 1) + length(Channels)*length(Bands)*(file - 1)).atlasroi = {''};
        end
    end
end

matlabbatch{1}.prt.fs.flag_mm = 1;
