# -*- coding: utf-8 -*-
"""
Created on Fri Feb  1 10:34:57 2019

@author: baumontm
"""
import mne
import numpy as np
import xlrd
from mne.time_frequency import tfr_morlet, tfr_multitaper
import os.path
import os
from mne.preprocessing import create_eog_epochs, ICA


close_event = 0

def biosemi_prep(subject, condition, manip, trigger_change=True):
    ''' Read raw BDF file with 4 EOG channels, resample and change trigger
    subject (str)
    condition (str)
    resample(int or None)
    trigger_change (boolean)

    return:
    raw (mne.raw) raw file <ith biosemi 128 channels
    '''
    if manip == 'children':
        main_path = 'Manip_EEG_Enfants'
    elif manip == 'adult':
        main_path = 'Manip_EEG_2'
    raw_file = ('.\\..\\Raw data\\'+main_path+'\\'+subject+'\\'+subject+'_EEG\\'+subject+'-'
                +condition+'.bdf')
    montage = mne.channels.read_montage(kind='biosemi128')
    raw = mne.io.read_raw_bdf(raw_file, montage, ['EXG5', 'EXG6', 'EXG7',
                                                  'EXG8'],
                              stim_channel='status', preload=True)


    raw.set_channel_types(mapping={'EXG5':'eog', 'EXG6':'eog', 'EXG7':'eog',
                                   'EXG8':'eog'})
    mne.io.set_bipolar_reference(raw, ['EXG5', 'EXG8'], ['EXG6', 'EXG7'],
                                 ch_name=['HEOG', 'VEOG'], copy=False)
    list_channels = list()
    '''raw_np = np.zeros((len(raw.ch_names),raw.times.size))
    raw_np = raw[:,:][0]
    for i in range(32):
        raw_np[(i + 32),:] = raw[(i + 64),:][0]
        raw_np[(i + 64),:] = raw[(i + 32),:][0]
        raw_np[(i + 96),:] = raw[(i + 96),:][0]
        raw_np[(i),:] = raw[(i),:][0]
    raw[:,:] = raw_np'''

    letter = ('A', 'B', 'C', 'D')

    for i in letter:
        for j in range(32):
            list_channels.append(i+str(j+1))
    list_channels.append('HEOG')
    list_channels.append('VEOG')
    list_channels.append('Status')
    raw = raw.pick_channels(list_channels)

    if trigger_change is True:
        raw = change_trigger_values(raw)
    return raw

def change_trigger_values(raw):
    ''' Change trigger value of go cue and end of movement
    from 254/255 to 256/257
    raw (mne.raw) raw containing the trigger

    RETURN:
    raw (mne.raw) raw with modified trigger
    '''
    x, y = raw[130]
    i = 0
    trig2 = 0
    trigger = np.zeros((1, x.size), dtype=int)
    trigger[0][i] = int(x[0][i])
    while i < x.size-1:
        if int(x[0][i+1]) != int(x[0][i]):

            if int(x[0][i+1]) == 255:
                if trig2 == 0:
                    trigger[0][i+1] = 255
                    trig2 = 1
                elif trig2 == 1:
                    trigger[0][i+1] = 257
                    trig2 = 0
            elif int(x[0][i+1]) == 254:
                if int(x[0][i]) == 65790:
                    trigger[0][i+1] = 65790
                elif trig2 == 0:
                    trigger[0][i+1] = 254
                    trig2 = 0
                elif trig2 == 1:
                    trigger[0][i+1] = 256
                    trig2 = 1
        else:
            trigger[0][i+1] = trigger[0][i]
        i += 1
    raw[130] = trigger

    return raw

def change_trigger_values_pb(raw):
    ''' Change trigger value of go cue and end of movement
    from 254/255 to 256/257
    raw (mne.raw) raw containing the trigger
    RETURN:
    raw (mne.raw) raw with modified trigger
    '''
    x, y = raw[130]
    i = 0
    trig2 = 0
    trigger = np.zeros((1, x.size), dtype=int)
    trigger[0][i] = int(x[0][i])
    while i < x.size-1:
        if int(x[0][i+1]) != int(x[0][i]):

            if int(x[0][i+1]) == 255:
                if int(x[0][i]) == 65790:
                    trigger[0][i+1] = 65790
                if trig2 == 0:
                    trigger[0][i+1] = 255
                    trig2 = 1
                elif trig2 == 1:
                    trigger[0][i+1] = 257
                    trig2 = 0
            elif int(x[0][i+1]) == 254:
                if int(x[0][i]) == 65790:
                    trigger[0][i+1] = 65790
                elif trig2 == 0:
                    trigger[0][i+1] = 254
                    trig2 = 0
                elif trig2 == 1:
                    trigger[0][i+1] = 256
                    trig2 = 1
        else:
            trigger[0][i+1] = trigger[0][i]
        i += 1
    raw[130] = trigger

    return raw

def ica_blink_removal(raw, plot=False, picks=None, n_components=50):

    """ Calculate ICA components of raw file, find the EOG components
    raw(mne.raw): raw file, biosemi 128 channels
    plot: bool
    picks: None or list of channels
    n_components: int, default to 50, number of components to find
    RETURN:
    ica: the calculated ICA
    """
    method = 'infomax'
    decim = 5
    ica = ICA(n_components=n_components, method=method)
    if picks is not None:
        ica.fit(raw, picks, decim=decim)
    else:
        ica.fit(raw, decim=decim)
    eog_average = create_eog_epochs(raw).average()
    eog_epochs = create_eog_epochs(raw)  # get single EOG trials
    eog_inds, scores = ica.find_bads_eog(eog_epochs, threshold=3)

    if plot is not False:
        ica.plot_scores(scores, exclude=eog_inds)
        ica.plot_components()
        ica.plot_sources(raw)
        ica.plot_sources(eog_average, exclude=eog_inds)
        if len(eog_inds) != 0:
            ica.plot_properties(raw, picks=eog_inds)
    return ica

def ica_plot_bads(raw, ica):
    '''Plots the selected ICA components
    INPUT:
    raw (mne.raw) raw file, biosemi 128 channels
    ica: ica component of raw file
    '''
    picks = ica.exclude
    ica.plot_properties(raw, picks=picks)


def ica_apply(raw, ica, subject, condition, manip='children'):
    ''' Apply ICA exclusion from list of ICAs written by the user
    (separated by commas)
    INPUT:
    raw (mne.raw): raw file, biosemi 128 channels
    ica: ica component of raw file
    subject: 
    condition: 
    manip: (str) children or adult
    OUTPUT:
    raw: raw with bad ICAs removed
    '''
    picks = input('Picks (separated by commas): ')
    list_picks = list(map(int, picks.split(',')))
    ica.exclude = list_picks
    ica.plot_overlay(raw)
    ica.apply(raw)
    if manip == 'children':
        file_path = os.path.join('.\\..\\Preprocessed data\\processed-raw-enfants','')
    else:
        file_path = '.\\..\\Preprocessed data\\processed-raw\\processed_raw_'
    raw.save(file_path+subject+'-'+condition+
             '.fif', overwrite=True)
    return raw





def find_bad_channels_eeg(or_raw, plot=True):
    ''' Plots figures to help find bad channels in EEG raw data(to do after
    opening the EEG file and before preprocessing)
    INPUT:
    raw: (mne.raw) raw element of subject, not preprocessed
    plot: (bool)
    '''
    raw = or_raw.copy()
    raw.plot_psd(spatial_colors=True, fmax=80)
    raw = filter_eeg(raw)
    raw.plot()
    ica_blink_removal(raw, plot=plot)


def open_eeg_file(subject, condition, manip='adults'):
    ''' Opens EEG fif file if it exists or opens the original BDF file, changes
    the trigger and delete useless channels (not EEG or EOG) from file
    INPUT:
    subject: (str) code of subject
    condition: (str) F,R1,RR,wR1 (R2,wR2,RJ)
    manip: (str) 'children' or 'adult'
    OUTPUT:
    raw: (mne.raw) raw element of subject 
    '''
    if manip=='children':
        file_path = os.path.join('.\\..\\Preprocessed data\\processed-raw-enfants\\','')
        fname = os.path.join(file_path, subject+'-'+
                         condition+'.fif')
    else:
        file_path = '.\\..\\Preprocessed data\\processed-raw\\processed_raw_'
        fname = file_path+subject+'-'+condition+'.fif_test'
 
    if os.path.isfile(fname):
        print('The processed file already exist')
        raw = mne.io.read_raw_fif(fname, preload=True)
    else:
        raw = biosemi_prep(subject, condition, manip)
    return raw

def open_eeg_fif_file(subject, condition, manip='adults'):
    ''' Opens EEG fif file if it exists or opens the original BDF file, changes
    the trigger and delete useless channels (not EEG or EOG) from file
    INPUT:
    subject: (str) code of subject
    condition: (str) F,R1,RR,wR1 (R2,wR2,RJ)
    manip: (str) 'children' or 'adult'
    OUTPUT:
    raw: (mne.raw) raw element of subject 
    '''
    if manip=='children':
        file_path = os.path.join('.\\..\\Preprocessed data\\processed-raw-enfants\\','')
        fname = os.path.join(file_path, subject+'-'+
                         condition+'.fif')
    else:
        file_path = '.\\..\\Preprocessed data\\processed-raw\\processed_raw_'
        fname = file_path+subject+'-'+condition+'.fif'
 
    if os.path.isfile(fname):
        print('The processed file already exist')
        raw = mne.io.read_raw_fif(fname, preload=True)
    else:
        raw = []
    return raw


def preprocess_eeg(subject, condition, raw, plot=True, bads='excel', 
                   save=True,resample=200, interpolate_bads=False,
                   manip='children'):
    ''' Preprocessing of EEG Biosemi data: rereference, interpolation, filter
    INPUT: 
    subject: (str) code of subject
    condition: (str) F,R1,RR,wR1 (R2,wR2,RJ)
    raw: (mne.raw) raw element of subject
    plot: (bool)
    bads: (list) or 'excel', if excel go fing bad channels inside the excel sheet
    save: (bool), if True save the processed file
    resample: (int), resample the EEG data to lower frequency in Hz
    interpolate_bads: (bool)
    manip: (str) 'children' or 'adult'
    RETURN:
    raw: (mne.raw) preprocessed file
    '''
    if resample is not None:
        raw.resample(resample, npad="auto")
    if manip=='children':
        file_path = os.path.join('.\\..\\Preprocessed data\\processed-raw-enfants','')
    else:
        file_path = '.\\..\\Preprocessed data\\processed-raw\\processed_raw_'
    if bads == 'excel':
        bads = read_bads_excel(subject, manip)
    raw.info['bads'] = bads
    raw.set_eeg_reference(ref_channels='average', projection=False)
    raw = filter_eeg(raw)

    if interpolate_bads is True:
        raw.interpolate_bads(reset_bads=True)

    if plot is True:
        raw.plot()
        raw.plot_psd(spatial_colors=True, fmax=80)
    if save is True:
        raw.save(os.path.join(file_path, subject+'-'+
                              condition+'.fif'), overwrite=False)
    return raw



def filter_eeg(raw):
    ''' Filters the EEG elements (1-80 + notch)
    INPUT:
    raw: (mne.raw) raw element of subject
    OUTPUT:
    raw: filtered
    '''

    if raw.info['bads'] is not None:
        picks = mne.pick_types(raw.info, meg=False, eeg=True, eog=False,
                               stim=False, exclude='bads')
    else:
        picks = mne.pick_types(raw.info, meg=False, eeg=True, eog=False,
                               stim=False)
    picks_eog = mne.pick_types(raw.info, meg=False, eeg=False, eog=True,
                               stim=False)
    raw.filter(1, None, picks=picks_eog)
    raw.notch_filter(np.arange(50, 80, 50), picks=picks, filter_length='auto',
                     phase='zero')
    raw.filter(1, 80., fir_design='firwin', picks=picks)
    return raw


def epoch_creation(raw, event_id, tmin, tmax, reject=dict(eeg=5e-5), 
                   channels=None, bad_epochs='None', plot=False):
    '''Create epochs from raw processed file
    INPUT:
    raw: (mne.raw) processed file
    event_id: (int) 254 for post movement or 257 for pre movement
    tmin: (float) tmin to start the epoch
    tmax: (float) tmax to start the epoch
    reject: (dict) if bad_epochs is reject we reject epochs that have values 
    higher than the reject values
    bad_epochs: (list,'None' or 'reject'), if list deletes epochs from the list
    if 'None' no epoch rejections, if 'reject' delete those higher than a value
    plot: (bool) plot epochs and images to find bad epochs
    OUTPUT:
    epochs: (mne.epochs)    
    '''
    events = mne.find_events(raw, consecutive=True, min_duration=0.5)
    baseline = None
    if channels != None:
        raw.pick_channels(channels)
    if bad_epochs != 'reject':
        reject = None
    
    epochs = mne.Epochs(raw, events=events, event_id=event_id, tmin=tmin,
                        tmax=tmax, baseline=baseline, picks = ['eeg'], reject=reject, detrend = 1,
                        reject_by_annotation=False, preload=True)
    
    toDrop = []
    if bad_epochs == 'auto':                    
        epoch_data = epochs.get_data()  
        epochs_temp = epochs.copy()
        epoch_data_temp = epochs_temp.get_data()         
        # break detection 
        for i in range(0,len(epoch_data_temp[:,0,0])):
            for j in range(0,len(epoch_data_temp[0,0:128,0])):
                std = []
                for t in epochs_temp.times:
                    t_start = np.max([tmin,t-0.1])
                    t_end = np.min([tmax,t+0.1])
                    epochs_subset = epoch_data_temp[i,j,(epochs_temp.times>t_start) & (epochs_temp.times<t_end)]
                    std.append(np.std(epochs_subset))  
                    
                if np.max(std) > 5*np.mean(std):
                    toDrop.append(i)
                    break
        epochs_temp.drop(toDrop,reason = 'break_detected')
        print("Dropped epochs (break): " + str(toDrop).strip('[]'))
        epoch_data_temp = epochs_temp.get_data()
        
        # noise detection
        for elec_group in [range(0,32),range(32,64),range(64,96),range(96,128)]:
            std = []
            for j in elec_group:
                for i in range(0,len(epoch_data_temp[:,0,0])):
                    std.append(np.std(epoch_data_temp[i,j,:]))
                
                
            meanStd = np.mean(std)
            stdStd = np.std(std)
            for j in elec_group:
                for i in range(0,len(epoch_data[:,0,0])):
                    if(np.std(epoch_data[i,j,:]) > meanStd + 6*stdStd):
                        if i not in toDrop: toDrop.append(i)   
        
        epochs_temp = epochs.copy()
        epochs_temp.drop(toDrop,reason = 'noisy_epoch')
        print("Dropped epochs (break + noise): " + str(toDrop).strip('[]'))
        epoch_data_temp = epochs_temp.get_data()
        
        # difference detection between epochs
        mean_epoch_data = []
        std_epoch_data = []
        
        for j in range(0,len(epoch_data_temp[0,0:128,0])):
            mean_epoch_data.append([])
            std_epoch_data.append([])
            for i in range(0,len(epoch_data_temp[:,0,0])):
                epoch_data_temp[i,j,:] = epoch_data_temp[i,j,:] - np.mean(epoch_data_temp[i,j,:])
            for t in range(0,len(epoch_data[0,0,:])):
                mean_epoch_data[-1].append(np.mean(epoch_data_temp[:,j,t]))
                std_epoch_data[-1].append(np.std(epoch_data_temp[:,j,t]))
             
        for i in range(0,len(epoch_data[:,0,0])):
            dropped = False
            for j in range(0,len(epoch_data[0,0:128,0])):
                for t in range(0,len(epoch_data[0,0,:])):
                    if np.abs(epoch_data[i][j][t]-mean_epoch_data[j][t]) > 6*std_epoch_data[j][t]:
                        if i not in toDrop: toDrop.append(i)
                        dropped = True
                        break
                if dropped : break
                    
        epochs.drop(toDrop,reason = 'bad_epoch')
        print("Dropped epochs (all): " + str(toDrop).strip('[]'))
    elif isinstance(bad_epochs,list):
        print("Dropped epochs (all): " + str(bad_epochs).strip('[]'))
        if(len(bad_epochs) > 0):
            while(bad_epochs[len(bad_epochs) - 1] > len(epochs) - 1):
                bad_epochs.pop()
        epochs.drop(bad_epochs,reason = 'bad_epoch')
    if plot:
        fig = epochs.plot(n_epochs = 4, n_channels = 32)
        if fig.number == 1:
            fig.canvas.mpl_connect('close_event', handle_close)
#        epochs.plot_image()
#        epochs.average().plot_image()
    return epochs, toDrop

def epoch_creation_new(raw, event_id, tmin, tmax, 
                   channels=None, bad_epochs_eeg='None', bad_epochs_kin='None', plot=False):
    '''Create epochs from raw processed file
    INPUT:
    raw: (mne.raw) processed file
    event_id: (int) 254 for post movement or 257 for pre movement
    tmin: (float) tmin to start the epoch
    tmax: (float) tmax to start the epoch
    reject: (dict) if bad_epochs is reject we reject epochs that have values 
    higher than the reject values
    bad_epochs: (list,'None' or 'reject'), if list deletes epochs from the list
    if 'None' no epoch rejections, if 'reject' delete those higher than a value
    plot: (bool) plot epochs and images to find bad epochs
    OUTPUT:
    epochs: (mne.epochs)    
    '''
        
    events = mne.find_events(raw, consecutive=True, min_duration=0.5)
    baseline = None
    if channels != None:
        raw.pick_channels(channels)
    
    epochs = mne.Epochs(raw, events=events, event_id=event_id, tmin=tmin,
                        tmax=tmax, baseline=baseline, picks = ['eeg'], reject=None, detrend = 1,
                        reject_by_annotation=False, preload=True)
    
    bad_epochs = [];
    if isinstance(bad_epochs_eeg,list):
        if(len(bad_epochs_eeg) > 0):
            while(bad_epochs_eeg[len(bad_epochs_eeg) - 1] > len(epochs) - 1):
                bad_epochs_eeg.pop()
    elif isinstance(bad_epochs_eeg,int):
        bad_epochs_eeg = [bad_epochs_eeg]
    else :
        bad_epochs_eeg = []
            
        
    if isinstance(bad_epochs_kin,list):
        if(len(bad_epochs_kin) > 0):
            if event_id == 257:
                if epochs.drop_log[2]:
                    bad_epochs_kin = [x - 1 for x in bad_epochs_kin]
            while(bad_epochs_kin[len(bad_epochs_kin) - 1] > len(epochs) - 1):
                bad_epochs_kin.pop()
    elif isinstance(bad_epochs_kin,int):
        bad_epochs_kin = [bad_epochs_kin]
    else :
        bad_epochs_kin = []
        
    bad_epochs = bad_epochs_eeg + bad_epochs_kin;   
        
    bad_epochs = list(set(bad_epochs))
    bad_epochs.sort();
    
    print("Dropped epochs (all): " + str(bad_epochs).strip('[]'))
    epochs.drop(bad_epochs,reason = 'bad_epoch')
        
    if plot:
        fig = epochs.plot(n_epochs = 4, n_channels = 32)
        if fig.number == 1:
            fig.canvas.mpl_connect('close_event', handle_close)
#        epochs.plot_image()
#        epochs.average().plot_image()
    return epochs, bad_epochs

def power_creation(epochs, freqs, event_id, folderName, fileName, tfr='morlet', bad_epochs='None', save=False, plot=False):
    ''' Creates power from epochs and saves it, writes in comment the selection
    of epochs if not all trials are used
    INPUT:
    epochs: (mne.epochs)
    subject: (str) code of subject
    condition: (str) F,R1,RR,wR1 (R2,wR2,RJ)
    event_id: (int) 254 for post movement or 257 for pre movement
    tmin: (float) tmin to start the epoch
    tmax: (float) tmax to start the epoch
    tfr: (str) 'morlet or multitaper
    bad_epochs: (str) None
    OUTPUT:
    power: (mne.EpochsTFR)
    '''
    epochs.drop_bad(reject='existing')
    # ratio f0/sigmaF = 7 in Torecillos --> n_cylces == f0/sigmaF ???
    n_cycles = freqs/2
    if tfr == 'morlet':
        power = tfr_morlet(epochs, freqs=freqs, n_cycles=n_cycles,
                           use_fft=False, return_itc=False,
                           average=True, zero_mean=False)
    elif tfr == 'multitaper':
        power = tfr_multitaper(epochs, freqs=freqs, n_cycles=n_cycles,
                              use_fft=False, return_itc=False,
                              time_bandwidth=3.0, average=True)
    selection = selection_epochs(event_id, epochs)
    power.comment = selection
    if(plot): power.plot_topo(baseline=(None,None),mode='logratio')
    
   
    if save == True:
        if not os.path.isdir(folderName):
            os.makedirs(folderName)
        power.save(os.path.join(folderName, fileName), overwrite=True)
     
    return power

def power_averaged_and_normalized(power, band, baseline=(None,None), mode='mean'):
    # normalization with respect to the average power computed across all trials
    # Torecillos : log transformation before normalization ?
    AveragePower = power.average()
    AveragePower.apply_baseline(baseline=baseline, mode=mode)
    
    # delete 200ms before and after
    # mean of power across frequencies
    
    # indicate group/condition/band... in data
    # save as... nii ? or mat ? or vec ?
    return 
    
def selection_epochs(event_id, epochs):
    ''' Converts selection of epochs to return the actual trials present in 
    the epoch file 
    INPUT: 
    event_id: (int) 254 for post movement or 257 for pre movement
    epochs: (mne.epochs) 
    OUTPUT:
    selection: (list) liste of trials in the epochs file
    '''
    selection = epochs.selection
    if event_id == 254:
        selection += 1
        selection = selection/4 - 1
    elif event_id == 257:
        selection += 2
        selection = selection/4 - 1
    return selection

def read_bads_excel(subject, manip= 'children'):
    ''' Reads bad channels from a subject in an excel file 
    subject: (str) code of subject
    manip: (str) 'children' or 'adult'
    OUTPUT:
    list_bads: (list), list with bad channels
    '''

    if manip=='children':
        excel_path = 'Manip_EEG_Enfants_bad_channels.xlsx'
    else:
        excel_path = 'Manip_EEG_2_bad_channels.xlsx'
    workbook = xlrd.open_workbook(excel_path)
    worksheet = workbook.sheet_by_name("Feuil1")
    num_cols = worksheet.ncols
    curr_row = int(subject[:2])
    for curr_col in range(0, num_cols, 1):
        if worksheet.cell_value(0, curr_col) == 'Bads':
            if worksheet.cell_value(curr_row, curr_col) != '':
                bads = worksheet.cell_value(curr_row, curr_col)
                list_bads = bads.split(",")
                print("Bad channels : " + bads)
                return list_bads
            else:
                return []


def read_bad_epochs_old(subject, condition, name_bad_epochs,
                    file):
    ''' Read bad epochs from cinematic file of the subject/condition
    INPUT:
    subject: (str) code of subject
    condition: (str) F,R1,RR,wR1 (R2,wR2,RJ)
    name_bad_epochs: (str) name of the column, 'Bad Epochs'
    OUTPUT:
    bad_epochs: (list) list of marked bad epochs
    
    '''
    path_name = file + '\\' + subject + '\\'
    if condition in ['RJ', 'J', 'RR', 'R1']:
        if condition == 'R1':
            condition = 'R'
        excel_name = path_name + condition  + '_1_' + subject +'.xlsx'
        nb_epochs = 80
    elif condition in ['F', 'R2']:
        if condition == 'R2':
            condition = 'R'
        excel_name = path_name + condition + '_2_' + subject + '.xlsx'
        nb_epochs = 80
    elif condition == 'wR1':
        excel_name = file + '\\' + subject + '\\wR1_' + subject + '.xlsx'
        nb_epochs = 40
    elif condition == 'wR2':
        excel_name = file + '\\' + subject + '\\wR2_' + subject + '.xlsx'
        nb_epochs = 40
    workbook = xlrd.open_workbook(excel_name)
    worksheet = workbook.sheet_by_name("Feuil1")
    num_cols = worksheet.ncols
    bad_epochs = list()
    for curr_col in range(0, num_cols, 1):
        if worksheet.cell_value(0, curr_col) == name_bad_epochs:
            print(1)
            for curr_row in range(1, nb_epochs+1,1):
                if worksheet.cell_value(curr_row, curr_col) != 0:
                    bad_epochs.append(int(worksheet.cell_value(
                        curr_row, curr_col)))
    return bad_epochs

def read_bad_epochs_Marie(subject, condition, file):
    ''' Read bad epochs from cinematic file of the subject/condition
    INPUT:
    subject: (str) code of subject
    condition: (str) F,R1,RR,wR1 (R2,wR2,RJ)
    name_bad_epochs: (str) name of the column, 'Bad Epochs'
    OUTPUT:
    bad_epochs: (list) list of marked bad epochs
    
    '''
    workbook = xlrd.open_workbook(file)
    worksheet = workbook.sheet_by_name("Feuil1")
    num_cols = worksheet.ncols
    curr_row = int(subject[:2])
    for curr_col in range(0, num_cols, 1):
        if worksheet.cell_value(0, curr_col) == condition:
            if worksheet.cell_value(curr_row, curr_col) != '':
                bads = worksheet.cell_value(curr_row, curr_col)
                if isinstance(bads, float):
                    list_bads = bads
                else:
                    list_bads = list(map(int, bads.split(",")))
                return list_bads
            else:
                return []
            
def read_bad_epochs(subject, condition, file, event_id):
    ''' Read bad epochs from cinematic file of the subject/condition
    INPUT:
    subject: (str) code of subject
    condition: (str) F,R1,RR,wR1 (R2,wR2,RJ)
    name_bad_epochs: (str) name of the column, 'Bad Epochs'
    OUTPUT:
    bad_epochs: (list) list of marked bad epochs
    
    '''
    workbook = xlrd.open_workbook(file)
    if event_id == 257:
        worksheet = workbook.sheet_by_name("PRE")
    elif event_id == 254:
        worksheet = workbook.sheet_by_name("POST")
    else:
        worksheet = workbook.sheet_by_name("Feuil1")
    num_cols = worksheet.ncols
    curr_row = int(subject[:2])
    for curr_col in range(0, num_cols, 1):
        if worksheet.cell_value(0, curr_col) == condition:
            if worksheet.cell_value(curr_row, curr_col) != '':
                bads = worksheet.cell_value(curr_row, curr_col)
                if isinstance(bads, float):
                    list_bads_eeg = int(bads - 1)
                else:
                    list_bads_eeg = list(map(int, bads.split(",")))
                    list_bads_eeg = [x - 1 for x in list_bads_eeg]
            else:
                list_bads_eeg = []
    
    worksheet = workbook.sheet_by_name("KIN")
    num_cols = worksheet.ncols
    curr_row = int(subject[:2])
    for curr_col in range(0, num_cols, 1):
        if worksheet.cell_value(0, curr_col) == condition:
            if worksheet.cell_value(curr_row, curr_col) != '':
                bads = worksheet.cell_value(curr_row, curr_col)
                if isinstance(bads, float):
                    list_bads_kin = int(bads - 1)
                else:
                    list_bads_kin = list(map(int, bads.split(",")))
                    list_bads_kin = [x - 1 for x in list_bads_kin]
            else:
                list_bads_kin = []
    
    return list_bads_eeg, list_bads_kin


def handle_close(evt):
    global close_event
    close_event = 1
    print("Closed !")