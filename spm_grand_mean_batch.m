% List of open inputs
% Grandmean: File Names - cfg_files
% Grandmean: Output filename - cfg_entry
Manip_all={'adultes';'enfants'};
Type_all = {'postmovement';'postmovement'};
generalPowerFolderName = './../../../Processed_data/Power data hilbert/pooled_baseline/';
Power_units = {'percent'}; % ou dB

for m = 1:size(Manip_all,1)
    for p = 1:size(Power_units,1)
        PowerUnit = Power_units{p};
        for t = 1:size(Type_all,1)
            Manip = Manip_all{m};
            Type = Type_all{t};
            FolderType = strcat('/',Type,'_',PowerUnit,'/');

            if strcmp(Manip,'adultes')
                subjects = {'01AC1';'02MG1';'03CC1';'04NB1';'05NF1';'06LL1';'07TJ1';'08CD1';'09MA1';'10NM1';'11LP1';'12KN1';'13LD1';'14NG1';'15OB1';'16SA1';'17GC1';'18AC1';'19BM1'};
                conditions = {'F';'RR';'RJ';'R1';'R2'};
%                 conditions = {'RR';'R1'};
            else
                subjects = {'01FE1';'02BN1';'03AN1';'04MC1';'05JK1';'07EG1';'08HM1';'09ED1';'10DT1';'13MC1';'14TG1';'16SB1';'17JL1';'18LF1';'19EV1';'20LB1';'21EF1';'22LK1';'23LM1'};
                conditions = {'F';'R1';'RR'};
            end

            for cond =1:length(conditions)
                nMeans = 5; % enter the number of runs here
                jobfile = {'spm_grand_mean_batch_job.m'};
                jobs = repmat(jobfile, 1, nMeans);
                inputs = cell(2, nMeans);

                Files = cell(length(subjects),nMeans);
                PowerFolderName = strcat(generalPowerFolderName, Manip, FolderType, conditions{cond}, '/');
                for sub = 1:length(subjects)
                    Files{sub,1} = strcat(PowerFolderName, 'c_av_r_tf_spmeeg_', subjects{sub}, '-epo.mat');
                    Files{sub,2} = strcat(PowerFolderName, 'theta_c_av_r_tf_spmeeg_', subjects{sub}, '-epo.mat');
                    Files{sub,3} = strcat(PowerFolderName, 'beta_c_av_r_tf_spmeeg_', subjects{sub}, '-epo.mat');
                    Files{sub,4} = strcat(PowerFolderName, 'alpha_c_av_r_tf_spmeeg_', subjects{sub}, '-epo.mat');
                    Files{sub,5} = strcat(PowerFolderName, 'gamma_c_av_r_tf_spmeeg_', subjects{sub}, '-epo.mat');
%                     Files{sub,6} = strcat(PowerFolderName, 'c_av_r_tph_spmeeg_', subjects{sub}, '-epo.mat');
%                     Files{sub,7} = strcat(PowerFolderName, 'theta_c_av_r_tph_spmeeg_', subjects{sub}, '-epo.mat');
%                     Files{sub,8} = strcat(PowerFolderName, 'beta_c_av_r_tph_spmeeg_', subjects{sub}, '-epo.mat');
%                     Files{sub,9} = strcat(PowerFolderName, 'alpha_c_av_r_tph_spmeeg_', subjects{sub}, '-epo.mat');
%                     Files{sub,10} = strcat(PowerFolderName, 'gamma_c_av_r_tph_spmeeg_', subjects{sub}, '-epo.mat');
    % 				Files{sub,11} = strcat(PowerFolderName, 'theta_beta_pac_spmeeg_', subjects{sub}, '-epo.mat');
    %                 Files{sub,12} = strcat(PowerFolderName, 'theta_gamma_pac_spmeeg_', subjects{sub}, '-epo.mat');
    %                 Files{sub,13} = strcat(PowerFolderName, 'alpha_beta_pac_spmeeg_', subjects{sub}, '-epo.mat');
    %                 Files{sub,14} = strcat(PowerFolderName, 'alpha_gamma_pac_spmeeg_', subjects{sub}, '-epo.mat');                
    % 				Files{sub,1} = strcat(PowerFolderName, 'theta_beta_tpac_spmeeg_', subjects{sub}, '-epo.mat');
    %                 Files{sub,2} = strcat(PowerFolderName, 'theta_gamma_tpac_spmeeg_', subjects{sub}, '-epo.mat');
    %                 Files{sub,17} = strcat(PowerFolderName, 'alpha_beta_tpac_spmeeg_', subjects{sub}, '-epo.mat');
    %                 Files{sub,18} = strcat(PowerFolderName, 'alpha_gamma_tpac_spmeeg_', subjects{sub}, '-epo.mat');
                end

                OutputFiles = cell(1,nMeans);
                OutputFiles{1,1} = strcat('_',FolderType(2:end-1),'_',conditions{cond},'_AverageGlobalTF');
                OutputFiles{1,2} = strcat('_',FolderType(2:end-1),'_',conditions{cond},'_AverageThetaPower');
                OutputFiles{1,3} = strcat('_',FolderType(2:end-1),'_',conditions{cond},'_AverageBetaPower');
                OutputFiles{1,4} = strcat('_',FolderType(2:end-1),'_',conditions{cond},'_AverageAlphaPower');
                OutputFiles{1,5} = strcat('_',FolderType(2:end-1),'_',conditions{cond},'_AverageGammaPower');
%                 OutputFiles{1,6} = strcat('_',FolderType(2:end-1),'_',conditions{cond},'_AverageGlobalTPH');
%                 OutputFiles{1,7} = strcat('_',FolderType(2:end-1),'_',conditions{cond},'_AverageThetaPhase');
%                 OutputFiles{1,8} = strcat('_',FolderType(2:end-1),'_',conditions{cond},'_AverageBetaPhase');
%                 OutputFiles{1,9} = strcat('_',FolderType(2:end-1),'_',conditions{cond},'_AverageAlphaPhase');
%                 OutputFiles{1,10} = strcat('_',FolderType(2:end-1),'_',conditions{cond},'_AverageGammaPhase');
    % 			OutputFiles{1,11} = strcat('_',FolderType(2:end-1),'_',conditions{cond},'_AverageThetaBetaPAC');
    %             OutputFiles{1,12} = strcat('_',FolderType(2:end-1),'_',conditions{cond},'_AverageThetaGammaPAC');
    %             OutputFiles{1,13} = strcat('_',FolderType(2:end-1),'_',conditions{cond},'_AverageAlphaBetaPAC');
    %             OutputFiles{1,14} = strcat('_',FolderType(2:end-1),'_',conditions{cond},'_AverageAlphaGammaPAC');            
    % 			OutputFiles{1,1} = strcat('_',FolderType(2:end-1),'_',conditions{cond},'_AverageThetaBetaTPAC');
    %             OutputFiles{1,2} = strcat('_',FolderType(2:end-1),'_',conditions{cond},'_AverageThetaGammaTPAC');
    %             OutputFiles{1,17} = strcat('_',FolderType(2:end-1),'_',conditions{cond},'_AverageAlphaBetaTPAC');
    %             OutputFiles{1,18} = strcat('_',FolderType(2:end-1),'_',conditions{cond},'_AverageAlphaGammaTPAC');

                for crun = 1:nMeans
                    inputs{1, crun} = Files(:,crun); % Grandmean: File Names - cfg_files
                    inputs{2, crun} = OutputFiles{1,crun}; % Grandmean: Output filename - cfg_entry
                end

                spm('defaults', 'EEG');
                spm_jobman('run', jobs, inputs{:});

            end
        end
    end
end
