% List of open inputs
% Percent change TF
Manip_all={'adultes'};
Type_all = {'/postmovement/'};
generalEpochedFolderName = './../Epoched data/';
generalPowerFolderName = './../Power data hilbert/pooled_baseline/'; % for single trial baseline, jobs must be modified
Power_units = {'percent'}; % ou dB

for m = 1:size(Manip_all,1)
    for p = 1:size(Power_units,1)
        Power_unit = Power_units{p};
        for t = 1:size(Type_all,1)
            Manip = Manip_all{m};
            Type = Type_all{t}; % ou postmovement

            if strcmp(Manip,'adultes')
                subjects = {'01AC1';'02MG1';'03CC1';'04NB1';'05NF1';'06LL1';'07TJ1';'08CD1';'09MA1';'10NM1';'11LP1';'12KN1';'13LD1';'14NG1';'15OB1';'16SA1';'17GC1';'18AC1';'19BM1'};
                conditions = {'F';'R1';'RR';'RJ';'R2'}; % {'F';'R1';'RR'}; 
                if strcmp(Type(1:13),'/postmovement')
                    timewin = [-1500 5500];
                    baseline = [3500 4000];
                else
                    timewin = [-5500 1500];
                    baseline = [-2000 -1500];
                end
            else
                subjects = {'01FE1';'02BN1';'03AN1';'04MC1';'05JK1';'06GC1';'07EG1';'08HM1';'09ED1';'10DT1';'11VH1';'13MC1';'14TG1';'15LG1';'16SB1';'17JL1';'18LF1';'19EV1';'20LB1';'21EF1';'22LK1';'23LM1'};
                conditions = {'F';'R1';'RR'};
                if strcmp(Type(1:13),'/postmovement')
                    timewin = [-2500 5500];
                    baseline = [3500 4000];
                else
                    timewin = [-5500 2500];
                    baseline = [-2000 -1500];
                end
            end

            % Boucler sur tous les fichiers + toutes les conditions (postmouvement)
            if strcmp(Power_unit, 'percent')
                for i = 1:length(subjects)
                    tic
                    jobfile = {'spm_tf_batch_job_percent.m'};
                    jobs = repmat(jobfile, 1, length(conditions) );
                    inputs = cell(23, length(conditions) );

                    for j = 1:length(conditions) 
                        EpochedFolderName = strcat(generalEpochedFolderName, Manip, Type, conditions{j}, '/');
                        inputs{1, j} = cellstr(strcat(EpochedFolderName, 'spmeeg_', subjects{i}, '-epo.mat')); % Time-frequency analysis: File Name - cfg_files
                        inputs{2, j} = cellstr(strcat(EpochedFolderName, 'tf_spmeeg_', subjects{i}, '-epo.mat')); % Time-frequency rescale: File Name - cfg_files
                        inputs{3, j} = baseline; % Time-frequency rescale: Time window - cfg_entry
                        inputs{4, j} = cellstr(strcat(EpochedFolderName, 'tph_spmeeg_', subjects{i}, '-epo.mat')); % Time-frequency rescale: File Name - cfg_files
                        inputs{5, j} = baseline; % Time-frequency rescale: Time window - cfg_entry
                        inputs{6, j} = cellstr(strcat(EpochedFolderName, 'r_tf_spmeeg_', subjects{i}, '-epo.mat')); % Averaging: File Name - cfg_files
                        inputs{7, j} = cellstr(strcat(EpochedFolderName, 'r_tph_spmeeg_', subjects{i}, '-epo.mat')); % Averaging: File Name - cfg_files
                        inputs{8, j} = cellstr(strcat(EpochedFolderName, 'av_r_tf_spmeeg_', subjects{i}, '-epo.mat')); % Crop: File Name - cfg_files
                        inputs{9, j} = timewin; % Crop: Time window - cfg_entry
                        inputs{10, j} = cellstr(strcat(EpochedFolderName, 'av_r_tph_spmeeg_', subjects{i}, '-epo.mat')); % Crop: File Name - cfg_files
                        inputs{11, j} = timewin; % Crop: Time window - cfg_entry
                        inputs{12, j} = cellstr(strcat(EpochedFolderName, 'c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat')); % Average over frequency: File Name - cfg_files
                        inputs{13, j} = cellstr(strcat(EpochedFolderName, 'c_av_r_tph_spmeeg_', subjects{i}, '-epo.mat')); % Average over frequency: File Name - cfg_files
                        inputs{14, j} = cellstr(strcat(EpochedFolderName, 'c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat')); % Average over frequency: File Name - cfg_files
                        inputs{15, j} = cellstr(strcat(EpochedFolderName, 'c_av_r_tph_spmeeg_', subjects{i}, '-epo.mat')); % Average over frequency: File Name - cfg_files
                        inputs{16, j} = cellstr(strcat(EpochedFolderName, 'c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat')); % Average over frequency: File Name - cfg_files
                        inputs{17, j} = cellstr(strcat(EpochedFolderName, 'c_av_r_tph_spmeeg_', subjects{i}, '-epo.mat')); % Average over frequency: File Name - cfg_files
                        inputs{18, j} = cellstr(strcat(EpochedFolderName, 'c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat')); % Average over frequency: File Name - cfg_files
                        inputs{19, j} = cellstr(strcat(EpochedFolderName, 'c_av_r_tph_spmeeg_', subjects{i}, '-epo.mat')); % Average over frequency: File Name - cfg_files
                        inputs{20, j} = cellstr(strcat(EpochedFolderName, 'av_r_tf_spmeeg_', subjects{i}, '-epo.mat')); % Delete: File Name - cfg_files
                        inputs{21, j} = cellstr(strcat(EpochedFolderName, 'av_r_tph_spmeeg_', subjects{i}, '-epo.mat')); % Delete: File Name - cfg_files
                        inputs{22, j} = cellstr(strcat(EpochedFolderName, 'tf_spmeeg_', subjects{i}, '-epo.mat')); % Delete: File Name - cfg_files 
                        inputs{23, j} = cellstr(strcat(EpochedFolderName, 'tph_spmeeg_', subjects{i}, '-epo.mat')); % Delete: File Name - cfg_files 
                    end

                    spm('defaults', 'EEG');
                    spm_jobman('run', jobs, inputs{:})

                    for j = 1:length(conditions) 
                        PowerFolderName = strcat(generalPowerFolderName, Manip, Type(1:end-1),'_',Power_unit,'/', conditions{j}, '/');
                        EpochedFolderName = strcat(generalEpochedFolderName, Manip, Type, conditions{j}, '/');
                        movefile(strcat(EpochedFolderName, 'c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat'), strcat(PowerFolderName, 'c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat'));
                        movefile(strcat(EpochedFolderName, 'c_av_r_tf_spmeeg_', subjects{i}, '-epo.dat'), strcat(PowerFolderName, 'c_av_r_tf_spmeeg_', subjects{i}, '-epo.dat'));
                        movefile(strcat(EpochedFolderName, 'r_tf_spmeeg_', subjects{i}, '-epo.dat'), strcat(PowerFolderName, 'r_tf_spmeeg_', subjects{i}, '-epo.dat'));
                        movefile(strcat(EpochedFolderName, 'r_tf_spmeeg_', subjects{i}, '-epo.mat'), strcat(PowerFolderName, 'r_tf_spmeeg_', subjects{i}, '-epo.mat'));
                        movefile(strcat(EpochedFolderName, 'theta_c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat'), strcat(PowerFolderName, 'theta_c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat'));
                        movefile(strcat(EpochedFolderName, 'theta_c_av_r_tf_spmeeg_', subjects{i}, '-epo.dat'), strcat(PowerFolderName, 'theta_c_av_r_tf_spmeeg_', subjects{i}, '-epo.dat'));
                        movefile(strcat(EpochedFolderName, 'alpha_c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat'), strcat(PowerFolderName, 'alpha_c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat'));
                        movefile(strcat(EpochedFolderName, 'alpha_c_av_r_tf_spmeeg_', subjects{i}, '-epo.dat'), strcat(PowerFolderName, 'alpha_c_av_r_tf_spmeeg_', subjects{i}, '-epo.dat'));
                        movefile(strcat(EpochedFolderName, 'beta_c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat'), strcat(PowerFolderName, 'beta_c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat'));
                        movefile(strcat(EpochedFolderName, 'beta_c_av_r_tf_spmeeg_', subjects{i}, '-epo.dat'), strcat(PowerFolderName, 'beta_c_av_r_tf_spmeeg_', subjects{i}, '-epo.dat'));
                        movefile(strcat(EpochedFolderName, 'gamma_c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat'), strcat(PowerFolderName, 'gamma_c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat'));
                        movefile(strcat(EpochedFolderName, 'gamma_c_av_r_tf_spmeeg_', subjects{i}, '-epo.dat'), strcat(PowerFolderName, 'gamma_c_av_r_tf_spmeeg_', subjects{i}, '-epo.dat'));

                        movefile(strcat(EpochedFolderName, 'c_av_r_tph_spmeeg_', subjects{i}, '-epo.mat'), strcat(PowerFolderName, 'c_av_r_tph_spmeeg_', subjects{i}, '-epo.mat'));
                        movefile(strcat(EpochedFolderName, 'c_av_r_tph_spmeeg_', subjects{i}, '-epo.dat'), strcat(PowerFolderName, 'c_av_r_tph_spmeeg_', subjects{i}, '-epo.dat'));
                        movefile(strcat(EpochedFolderName, 'r_tph_spmeeg_', subjects{i}, '-epo.dat'), strcat(PowerFolderName, 'r_tph_spmeeg_', subjects{i}, '-epo.dat'));
                        movefile(strcat(EpochedFolderName, 'r_tph_spmeeg_', subjects{i}, '-epo.mat'), strcat(PowerFolderName, 'r_tph_spmeeg_', subjects{i}, '-epo.mat'));
                        movefile(strcat(EpochedFolderName, 'theta_c_av_r_tph_spmeeg_', subjects{i}, '-epo.mat'), strcat(PowerFolderName, 'theta_c_av_r_tph_spmeeg_', subjects{i}, '-epo.mat'));
                        movefile(strcat(EpochedFolderName, 'theta_c_av_r_tph_spmeeg_', subjects{i}, '-epo.dat'), strcat(PowerFolderName, 'theta_c_av_r_tph_spmeeg_', subjects{i}, '-epo.dat'));
                        movefile(strcat(EpochedFolderName, 'alpha_c_av_r_tph_spmeeg_', subjects{i}, '-epo.mat'), strcat(PowerFolderName, 'alpha_c_av_r_tph_spmeeg_', subjects{i}, '-epo.mat'));
                        movefile(strcat(EpochedFolderName, 'alpha_c_av_r_tph_spmeeg_', subjects{i}, '-epo.dat'), strcat(PowerFolderName, 'alpha_c_av_r_tph_spmeeg_', subjects{i}, '-epo.dat'));
                        movefile(strcat(EpochedFolderName, 'beta_c_av_r_tph_spmeeg_', subjects{i}, '-epo.mat'), strcat(PowerFolderName, 'beta_c_av_r_tph_spmeeg_', subjects{i}, '-epo.mat'));
                        movefile(strcat(EpochedFolderName, 'beta_c_av_r_tph_spmeeg_', subjects{i}, '-epo.dat'), strcat(PowerFolderName, 'beta_c_av_r_tph_spmeeg_', subjects{i}, '-epo.dat'));
                        movefile(strcat(EpochedFolderName, 'gamma_c_av_r_tph_spmeeg_', subjects{i}, '-epo.mat'), strcat(PowerFolderName, 'gamma_c_av_r_tph_spmeeg_', subjects{i}, '-epo.mat'));
                        movefile(strcat(EpochedFolderName, 'gamma_c_av_r_tph_spmeeg_', subjects{i}, '-epo.dat'), strcat(PowerFolderName, 'gamma_c_av_r_tph_spmeeg_', subjects{i}, '-epo.dat'));
            %             movefile(strcat(EpochedFolderName, 'tf_spmeeg_', subjects{i}, '-epo.dat'), strcat(PowerFolderName, 'tf_spmeeg_', subjects{i}, '-epo.dat'));
            %             movefile(strcat(EpochedFolderName, 'tf_spmeeg_', subjects{i}, '-epo.mat'), strcat(PowerFolderName, 'tf_spmeeg_', subjects{i}, '-epo.mat'));

                    end
                    toc
                end
            elseif strcmp(Power_unit, 'dB')
                for i = 1:length(subjects)
                    tic
                    jobfile = {'spm_tf_batch_job_dB.m'};
                    jobs = repmat(jobfile, 1, length(conditions) );
                    inputs = cell(23, length(conditions) );

                    for j = 1:length(conditions) 
                        EpochedFolderName = strcat(generalEpochedFolderName, Manip, Type, conditions{j}, '/');
                        inputs{1, j} = cellstr(strcat(EpochedFolderName, 'spmeeg_', subjects{i}, '-epo.mat')); % Time-frequency analysis: File Name - cfg_files
                        inputs{2, j} = cellstr(strcat(EpochedFolderName, 'tf_spmeeg_', subjects{i}, '-epo.mat')); % Time-frequency rescale: File Name - cfg_files
                        inputs{3, j} = baseline; % Time-frequency rescale: Time window - cfg_entry
                        inputs{4, j} = cellstr(strcat(EpochedFolderName, 'tph_spmeeg_', subjects{i}, '-epo.mat')); % Time-frequency rescale: File Name - cfg_files
                        inputs{5, j} = baseline; % Time-frequency rescale: Time window - cfg_entry
                        inputs{6, j} = cellstr(strcat(EpochedFolderName, 'r_tf_spmeeg_', subjects{i}, '-epo.mat')); % Averaging: File Name - cfg_files
                        inputs{7, j} = cellstr(strcat(EpochedFolderName, 'r_tph_spmeeg_', subjects{i}, '-epo.mat')); % Averaging: File Name - cfg_files
                        inputs{8, j} = cellstr(strcat(EpochedFolderName, 'av_r_tf_spmeeg_', subjects{i}, '-epo.mat')); % Crop: File Name - cfg_files
                        inputs{9, j} = timewin; % Crop: Time window - cfg_entry
                        inputs{10, j} = cellstr(strcat(EpochedFolderName, 'av_r_tph_spmeeg_', subjects{i}, '-epo.mat')); % Crop: File Name - cfg_files
                        inputs{11, j} = timewin; % Crop: Time window - cfg_entry
                        inputs{12, j} = cellstr(strcat(EpochedFolderName, 'c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat')); % Average over frequency: File Name - cfg_files
                        inputs{13, j} = cellstr(strcat(EpochedFolderName, 'c_av_r_tph_spmeeg_', subjects{i}, '-epo.mat')); % Average over frequency: File Name - cfg_files
                        inputs{14, j} = cellstr(strcat(EpochedFolderName, 'c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat')); % Average over frequency: File Name - cfg_files
                        inputs{15, j} = cellstr(strcat(EpochedFolderName, 'c_av_r_tph_spmeeg_', subjects{i}, '-epo.mat')); % Average over frequency: File Name - cfg_files
                        inputs{16, j} = cellstr(strcat(EpochedFolderName, 'c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat')); % Average over frequency: File Name - cfg_files
                        inputs{17, j} = cellstr(strcat(EpochedFolderName, 'c_av_r_tph_spmeeg_', subjects{i}, '-epo.mat')); % Average over frequency: File Name - cfg_files
                        inputs{18, j} = cellstr(strcat(EpochedFolderName, 'c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat')); % Average over frequency: File Name - cfg_files
                        inputs{19, j} = cellstr(strcat(EpochedFolderName, 'c_av_r_tph_spmeeg_', subjects{i}, '-epo.mat')); % Average over frequency: File Name - cfg_files
                        inputs{20, j} = cellstr(strcat(EpochedFolderName, 'av_r_tf_spmeeg_', subjects{i}, '-epo.mat')); % Delete: File Name - cfg_files
                        inputs{21, j} = cellstr(strcat(EpochedFolderName, 'av_r_tph_spmeeg_', subjects{i}, '-epo.mat')); % Delete: File Name - cfg_files
                        inputs{22, j} = cellstr(strcat(EpochedFolderName, 'tf_spmeeg_', subjects{i}, '-epo.mat')); % Delete: File Name - cfg_files 
                        inputs{23, j} = cellstr(strcat(EpochedFolderName, 'tph_spmeeg_', subjects{i}, '-epo.mat')); % Delete: File Name - cfg_files 
                    end

                    spm('defaults', 'EEG');
                    spm_jobman('run', jobs, inputs{:})

                    for j = 1:length(conditions) 
                        PowerFolderName = strcat(generalPowerFolderName, Manip, Type(1:end-1),'_',Power_unit,'/', conditions{j}, '/');
                        EpochedFolderName = strcat(generalEpochedFolderName, Manip, Type, conditions{j}, '/');
                        movefile(strcat(EpochedFolderName, 'c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat'), strcat(PowerFolderName, 'c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat'));
                        movefile(strcat(EpochedFolderName, 'c_av_r_tf_spmeeg_', subjects{i}, '-epo.dat'), strcat(PowerFolderName, 'c_av_r_tf_spmeeg_', subjects{i}, '-epo.dat'));
                        movefile(strcat(EpochedFolderName, 'r_tf_spmeeg_', subjects{i}, '-epo.dat'), strcat(PowerFolderName, 'r_tf_spmeeg_', subjects{i}, '-epo.dat'));
                        movefile(strcat(EpochedFolderName, 'r_tf_spmeeg_', subjects{i}, '-epo.mat'), strcat(PowerFolderName, 'r_tf_spmeeg_', subjects{i}, '-epo.mat'));
                        movefile(strcat(EpochedFolderName, 'theta_c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat'), strcat(PowerFolderName, 'theta_c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat'));
                        movefile(strcat(EpochedFolderName, 'theta_c_av_r_tf_spmeeg_', subjects{i}, '-epo.dat'), strcat(PowerFolderName, 'theta_c_av_r_tf_spmeeg_', subjects{i}, '-epo.dat'));
                        movefile(strcat(EpochedFolderName, 'alpha_c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat'), strcat(PowerFolderName, 'alpha_c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat'));
                        movefile(strcat(EpochedFolderName, 'alpha_c_av_r_tf_spmeeg_', subjects{i}, '-epo.dat'), strcat(PowerFolderName, 'alpha_c_av_r_tf_spmeeg_', subjects{i}, '-epo.dat'));
                        movefile(strcat(EpochedFolderName, 'beta_c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat'), strcat(PowerFolderName, 'beta_c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat'));
                        movefile(strcat(EpochedFolderName, 'beta_c_av_r_tf_spmeeg_', subjects{i}, '-epo.dat'), strcat(PowerFolderName, 'beta_c_av_r_tf_spmeeg_', subjects{i}, '-epo.dat'));
                        movefile(strcat(EpochedFolderName, 'gamma_c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat'), strcat(PowerFolderName, 'gamma_c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat'));
                        movefile(strcat(EpochedFolderName, 'gamma_c_av_r_tf_spmeeg_', subjects{i}, '-epo.dat'), strcat(PowerFolderName, 'gamma_c_av_r_tf_spmeeg_', subjects{i}, '-epo.dat'));

                        movefile(strcat(EpochedFolderName, 'c_av_r_tph_spmeeg_', subjects{i}, '-epo.mat'), strcat(PowerFolderName, 'c_av_r_tph_spmeeg_', subjects{i}, '-epo.mat'));
                        movefile(strcat(EpochedFolderName, 'c_av_r_tph_spmeeg_', subjects{i}, '-epo.dat'), strcat(PowerFolderName, 'c_av_r_tph_spmeeg_', subjects{i}, '-epo.dat'));
                        movefile(strcat(EpochedFolderName, 'r_tph_spmeeg_', subjects{i}, '-epo.dat'), strcat(PowerFolderName, 'r_tph_spmeeg_', subjects{i}, '-epo.dat'));
                        movefile(strcat(EpochedFolderName, 'r_tph_spmeeg_', subjects{i}, '-epo.mat'), strcat(PowerFolderName, 'r_tph_spmeeg_', subjects{i}, '-epo.mat'));
                        movefile(strcat(EpochedFolderName, 'theta_c_av_r_tph_spmeeg_', subjects{i}, '-epo.mat'), strcat(PowerFolderName, 'theta_c_av_r_tph_spmeeg_', subjects{i}, '-epo.mat'));
                        movefile(strcat(EpochedFolderName, 'theta_c_av_r_tph_spmeeg_', subjects{i}, '-epo.dat'), strcat(PowerFolderName, 'theta_c_av_r_tph_spmeeg_', subjects{i}, '-epo.dat'));
                        movefile(strcat(EpochedFolderName, 'alpha_c_av_r_tph_spmeeg_', subjects{i}, '-epo.mat'), strcat(PowerFolderName, 'alpha_c_av_r_tph_spmeeg_', subjects{i}, '-epo.mat'));
                        movefile(strcat(EpochedFolderName, 'alpha_c_av_r_tph_spmeeg_', subjects{i}, '-epo.dat'), strcat(PowerFolderName, 'alpha_c_av_r_tph_spmeeg_', subjects{i}, '-epo.dat'));
                        movefile(strcat(EpochedFolderName, 'beta_c_av_r_tph_spmeeg_', subjects{i}, '-epo.mat'), strcat(PowerFolderName, 'beta_c_av_r_tph_spmeeg_', subjects{i}, '-epo.mat'));
                        movefile(strcat(EpochedFolderName, 'beta_c_av_r_tph_spmeeg_', subjects{i}, '-epo.dat'), strcat(PowerFolderName, 'beta_c_av_r_tph_spmeeg_', subjects{i}, '-epo.dat'));
                        movefile(strcat(EpochedFolderName, 'gamma_c_av_r_tph_spmeeg_', subjects{i}, '-epo.mat'), strcat(PowerFolderName, 'gamma_c_av_r_tph_spmeeg_', subjects{i}, '-epo.mat'));
                        movefile(strcat(EpochedFolderName, 'gamma_c_av_r_tph_spmeeg_', subjects{i}, '-epo.dat'), strcat(PowerFolderName, 'gamma_c_av_r_tph_spmeeg_', subjects{i}, '-epo.dat'));
            %             movefile(strcat(EpochedFolderName, 'tf_spmeeg_', subjects{i}, '-epo.dat'), strcat(PowerFolderName, 'tf_spmeeg_', subjects{i}, '-epo.dat'));
            %             movefile(strcat(EpochedFolderName, 'tf_spmeeg_', subjects{i}, '-epo.mat'), strcat(PowerFolderName, 'tf_spmeeg_', subjects{i}, '-epo.mat'));

                    end
                    toc
                end
            end
        end
    end
end