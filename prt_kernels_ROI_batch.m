% List of open inputs
% Feature set/Kernel: Load PRT.mat - cfg_files

% List of open inputs
% Data & Design: Directory - cfg_files
% Data & Design: Masks - cfg_repeat
global Manip Type Channels BandsSimple BandsPAC Files PowerUnit

Manip = 'adultes'; % ou 'enfants'
Type = 'postmovement_order';
Files = {'power'};
PowerUnit = 'percent'; % or 'dB'

generalProntoFolderName = './../Pronto ROI data/';
BandsSimple = {'theta';'alpha';'beta';'gamma'};
% BandsPAC = {'theta_beta';'theta_gamma';'alpha_beta';'alpha_gamma'};
BandsPAC = {'theta_beta';'theta_gamma'};

Channels = {'FP';'SM';'RPM';'LPM';'RM';'LM';'RSS';'LSS';'RP';'LP';'OP'};

jobfile = {'prt_kernels_ROI_batch_job.m'};
jobs = repmat(jobfile, 1, 1);
inputs = cell(1, 1);
if(length(Files) == 1), inputs{1, 1} = cellstr(strcat(generalProntoFolderName, Manip, '/', PowerUnit, '/', Type, '_', Files{1},'_lowgamma', '/PRT.mat')); % Feature set/Kernel: Load PRT.mat - cfg_files
else, inputs{1, 1} = cellstr(strcat(generalProntoFolderName, Manip, '/', PowerUnit, '/', Type, '_all/PRT.mat'));
end
job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);
