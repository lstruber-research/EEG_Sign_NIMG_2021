# EEG Sign NIMG 2021
Python (MNE) and Matlab (SPM/Pronto) scripts that allows to process data from experiment EEG_Sign that led to article “Brain oscillatory correlates of visuomotor adaptive learning” (cf. References)

## Installation
Python scripts need MNE library (https://mne.tools/stable/index.html)
Matlab scripts need SPM 12 (https://www.fil.ion.ucl.ac.uk/spm/software/spm12/) and Pronto toolboxes (http://www.mlnl.cs.ucl.ac.uk/pronto/prtsoftware.html)

## License
[MIT](https://choosealicense.com/licenses/mit/)

## Reference
Struber L, Baumont M, Barraud P-A, Nougier V, Cignetti F, “Brain oscillatory correlates of visuomotor adaptive learning”, Neuroimage, 2021, 245:118645 (https://www.sciencedirect.com/science/article/pii/S1053811921009186)
