"""
EEG Power calculation (MNE) 
Created on Thu Feb 20 10:33:55 2020
@author: struberl
power"""

import Preprocessing as pp
import os.path
import os
import mne
import matplotlib.pyplot as plt

Manip = "adultes" #ou "enfants"
generalFolderName = "./../"
channels = None #['D19','D20','D21','D22','D1']

if Manip == "adultes":
	groupRawFolderName = "Preprocessed data/processed-raw/processed_raw_"
	subjects = ['01AC1','02MG1','03CC1','04NB1','05NF1','06LL1','07TJ1','08CD1','09MA1','10NM1','11LP1','12KN1','13LD1','14NG1','15OB1','16SA1','17GC1','18AC1','19BM1']
	conditions = ['RJ','R2'] # ['F','R1','R2','RJ','RR','wR1','wR2']  
	t_pre = [-5.5, 1.5]
	t_post = [-1.5, 5.5]
    
else:
	groupRawFolderName = "Preprocessed data/processed-raw-enfants/"
	subjects = ['01FE1'] #['01FE1','02BN1','03AN1','04MC1','05JK1','06GC1','07EG1','08HM1','09ED1','10DT1','11VH1','13MC1','14TG1','15LG1','16SB1','17JL1','18LF1','19EV1','20LB1','21EF1','22LK1','23LM1']
	conditions = ['RR'] #['F','R1','RR']
	t_pre = [-5.5, 2.5]
	t_post = [-2.5, 5.5]

list_bad_epochs_pre = []
list_bad_epochs_post = []
was_first_epoch_deleted = []	
for sub in subjects:
    list_bad_epochs_pre.append([])
    list_bad_epochs_post.append([])
    was_first_epoch_deleted.append([])
    for cond in conditions:
        list_bad_epochs_pre[-1].append([])
        list_bad_epochs_post[-1].append([])
        was_first_epoch_deleted[-1].append([])
    
        rawFileName = generalFolderName + groupRawFolderName + sub + "-" + cond + ".fif"
        powerFolderName = generalFolderName + "Power data/" + Manip
        if os.path.isfile(rawFileName):
            if os.path.isfile(powerFolderName  + "/postmovement/" + cond + "/" + sub + "-gamma-tfr.h5"):
                print("The processed files " + powerFolderName  + "/postmovement/" + cond + "/" + sub + "-gamma-tfr.h5" + " already exists")
            else:
                print("subject : " + sub + " -- condition :" + cond)
                raw = mne.io.read_raw_fif(rawFileName, preload=True)
                
                # pre movement (epoch 3 fig + power 1 fig)
#                epochs_pre, toDrop_pre = pp.epoch_creation(raw, 257, t_pre[0] - 0.2, t_pre[1] + 0.2, reject=dict(eeg=5e-5), bad_epochs='None', channels=channels, plot = True)
#                list_bad_epochs_pre[-1][-1] = toDrop_pre
#                if epochs_pre.drop_log[2]: 
#                    was_first_epoch_deleted[-1][-1] = 1
#                else:
#                    was_first_epoch_deleted[-1][-1] = 0 
                # post movement (epoch 3 fig + power 1 fig)
                epochs_post, toDrop_post = pp.epoch_creation(raw, 254, t_post[0] - 0.2, t_post[1]  + 0.2, reject=dict(eeg=5e-5), bad_epochs='None', channels=channels, plot = True)
                list_bad_epochs_post[-1][-1] = toDrop_post
              
                while pp.close_event == 0:
                    plt.pause(2)
                print("Next File\n\n")
                pp.close_event = 0
        else:
            print("The preprocessed file " + rawFileName + " does not exist")
            
#NbDropped = 0
#for bad_epochs_pre in list_bad_epochs_pre:
#    NbDropped += (len(bad_epochs_pre[0]) + len(bad_epochs_pre[1]) + len(bad_epochs_pre[2]))/3
#    
#for bad_epochs_post in list_bad_epochs_post:
#    NbDropped += (len(bad_epochs_post[0]) + len(bad_epochs_post[1]) + len(bad_epochs_post[2]))/3
#    
#PercentDropped = NbDropped/(19*2*80) * 100
