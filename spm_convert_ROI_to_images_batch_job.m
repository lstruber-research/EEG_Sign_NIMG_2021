%-----------------------------------------------------------------------
% Job saved on 08-Apr-2020 08:42:02 by cfg_util (rev $Rev: 7345 $)
% spm SPM - SPM12 (7771)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------

matlabbatch{1}.spm.meeg.images.convert2images.D = '<UNDEFINED>';
matlabbatch{1}.spm.meeg.images.convert2images.mode = 'time';
matlabbatch{1}.spm.meeg.images.convert2images.conditions = {};
matlabbatch{1}.spm.meeg.images.convert2images.channels{1}.chan = '<UNDEFINED>';
matlabbatch{1}.spm.meeg.images.convert2images.timewin = '<UNDEFINED>';
matlabbatch{1}.spm.meeg.images.convert2images.freqwin = [-Inf Inf];
matlabbatch{1}.spm.meeg.images.convert2images.prefix = '<UNDEFINED>';

matlabbatch{2}.spm.meeg.images.convert2images.D = '<UNDEFINED>';
matlabbatch{2}.spm.meeg.images.convert2images.mode = 'time';
matlabbatch{2}.spm.meeg.images.convert2images.conditions = {};
matlabbatch{2}.spm.meeg.images.convert2images.channels{1}.chan = '<UNDEFINED>';
matlabbatch{2}.spm.meeg.images.convert2images.timewin = '<UNDEFINED>';
matlabbatch{2}.spm.meeg.images.convert2images.freqwin = [-Inf Inf];
matlabbatch{2}.spm.meeg.images.convert2images.prefix = '<UNDEFINED>';
% 
% matlabbatch{3}.spm.meeg.images.convert2images.D = '<UNDEFINED>';
% matlabbatch{3}.spm.meeg.images.convert2images.mode = 'time';
% matlabbatch{3}.spm.meeg.images.convert2images.conditions = {};
% matlabbatch{3}.spm.meeg.images.convert2images.channels{1}.chan = '<UNDEFINED>';
% matlabbatch{3}.spm.meeg.images.convert2images.timewin = '<UNDEFINED>';
% matlabbatch{3}.spm.meeg.images.convert2images.freqwin = [-Inf Inf];
% matlabbatch{3}.spm.meeg.images.convert2images.prefix = '<UNDEFINED>';
% 
% matlabbatch{4}.spm.meeg.images.convert2images.D = '<UNDEFINED>';
% matlabbatch{4}.spm.meeg.images.convert2images.mode = 'time';
% matlabbatch{4}.spm.meeg.images.convert2images.conditions = {};
% matlabbatch{4}.spm.meeg.images.convert2images.channels{1}.chan = '<UNDEFINED>';
% matlabbatch{4}.spm.meeg.images.convert2images.timewin = '<UNDEFINED>';
% matlabbatch{4}.spm.meeg.images.convert2images.freqwin = [-Inf Inf];
% matlabbatch{4}.spm.meeg.images.convert2images.prefix = '<UNDEFINED>';
%
% matlabbatch{5}.spm.meeg.images.convert2images.D = '<UNDEFINED>';
% matlabbatch{5}.spm.meeg.images.convert2images.mode = 'time';
% matlabbatch{5}.spm.meeg.images.convert2images.conditions = {};
% matlabbatch{5}.spm.meeg.images.convert2images.channels{1}.chan = '<UNDEFINED>';
% matlabbatch{5}.spm.meeg.images.convert2images.timewin = '<UNDEFINED>';
% matlabbatch{5}.spm.meeg.images.convert2images.freqwin = [-Inf Inf];
% matlabbatch{5}.spm.meeg.images.convert2images.prefix = '<UNDEFINED>';