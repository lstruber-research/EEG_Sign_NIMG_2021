% List of open inputs
% Convert2Images: File Name - cfg_files
% Convert2Images: Time window - cfg_entry
Manip_all={'adultes'};
Type_all = {'/postmovement_percent/';'/postmovement_first_percent/';'/postmovement_last_percent/'};
generalPowerFolderName = './../Power data hilbert/pooled_baseline/';
generalImagesFolderName = './../Pronto ROI images/';
ROIs = {{'C8';'C9';'C10';'C14';'C15';'C16';'C17';'C18';'C19';'C27';'C28';'C29';'C30';'C31';'C32'};...% FP
    {'C11';'C12';'C13';'C20';'C21';'C22';'C24';'C25';'C26'};...                                      % SM
    {'C3';'C4';'C5';'C6';'C7';'B27';'B28';'B29';'B30'};...                                           % RPM
    {'D3';'D4';'D5';'D6';'D7';'D8';'D9';'D10';'D11'};...                                             % LPM
    {'A1';'B1';'C1';'C2';'B20';'B21';'B22';'B23';'B31';'B32';'C23'};...                              % RM
    {'A1';'D1';'D2';'D12';'D13';'D14';'D15';'D18';'D19';'D20';'C23'};...                             % LM
    {'A1';'A2';'A3';'B1';'B2';'B16';'B17';'B18';'B19';'B20'};...                                     % RSS
    {'A1';'A2';'A3';'D14';'D15';'D16';'D17';'D26';'D27';'D28'};...                                   % LSS
    {'A4';'A19';'A20';'A21';'A30';'A31';'A32';'B3';'B4';'B5';'B6';'B10';'B11';'B12';'B13'};...       % RP
    {'A4';'A19';'A20';'A21';'A5';'A6';'A7';'A8';'A9';'A17';'A18';'D29';'D30';'D31';'D32'};...        % LP
    {'A10';'A11';'A12';'A14';'A15';'A16';'A22';'A23';'A24';'A27';'A28';'A29';'B7';'B8';'B9'}};       % OP

ROI_names = {'FP';'SM';'RPM';'LPM';'RM';'LM';'RSS';'LSS';'RP';'LP';'OP'};

Bands = {'theta';'alpha';'beta';'gamma'};
BandsPAC = {'theta_beta';'theta_gamma'};

for m = 1:size(Manip_all,1)
    Manip = Manip_all{m};
    for t = 1:size(Type_all,1)
        Type = Type_all{t}; % ou postmovement
        
        if strcmp(Manip,'adultes')
            subjects = {'01AC1';'02MG1';'03CC1';'04NB1';'05NF1';'06LL1';'07TJ1';'08CD1';'09MA1';'10NM1';'11LP1';'12KN1';'13LD1';'14NG1';'15OB1';'16SA1';'17GC1';'18AC1';'19BM1'};
            conditions = {'F';'RR';'RJ';'R1';'R2'}; 
            if strcmp(Type(1:5),'/post')
                timewin = [-200 2500];
                Type_power = Type;
            elseif strcmp(Type(1:4),'/pre')
                timewin = [-2000 700];
                Type_power = Type;
            else
                timewin = [0 1500];
                Type_power = '/premovement/';
            end
        else
            subjects = {'01FE1';'02BN1';'03AN1';'04MC1';'05JK1';'06GC1';'07EG1';'08HM1';'09ED1';'10DT1';'11VH1';'13MC1';'14TG1';'15LG1';'16SB1';'17JL1';'18LF1';'19EV1';'20LB1';'21EF1';'22LK1';'23LM1'};
            conditions = {'F';'RR';'R1'};
            if strcmp(Type(1:5),'/post')
                timewin = [-200 2500];
                Type_power = Type;
            elseif strcmp(Type(1:4),'/pre')
                timewin = [-2000 700];
                Type_power = Type;
            else
                timewin = [0 2500];
                Type_power = '/premovement/';
            end
        end

        % Boucler sur tous les fichiers + toutes les conditions (postmovement)
        for i = 1:length(subjects)
            for j = 1:length(conditions)
                tic
                for roi = 1:length(ROIs)
                    for band = 1:length(Bands)

                        jobfile = {'spm_convert_ROI_to_images_batch_job.m'};
                        jobs = repmat(jobfile, 1, length(ROIs{roi}));
%                             inputs = cell(16, length(ROIs{roi}));
                        inputs = cell(8, length(ROIs{roi}));

                        for ch_num = 1:length(ROIs{roi})
                            PowerFolderName_file = strcat(generalPowerFolderName, Manip, Type_power, conditions{j}, '/',Bands{band}, '_c_av_r_tf_spmeeg_',subjects{i},'-epo.mat');
                            inputs{1, ch_num} = cellstr(PowerFolderName_file); % Convert2Images: File Name - cfg_files
                            inputs{2, ch_num} = ROIs{roi}{ch_num}; % Convert2Images: Time window - cfg_entry
                            inputs{3, ch_num} = timewin; % Convert2Images: Time window - cfg_entry
                            inputs{4, ch_num} = strcat(ROI_names{roi},'_',ROIs{roi}{ch_num},'_'); % Convert2Images: Prefix - cfg_entry
%                                 
                            PhaseFolderName_file = strcat(generalPowerFolderName, Manip, Type_power, conditions{j}, '/',Bands{band}, '_c_av_r_tph_spmeeg_',subjects{i},'-epo.mat');
                            inputs{5, ch_num} = cellstr(PhaseFolderName_file); % Convert2Images: File Name - cfg_files
                            inputs{6, ch_num} = ROIs{roi}{ch_num}; % Convert2Images: Time window - cfg_entry
                            inputs{7, ch_num} = timewin; % Convert2Images: Time window - cfg_entry
                            inputs{8, ch_num} = strcat(ROI_names{roi},'_',ROIs{roi}{ch_num},'_'); % Convert2Images: Prefix - cfg_entry
% 								
% 								  PACFolderName_file = strcat(generalPowerFolderName, Manip, Type_power, conditions{j}, '/',BandsPAC{band}, '_pac_spmeeg_',subjects{i},'-epo.mat');
%                                 inputs{9, ch_num} = cellstr(PACFolderName_file); % Convert2Images: File Name - cfg_files
%                                 inputs{10, ch_num} = ROIs{roi}{ch_num}; % Convert2Images: Time window - cfg_entry
%                                 inputs{11, ch_num} = timewin; % Convert2Images: Time window - cfg_entry
%                                 inputs{12, ch_num} = strcat(ROI_names{roi},'_',ROIs{roi}{ch_num},'_'); % Convert2Images: Prefix - cfg_entry
%                                 
%                                 tPACFolderName_file = strcat(generalPowerFolderName, Manip, Type_power, conditions{j}, '/',BandsPAC{band}, '_tpac_spmeeg_',subjects{i},'-epo.mat');
%                                 inputs{9, ch_num} = cellstr(tPACFolderName_file); % Convert2Images: File Name - cfg_files
%                                 inputs{10, ch_num} = ROIs{roi}{ch_num}; % Convert2Images: Time window - cfg_entry
%                                 inputs{11, ch_num} = timewin; % Convert2Images: Time window - cfg_entry
%                                 inputs{12, ch_num} = strcat(ROI_names{roi},'_',ROIs{roi}{ch_num},'_'); % Convert2Images: Prefix - cfg_entry
%                                 
%                                 mvlPACFolderName_file = strcat(generalPowerFolderName, Manip, Type_power, conditions{j}, '/',BandsPAC{band}, '_mvlpac_spmeeg_',subjects{i},'-epo.mat');
%                                 inputs{1, ch_num} = cellstr(mvlPACFolderName_file); % Convert2Images: File Name - cfg_files
%                                 inputs{2, ch_num} = ROIs{roi}{ch_num}; % Convert2Images: Time window - cfg_entry
%                                 inputs{3, ch_num} = timewin; % Convert2Images: Time window - cfg_entry
%                                 inputs{4, ch_num} = strcat(ROI_names{roi},'_',ROIs{roi}{ch_num},'_'); % Convert2Images: Prefix - cfg_entry


%                                 
%                                 tPACFolderName_file = strcat(generalPowerFolderName, Manip, Type_power, conditions{j}, '/',BandsPAC{band}, '_tpac_spmeeg_',subjects{i},'-epo.mat');
%                                 inputs{1, ch_num} = cellstr(tPACFolderName_file); % Convert2Images: File Name - cfg_files
%                                 inputs{2, ch_num} = ROIs{roi}{ch_num}; % Convert2Images: Time window - cfg_entry
%                                 inputs{3, ch_num} = [-Inf Inf]; % Convert2Images: Time window - cfg_entry
%                                 inputs{4, ch_num} = strcat(ROI_names{roi},'_',ROIs{roi}{ch_num},'_'); % Convert2Images: Prefix - cfg_entry
%                                 
% %                                 MIFolderName_file = strcat(generalPowerFolderName, Manip, Type_power, conditions{j}, '/',BandsPAC{band}, '_MI_spmeeg_',subjects{i},'-epo.mat');
% %                                 inputs{1, ch_num} = cellstr(MIFolderName_file); % Convert2Images: File Name - cfg_files
% %                                 inputs{2, ch_num} = ROIs{roi}{ch_num}; % Convert2Images: Time window - cfg_entry
% %                                 inputs{3, ch_num} = [-Inf Inf]; % Convert2Images: Time window - cfg_entry
% %                                 inputs{4, ch_num} = strcat(ROI_names{roi},'_',ROIs{roi}{ch_num},'_'); % Convert2Images: Prefix - cfg_entry
%                                 
%                                 PCFolderName_file = strcat(generalPowerFolderName, Manip, Type_power, conditions{j}, '/',BandsPAC{band}, '_PC_spmeeg_',subjects{i},'-epo.mat');
%                                 inputs{1, ch_num} = cellstr(PCFolderName_file); % Convert2Images: File Name - cfg_files
%                                 inputs{2, ch_num} = ROIs{roi}{ch_num}; % Convert2Images: Time window - cfg_entry
%                                 inputs{3, ch_num} = [-Inf Inf]; % Convert2Images: Time window - cfg_entry
%                                 inputs{4, ch_num} = strcat(ROI_names{roi},'_',ROIs{roi}{ch_num},'_'); % Convert2Images: Prefix - cfg_entry
                        end

                        spm('defaults', 'EEG');
                        spm_jobman('run', jobs, inputs{:});

                        % Open all power files
                        V = zeros(abs(timewin(2) - timewin(1))/5 + 1, length(ROIs{roi}),'single');
                        for ch_num = 1:length(ROIs{roi})
                            ImageFolderName =  strcat(generalPowerFolderName, Manip, Type_power, conditions{j}, '/', ROI_names{roi},'_',ROIs{roi}{ch_num},'_',Bands{band},'_c_av_r_tf_spmeeg_',subjects{i},'-epo');
                            info = niftiinfo(strcat(ImageFolderName,'/condition_',conditions{j},'.nii'));
                            V(:,ch_num) = niftiread(info);
                        end
                        V = mean(V,2);
                        ImageRenamedFolderName = strcat(generalPowerFolderName, Manip, Type_power, conditions{j}, '/', ROI_names{roi});
                        mkdir(ImageRenamedFolderName);
                        info.ImageSize = [abs(timewin(2) - timewin(1))/5 + 1 1];
                        info.PixelDimensions = [1 1];
                        info.raw.dim = [2 abs(timewin(2) - timewin(1))/5 + 1 1 1 1 1 1 1];
                        niftiwrite(V,strcat(ImageRenamedFolderName,'/', Bands{band},'_power_',conditions{j},'.nii'), info);

                        for ch_num = 1:length(ROIs{roi})
                            ImageFolderName =  strcat(generalPowerFolderName, Manip, Type_power, conditions{j}, '/', ROI_names{roi},'_',ROIs{roi}{ch_num},'_',Bands{band},'_c_av_r_tf_spmeeg_',subjects{i},'-epo');
                            rmdir(strcat(ImageFolderName),'s');
                        end

                        ImageNewFolderName = strcat(generalImagesFolderName, Manip, Type, subjects{i},'/');
                        mkdir(ImageNewFolderName);
                        movefile(ImageRenamedFolderName, ImageNewFolderName);
%                             
                        % Open all phase files
                        V = zeros(abs(timewin(2) - timewin(1))/5 + 1, length(ROIs{roi}),'single');
                        for ch_num = 1:length(ROIs{roi})
                            ImageFolderName =  strcat(generalPowerFolderName, Manip, Type_power, conditions{j}, '/', ROI_names{roi},'_',ROIs{roi}{ch_num},'_',Bands{band},'_c_av_r_tph_spmeeg_',subjects{i},'-epo');
                            info = niftiinfo(strcat(ImageFolderName,'/condition_',conditions{j},'.nii'));
                            V(:,ch_num) = niftiread(info);
                        end
                        V = mean(V,2);
                        ImageRenamedFolderName = strcat(generalPowerFolderName, Manip, Type_power, conditions{j}, '/', ROI_names{roi});
                        mkdir(ImageRenamedFolderName);
                        info.ImageSize = [abs(timewin(2) - timewin(1))/5 + 1 1];
                        info.PixelDimensions = [1 1];
                        info.raw.dim = [2 abs(timewin(2) - timewin(1))/5 + 1 1 1 1 1 1 1];
                        niftiwrite(V,strcat(ImageRenamedFolderName,'/', Bands{band},'_phase_',conditions{j},'.nii'), info);

                        for ch_num = 1:length(ROIs{roi})
                            ImageFolderName =  strcat(generalPowerFolderName, Manip, Type_power, conditions{j}, '/', ROI_names{roi},'_',ROIs{roi}{ch_num},'_',Bands{band},'_c_av_r_tph_spmeeg_',subjects{i},'-epo');
                            rmdir(strcat(ImageFolderName),'s');
                        end

                        ImageNewFolderName = strcat(generalImagesFolderName, Manip, Type, subjects{i},'/');
                        mkdir(ImageNewFolderName);
                        movefile(ImageRenamedFolderName, ImageNewFolderName);
% 							
% 							% Open all PAC files
%                             V = zeros(abs(timewin(2) - timewin(1))/5 + 1, length(ROIs{roi}),'single');
%                             for ch_num = 1:length(ROIs{roi})
%                                 ImageFolderName =  strcat(generalPowerFolderName, Manip, Type_power, conditions{j}, '/', ROI_names{roi},'_',ROIs{roi}{ch_num},'_',BandsPAC{band},'_pac_spmeeg_',subjects{i},'-epo');
%                                 info = niftiinfo(strcat(ImageFolderName,'/condition_',conditions{j},'.nii'));
%                                 V(:,ch_num) = niftiread(info);
%                             end
%                             V = mean(V,2);
%                             ImageRenamedFolderName = strcat(generalPowerFolderName, Manip, Type_power, conditions{j}, '/', ROI_names{roi});
%                             mkdir(ImageRenamedFolderName);
%                             info.ImageSize = [abs(timewin(2) - timewin(1))/5 + 1 1];
%                             info.PixelDimensions = [1 1];
%                             info.raw.dim = [2 abs(timewin(2) - timewin(1))/5 + 1 1 1 1 1 1 1];
%                             niftiwrite(V,strcat(ImageRenamedFolderName,'/', BandsPAC{band},'_pac_',conditions{j},'.nii'), info);
% 
%                             for ch_num = 1:length(ROIs{roi})
%                                 ImageFolderName =  strcat(generalPowerFolderName, Manip, Type_power, conditions{j}, '/', ROI_names{roi},'_',ROIs{roi}{ch_num},'_',BandsPAC{band},'_pac_spmeeg_',subjects{i},'-epo');
%                                 rmdir(strcat(ImageFolderName),'s');
%                             end
% 
%                             ImageNewFolderName = strcat(generalImagesFolderName, Manip, Type, subjects{i},'/');
%                             mkdir(ImageNewFolderName);
%                             movefile(ImageRenamedFolderName, ImageNewFolderName);
%                             
%                             % Open all tPAC files
%                             StartWin = - 1.7;
%                             Margin = 0.2;
%                             FinalWin = [-5.5, 0.5];
%                             winLen = 0.5;
%                             Rate = 200;
%                             overlap = 0.5;
%                             Idx = (FinalWin-StartWin).*Rate + 1;
%                             tStep = winLen*(1-overlap);
%                             nTime = fix((length(Idx(1):Idx(2)) - 1 -fix(winLen*Rate))/fix(tStep*Rate))+1;
%                             TimeOutStart = ((FinalWin(1)+winLen/2)-StartWin-Margin)/(winLen/2);
% 
%                             V = zeros(length(TimeOutStart:TimeOutStart + nTime - 1), length(ROIs{roi}),'single');
%                             for ch_num = 1:length(ROIs{roi})
%                                 ImageFolderName =  strcat(generalPowerFolderName, Manip, Type_power, conditions{j}, '/', ROI_names{roi},'_',ROIs{roi}{ch_num},'_',BandsPAC{band},'_tpac_spmeeg_',subjects{i},'-epo');
%                                 info = niftiinfo(strcat(ImageFolderName,'/condition_Undefined.nii'));
%                                 V(:,ch_num) = niftiread(info);
%                             end
%                             V = mean(V,2);
%                             ImageRenamedFolderName = strcat(generalPowerFolderName, Manip, Type_power, conditions{j}, '/', ROI_names{roi});
%                             mkdir(ImageRenamedFolderName);
%                             info.ImageSize = [length(TimeOutStart:TimeOutStart + nTime - 1) 1];
%                             info.PixelDimensions = [1 1];
%                             info.raw.dim = [2 length(TimeOutStart:TimeOutStart + nTime - 1) 1 1 1 1 1 1];
%                             niftiwrite(V,strcat(ImageRenamedFolderName,'/', BandsPAC{band},'_tpac_',conditions{j},'.nii'), info);
% 
%                             for ch_num = 1:length(ROIs{roi})
%                                 ImageFolderName =  strcat(generalPowerFolderName, Manip, Type_power, conditions{j}, '/', ROI_names{roi},'_',ROIs{roi}{ch_num},'_',BandsPAC{band},'_tpac_spmeeg_',subjects{i},'-epo');
%                                 rmdir(strcat(ImageFolderName),'s');
%                             end
% 
%                             ImageNewFolderName = strcat(generalImagesFolderName, Manip, Type, subjects{i},'/');
%                             mkdir(ImageNewFolderName);
%                             movefile(ImageRenamedFolderName, ImageNewFolderName);
%                             % Open all MI files
%                             if(band == 1 && roi == 1 && i == 1 && j ==1)
%                                 ImageRenamedFolderName = strcat(generalImagesFolderName, Manip, Type, subjects{i},'/', ROI_names{roi});
%                                 info = niftiinfo(strcat(ImageRenamedFolderName,'/', BandsPAC{band},'_tpac_',conditions{j},'.nii'));
%                             end
%                             V = zeros(1, length(ROIs{roi}),'single');
%                             for ch_num = 1:length(ROIs{roi})
%                                 ImageFolderName =  strcat(generalPowerFolderName, Manip, Type_power, conditions{j}, '/', ROI_names{roi},'_',ROIs{roi}{ch_num},'_',BandsPAC{band},'_mvlpac_spmeeg_',subjects{i},'-epo');
%                                 V(:,ch_num) = niftiread(strcat(ImageFolderName,'/condition_Undefined.nii'));
%                             end
%                             V = mean(V,2);
%                             ImageRenamedFolderName = strcat(generalPowerFolderName, Manip, Type_power, conditions{j}, '/', ROI_names{roi});
%                             mkdir(ImageRenamedFolderName);
%                             info.ImageSize = [1 1];
%                             info.PixelDimensions = [1 1];
%                             info.raw.dim = [2 1 1 1 1 1 1 1];
%                             niftiwrite(V,strcat(ImageRenamedFolderName,'/', BandsPAC{band},'_mvlpac_',conditions{j},'.nii'), info);
% 
%                             for ch_num = 1:length(ROIs{roi})
%                                 ImageFolderName =  strcat(generalPowerFolderName, Manip, Type_power, conditions{j}, '/', ROI_names{roi},'_',ROIs{roi}{ch_num},'_',BandsPAC{band},'_mvlpac_spmeeg_',subjects{i},'-epo');
%                                 rmdir(strcat(ImageFolderName),'s');
%                             end
% 
%                             ImageNewFolderName = strcat(generalImagesFolderName, Manip, Type, subjects{i},'/');
%                             mkdir(ImageNewFolderName);
%                             movefile(ImageRenamedFolderName, ImageNewFolderName);

%                             % Open all PC files
%                             Start = - 1.7;
%                             Window = [-0.2, 2.5];
%                             Index = 200.*(Window - Start) + 1;
%                             VecTime = fix(Index(1):Index(2));
% 
%                             V = zeros(length(VecTime), length(ROIs{roi}),'single');
%                             for ch_num = 1:length(ROIs{roi})
%                                 ImageFolderName =  strcat(generalPowerFolderName, Manip, Type_power, conditions{j}, '/', ROI_names{roi},'_',ROIs{roi}{ch_num},'_',BandsPAC{band},'_PC_spmeeg_',subjects{i},'-epo');
%                                 info = niftiinfo(strcat(ImageFolderName,'/condition_Undefined.nii'));
%                                 V(:,ch_num) = niftiread(info);
%                             end
%                             V = mean(V,2);
%                             ImageRenamedFolderName = strcat(generalPowerFolderName, Manip, Type_power, conditions{j}, '/', ROI_names{roi});
%                             mkdir(ImageRenamedFolderName);
%                             info.ImageSize = [length(VecTime) 1];
%                             info.PixelDimensions = [1 1];
%                             info.raw.dim = [2 length(VecTime) 1 1 1 1 1 1];
%                             niftiwrite(V,strcat(ImageRenamedFolderName,'/', BandsPAC{band},'_PCpac_',conditions{j},'.nii'), info);
% 
%                             for ch_num = 1:length(ROIs{roi})
%                                 ImageFolderName =  strcat(generalPowerFolderName, Manip, Type_power, conditions{j}, '/', ROI_names{roi},'_',ROIs{roi}{ch_num},'_',BandsPAC{band},'_PC_spmeeg_',subjects{i},'-epo');
%                                 rmdir(strcat(ImageFolderName),'s');
%                             end
% 
%                             ImageNewFolderName = strcat(generalImagesFolderName, Manip, Type, subjects{i},'/');
%                             mkdir(ImageNewFolderName);
%                             movefile(ImageRenamedFolderName, ImageNewFolderName);
                    end
                end
                toc
            end
        end
    end
end