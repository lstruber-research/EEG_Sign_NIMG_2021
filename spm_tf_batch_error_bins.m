% List of open inputs
% Percent change TF
Manip_all={'adultes'};
Type_all = {'/postmovement/'};
generalPowerFolderName = './../Power data hilbert/pooled_baseline/';
Power_unit = 'percent'; % ou dB
BinsType = 'quantiles'; % or quantiles

for t = 1:size(Type_all,1)
    for m = 1:size(Manip_all,1)
        Manip = Manip_all{m};
        Type = Type_all{t}; % ou postmovement
        
        if strcmp(Manip,'adultes')
            subjects = {'01AC1';'02MG1';'03CC1';'04NB1';'05NF1';'06LL1';'07TJ1';'08CD1';'09MA1';'10NM1';'11LP1';'12KN1';'13LD1';'14NG1';'15OB1';'16SA1';'17GC1';'18AC1';'19BM1'};
            conditions = {'R1';'RR'};
            if strcmp(Type(1:13),'/postmovement')
                timewin = [-1500 5500];
                if(strcmp(BinsType,'quantiles')), load('Errors_bins_quantiles_post_adultes.mat');
                else, load('Errors_bins_median_post_adultes.mat');
                end
            else
                timewin = [-5500 1500];
                if(strcmp(BinsType,'quantiles')), load('Errors_bins_quantiles_pre_adultes.mat');
                else, load('Errors_bins_median_pre_adultes.mat');
                end
            end

        else
            subjects = {'01FE1';'02BN1';'03AN1';'04MC1';'05JK1';'06GC1';'07EG1';'08HM1';'09ED1';'10DT1';'11VH1';'13MC1';'14TG1';'15LG1';'16SB1';'17JL1';'18LF1';'19EV1';'20LB1';'21EF1';'22LK1';'23LM1'};
            conditions = {'R1';'RR'};
            if strcmp(Type(1:13),'/postmovement')
                timewin = [-2500 5500];
                if(strcmp(BinsType,'quantiles')), load('Errors_bins_quantiles_post_enfants.mat');
                else, load('Errors_bins_median_post_enfants.mat');
                end
            else
                timewin = [-5500 2500];
                if(strcmp(BinsType,'quantiles')), load('Errors_bins_quantiles_pre_enfants.mat');
                else, load('Errors_bins_median_pre_enfants.mat');
                end
            end

        end

        % Boucler sur tous les fichiers + toutes les conditions (postmouvement)
        if strcmp(Power_unit, 'percent')
            for i = 1:length(subjects)

                for j = 1:length(conditions)
                    PowerFolderName_allTrials = strcat(generalPowerFolderName, Manip, Type(1:end-1),'_',Power_unit,'/', conditions{j}, '/');
                    TF_File = strcat(PowerFolderName_allTrials, 'r_tf_spmeeg_', subjects{i}, '-epo.mat');
                    
                    if(strcmp(BinsType,'quantiles')), PowerFolderName_large_errors = strcat(generalPowerFolderName, Manip, Type(1:end-1),'_very_large_',Power_unit,'/', conditions{j}, '/');
                    else, PowerFolderName_large_errors = strcat(generalPowerFolderName, Manip, Type(1:end-1),'_large_',Power_unit,'/', conditions{j}, '/');
                    end
                    S_large_all.D = spm_eeg_load(TF_File);
                    S_large_all.outfile = strcat(PowerFolderName_large_errors,'_tf_spmeeg_', subjects{i}, '-epo.mat');
                    D_large_all = spm_eeg_copy(S_large_all);
                    while(LargeErrorBadEpochsMatrix{i,j}(end) > size(D_large_all,4)), LargeErrorBadEpochsMatrix{i,j}(end) = []; end
                    if(LargeErrorBadEpochsMatrix{i,j}(1) == 1), LargeErrorBadEpochsMatrix{i,j}(1) = []; end %%%% !!%%%
                    D_large_all = badtrials(D_large_all,LargeErrorBadEpochsMatrix{i,j}-1,1); %%%%%%% !!!%%
%                     D_large_all = badtrials(D_large_all,LargeErrorBadEpochsMatrix{i,j},1);
                    S_large.D = D_large_all;
                    spm_eeg_remove_bad_trials(S_large);

                    if(strcmp(BinsType,'quantiles')), PowerFolderName_small_errors = strcat(generalPowerFolderName, Manip, Type(1:end-1),'_very_small_',Power_unit,'/', conditions{j}, '/');
                    else, PowerFolderName_small_errors = strcat(generalPowerFolderName, Manip, Type(1:end-1),'_small_',Power_unit,'/', conditions{j}, '/');
                    end
                    S_small_all.D = spm_eeg_load(TF_File);
                    S_small_all.outfile = strcat(PowerFolderName_small_errors,'_tf_spmeeg_', subjects{i}, '-epo.mat');
                    D_small_all = spm_eeg_copy(S_small_all);
                    while(SmallErrorBadEpochsMatrix{i,j}(end) > size(D_small_all,4)), SmallErrorBadEpochsMatrix{i,j}(end) = []; end
                    if(SmallErrorBadEpochsMatrix{i,j}(1) == 1), SmallErrorBadEpochsMatrix{i,j}(1) = []; end %%%% !!%%%
                    D_small_all = badtrials(D_small_all,SmallErrorBadEpochsMatrix{i,j}-1,1); %%%%%%% !!!%%
%                     D_small_all = badtrials(D_small_all,SmallErrorBadEpochsMatrix{i,j},1); 
                    S_small.D = D_small_all;
                    spm_eeg_remove_bad_trials(S_small);
                end

                jobfile = {'E:\Manip EEG_sign\EEG processing\spm_tf_batch_error_bins_job_percent.m'};
                jobs = repmat(jobfile, 1, length(conditions) );
                inputs = cell(20, length(conditions) );

                for j = 1:length(conditions)
                    if(strcmp(BinsType,'quantiles')), PowerFolderName_large_errors = strcat(generalPowerFolderName, Manip, Type(1:end-1),'_very_large_',Power_unit,'/', conditions{j}, '/');
                    else, PowerFolderName_large_errors = strcat(generalPowerFolderName, Manip, Type(1:end-1),'_large_',Power_unit,'/', conditions{j}, '/');
                    end
                    if(strcmp(BinsType,'quantiles')), PowerFolderName_small_errors = strcat(generalPowerFolderName, Manip, Type(1:end-1),'_very_small_',Power_unit,'/', conditions{j}, '/');
                    else, PowerFolderName_small_errors = strcat(generalPowerFolderName, Manip, Type(1:end-1),'_small_',Power_unit,'/', conditions{j}, '/');
                    end

                    inputs{1, j} = cellstr(strcat(PowerFolderName_large_errors, 'r_tf_spmeeg_', subjects{i}, '-epo.mat')); % Averaging: File Name - cfg_files
                    inputs{2, j} = cellstr(strcat(PowerFolderName_large_errors, 'av_r_tf_spmeeg_', subjects{i}, '-epo.mat')); % Crop: File Name - cfg_files
                    inputs{3, j} = timewin; % Crop: Time window - cfg_entry
                    inputs{4, j} = cellstr(strcat(PowerFolderName_large_errors, 'c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat')); % Average over frequency: File Name - cfg_files
                    inputs{5, j} = cellstr(strcat(PowerFolderName_large_errors, 'c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat')); % Average over frequency: File Name - cfg_files
                    inputs{6, j} = cellstr(strcat(PowerFolderName_large_errors, 'c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat')); % Average over frequency: File Name - cfg_files
                    inputs{7, j} = cellstr(strcat(PowerFolderName_large_errors, 'c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat')); % Average over frequency: File Name - cfg_files
                    inputs{8, j} = cellstr(strcat(PowerFolderName_large_errors, 'av_r_tf_spmeeg_', subjects{i}, '-epo.mat')); % Delete: File Name - cfg_files
                    inputs{9, j} = cellstr(strcat(PowerFolderName_large_errors, '_tf_spmeeg_', subjects{i}, '-epo.mat')); % Delete: File Name - cfg_files
                    inputs{10, j} = cellstr(strcat(PowerFolderName_large_errors, 'r_tf_spmeeg_', subjects{i}, '-epo.mat')); % Delete: File Name - cfg_files

                    inputs{11, j} = cellstr(strcat(PowerFolderName_small_errors, 'r_tf_spmeeg_', subjects{i}, '-epo.mat')); % Averaging: File Name - cfg_files
                    inputs{12, j} = cellstr(strcat(PowerFolderName_small_errors, 'av_r_tf_spmeeg_', subjects{i}, '-epo.mat')); % Crop: File Name - cfg_files
                    inputs{13, j} = timewin; % Crop: Time window - cfg_entry
                    inputs{14, j} = cellstr(strcat(PowerFolderName_small_errors, 'c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat')); % Average over frequency: File Name - cfg_files
                    inputs{15, j} = cellstr(strcat(PowerFolderName_small_errors, 'c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat')); % Average over frequency: File Name - cfg_files
                    inputs{16, j} = cellstr(strcat(PowerFolderName_small_errors, 'c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat')); % Average over frequency: File Name - cfg_files
                    inputs{17, j} = cellstr(strcat(PowerFolderName_small_errors, 'c_av_r_tf_spmeeg_', subjects{i}, '-epo.mat')); % Average over frequency: File Name - cfg_files
                    inputs{18, j} = cellstr(strcat(PowerFolderName_small_errors, 'av_r_tf_spmeeg_', subjects{i}, '-epo.mat')); % Delete: File Name - cfg_files
                    inputs{19, j} = cellstr(strcat(PowerFolderName_small_errors, '_tf_spmeeg_', subjects{i}, '-epo.mat')); % Delete: File Name - cfg_files
                    inputs{20, j} = cellstr(strcat(PowerFolderName_small_errors, 'r_tf_spmeeg_', subjects{i}, '-epo.mat')); % Delete: File Name - cfg_files
                end

                spm('defaults', 'EEG');
                spm_jobman('run', jobs, inputs{:})
            end
        elseif strcmp(Power_unit,'dB')

        end
    end
end