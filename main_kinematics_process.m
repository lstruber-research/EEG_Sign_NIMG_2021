% Opening the file
clear all;

Manip = 'enfants'; % ou 'enfants'

if strcmp(Manip,'adultes')
	subjects = {'01AC1';'02MG1';'03CC1';'04NB1';'05NF1';'06LL1';'07TJ1';'08CD1';'09MA1';'10NM1';'11LP1';'12KN1';'13LD1';'14NG1';'15OB1';'16SA1';'17GC1';'18AC1';'19BM1'};
	conditions = {'F';'RR';'RJ';'R1';'R2'};
    Path_name = './../Raw data/Manip_EEG_2/';
    BadEpochsTable = readtable('Manip_EEG_2_bad_epochs.xlsx','Sheet','KIN','Range','A1:F25');
else
    subjects = {'01FE1';'02BN1';'03AN1';'04MC1';'05JK1';'06GC1';'07EG1';'08HM1';'09ED1';'10DT1';'11VH1';'13MC1';'14TG1';'15LG1';'16SB1';'17JL1';'18LF1';'19EV1';'20LB1';'21EF1';'22LK1';'23LM1'};
	conditions = {'F';'RR';'R1'};
    Path_name = './../Raw data/Manip_EEG_Enfants/';
    BadEpochsTable = readtable('Manip_EEG_Enfants_bad_epochs.xlsx','Sheet','KIN','Range','A1:D25');
end


ParametersNames = {'essai','Cible','deviation','dur�e essai','TR','TM','PicVTime','DevTR200','DevPicV','DistTR200','DistPicV','DistTot'};

% Files
ProcessedData = zeros(80,length(subjects),length(conditions),length(ParametersNames));
for h = 1:length(subjects)
    Subject = char(subjects(h));
    FolderName = strcat(Path_name, Subject ,'/',Subject ,'/');

    if strcmp(Manip,'adultes')
        FileNames = { 
           strcat('RAW_F_2_',Subject,'.csv');...
           strcat('RAW_RR_1_',Subject,'.csv');...
           strcat('RAW_RJ_1_',Subject,'.csv');...
           strcat('RAW_R_1_',Subject,'.csv');...  
           strcat('RAW_R_2_',Subject,'.csv')};
    else
        FileNames = { 
           strcat('RAW_F_2_',Subject,'.csv');...
           strcat('RAW_RR_1_',Subject,'.csv');...
           strcat('RAW_R_1_',Subject,'.csv')};
    end
       
    % General parameters
    for f = 1:length(FileNames)
        
        BadEpochs = str2num(BadEpochsTable{ismember(BadEpochsTable.Var1, Subject), conditions{f}}{1});
        
        % General parameters
        TargetSize = 80;
        BallSize = 20;
        F = 2048;
        T = 5; %temps max de l'essai
        dev_angle = 0;

        % Data
        Mat = dlmread(strcat(FolderName,FileNames{f}),';');
        NbCol = size(Mat,2);
        if(NbCol < F*T + 3)
            %rempli de 0 la matrice
            Mat(:,NbCol+1:F*T + 3) = zeros(size(Mat,1),length(NbCol+1:F*T + 3)); 
        end
        NbEssais = size(Mat,1)/3;
        colors = zeros(NbEssais, 3);

        % File of output of results
        
        figure1 = zeros(NbEssais,512,2);
        for e = 1:NbEssais
            % Trial
            ProcessedData(e,h,f,1) = e;
            
            if(ismember(e,BadEpochs))
                ProcessedData(e,h,f,2:end) = nan;
            else
                % Load data of trial
                Data = Mat(3*(e-1) + 1 : 3*(e-1) + 3, 4:end);
                Target = [Mat(3*(e-1) + 1,2); Mat(3*(e-1) + 2,2)]; 
                real_Target = Target;
                
                % Target number
                if (Target(1) == 550 && Target(2) == 0)
                    n_Target = 1;
                elseif (Target(1) == 388 && Target(2) == 388)
                    n_Target = 2;
                elseif (Target(1) == 0 && Target(2) == 550)
                    n_Target = 3;
                elseif (Target(1) == -388 && Target(2) == 388)
                    n_Target = 4;
                elseif (Target(1) == -550 && Target(2) == 0)
                    n_Target = 5;
                elseif (Target(1) == -388 && Target(2) == -388)
                    n_Target = 6;
                elseif (Target(1) == 0 && Target(2) == -550)
                    n_Target = 7;
                elseif (Target(1) == 388 && Target(2) == -388)
                    n_Target = 8;
                end    
                ProcessedData(e,h,f,2) = n_Target;
                
                % Downsampling
                DataSampled = zeros(3,512);
                N_avg = 20;
                FS = 2048/N_avg;
                for l = 1:3
                    DataSampled(l,:) = mean(reshape(Data(l,:),N_avg,[])); 
                end
                X = DataSampled(1,:);
                Y = DataSampled(2,:);

                % Deviation
                ProcessedData(e,h,f,3) = Mat(3*(e-1) + 1,1); 
                if ProcessedData(e,h,f,3) == 59 
                    ProcessedData(e,h,f,3) = 60;
                elseif ProcessedData(e,h,f,3) == -59 
                    ProcessedData(e,h,f,3) = -60;
                end

                % modify the target position if there is a deviation  
%                 initial_angle = 0;
% 
%                 if (real_Target(1)<0 && real_Target(2)<0 )
%                     initial_angle = 180; 
%                 elseif (real_Target(1)<= 0 && real_Target(2)>=0)
%                     initial_angle = 90;
%                 elseif (real_Target(1)>=0 && real_Target(2)<=0)
%                     initial_angle = 270; 
%                 end 
% 
%                 initial_angle = initial_angle + abs(atand(real_Target(1)/real_Target(2)));
%                 if ProcessedData(e,h,f,2) ~= 0
%                     dev_angle = initial_angle - ProcessedData(e,h,f,2);
%                     mult = 550;
%                     real_Target(1) = cosd(dev_angle)*mult ;
%                     real_Target(2) = sind(dev_angle)*mult ;
%                 else 
%                     dev_angle = initial_angle;
%                 end
                real_Target(1) = Target(1)*cosd(ProcessedData(e,h,f,3)) + Target(2)*sind(ProcessedData(e,h,f,3));
                real_Target(2) = -Target(1)*sind(ProcessedData(e,h,f,3)) + Target(2)*cosd(ProcessedData(e,h,f,3));

                % Duration of trial
                Start = 1;
                End = 512;
                for t = 1:size(DataSampled,2)
                    Dist = sqrt((real_Target(2,1) - Y(t))^2 + (real_Target(1,1) - X(t))^2);
                    if Dist <= (TargetSize/2 - BallSize/2)
                        End = t;
                        break;
                    end
                end
                if End == 512
                    DureeEssai = 5;
                else
                    DureeEssai = (End-Start)/FS;
                end
                ProcessedData(e,h,f,4) = DureeEssai;

                % Deviation 
                Deplacement = sqrt(X(Start:End).^2 + Y(Start:End).^2);
                FC = 20;
                [b,a] = butter(2,FC/(FS/2));
                Deplacement = filtfilt(b,a,Deplacement);
                Vitesse = diff(Deplacement)*FS;

                %20% pic vitesse
                [peak, locs] = findpeaks(Vitesse, 'MinPeakHeight', 200);
               

%                 %lets add in a matrice what to plot
%                 non_zeroX = find(X,1,'last');%finding the last non zero element
%                 non_zeroY = find(Y,1,'last');
%                 non_zero = max(non_zeroX,non_zeroY);
% 
%                 X(non_zero:length(X)) = X(non_zero-1);% changing zeros at the end
%                 Y(non_zero:length(X)) = Y(non_zero-1);
%                 MatRot=[ cosd(dev_angle) -sind(dev_angle);...
%                                         sind(dev_angle) cosd(dev_angle)];     
%                 NewMat=[X' Y']*MatRot; %rotation aroung the dev_angle to plot
% 
%                 figure1(e,:,:) = [NewMat(:,1) NewMat(:,2)]; % creating the matrice to plot
% 
%                 %specification of the color 
% 
%                 if strcmp(char(FileNames(f)),strcat('RAW_RR_1_',Subject,'.csv'))==0 && strcmp(char(FileNames(f)),strcat('RAW_RJ_1_',Subject,'.csv')) == 0
%                     if e>NbEssais/2
%                         colors(e,:) = [0 0 (NbEssais - e)/(NbEssais/2)] ;
%                     elseif e<=NbEssais/2
%                         colors(e,:) = [0 (((NbEssais/2)-e)/(NbEssais/2)) 1] ;
%                     end
%                 else 
%                     % add a counter for each angle so that we can shade the colors
%                     % for each angle type and see if there is any shift toward
%                     % adaptation
%                     if abs(ProcessedData(e,h,f,2)) == 20
%                         colors(e,:) = [0 0 1] ;    
%                     elseif abs(ProcessedData(e,h,f,2)) == 40
%                         colors(e,:) = [0 1 0] ; 
%                     elseif abs(ProcessedData(e,h,f,2)) == 60
%                         colors(e,:) = [1 0 0] ; 
%                     else
%                         colors(e,:) = [0 1 1] ; 
%                     end 
%                 end

                %TR200
                if isempty(peak)
                    TR = nan;
                    TM = nan;
                    Dev200 = nan;
                    IdxTR200 = nan;
                else
                    SDrest = std(Vitesse(1:15));
                    PicVitesse = peak(1);
                    PicVitesse20 = 0.2*PicVitesse;
                    
                    for t = 1:size(Vitesse,2)
                        if Vitesse(t) > 3*SDrest
                            if t == 1
                                IdxInitiation = 1;
                            else
                                IdxInitiation = t - 1;
                            end
                            break;
                        end
                    end
                    TR = IdxInitiation/FS;
                    if TR < 0.1
                        TR = nan;
                        TM = nan;
                        Dev200 = nan;
                        IdxTR200 = nan;
                    else  
                        TM = DureeEssai - TR;
                        IdxTR200 = IdxInitiation + 20;
                        if(IdxTR200 > length(X))
                            IdxTR200 = length(X);
                        end
                        % Dev TR + 200ms 
                        Pos = [X(IdxTR200) - mean(X(1:20));Y(IdxTR200)- mean(Y(1:20))];
                        cosAngle = dot(real_Target,Pos)/(norm(real_Target)*norm(Pos));
                        S = sign(real_Target(1)*Pos(2) - real_Target(2)*Pos(1));
                        Dev200 = S*acosd(cosAngle);    
                    end
                end
                
                % Reaction Time and movement time
                ProcessedData(e,h,f,5) = TR;
                ProcessedData(e,h,f,6) = TM;
             
                % Deviation and distance to the distance pic
                if isempty(locs)
                    disp(strcat('Fichier : ', FileNames{f}, ' - Essai : ', int2str(e), ' : Pas de pic de vitesse'));
                    DevPicV = nan;
                    IdxPicVitesse = nan;
                else
                    IdxPicVitesse = locs(1);
                    Pos = [X(IdxPicVitesse) - mean(X(1:20));Y(IdxPicVitesse) - mean(Y(1:20))];
                    cosAngle = dot(real_Target,Pos)/(norm(real_Target)*norm(Pos));
                    S = sign(real_Target(1)*Pos(2) - real_Target(2)*Pos(1));
                    DevPicV = S*acosd(cosAngle);
                end

                ProcessedData(e,h,f,7) = IdxPicVitesse/FS;
                ProcessedData(e,h,f,8) = Dev200;
                ProcessedData(e,h,f,9) = DevPicV;
                

                % Distance parcourue � TR200
                Dist200 = 0;
                if(isnan(IdxTR200)), IdxTR200End = 0;
                else, IdxTR200End = IdxTR200;
                end
                for t = Start+1:IdxTR200End
                    Pos = [X(t);Y(t)];
                    Dist =  sqrt((X(t)-X(t-1))^2 + (Y(t)-Y(t-1))^2)/sqrt(real_Target(2)^2+real_Target(1)^2);
                    Dist200 = Dist200 + Dist;
                end

                ProcessedData(e,h,f,10) = Dist200;

                % Distance parcourue au pic de vitesse
                DistPicV = 0;
                if(isnan(IdxPicVitesse)), IdxPicVitesseEnd = 0;
                else, IdxPicVitesseEnd = IdxPicVitesse;
                end
                for t = Start+1:IdxPicVitesseEnd
                    Pos = [X(t);Y(t)];
                    Dist =  sqrt((X(t)-X(t-1))^2 + (Y(t)-Y(t-1))^2)/sqrt(real_Target(2)^2+real_Target(1)^2);
                    DistPicV = DistPicV + Dist;
                end
                
                ProcessedData(e,h,f,11) = DistPicV;
                
                % Distance parcourue
                CumDist = 0;
                for t = Start+1:End
                    Pos = [X(t);Y(t)];
                    Pos_old = [X(t-1);Y(t-1)];
                    Dist =  sqrt((X(t)-X(t-1))^2 + (Y(t)-Y(t-1))^2)/sqrt(real_Target(2)^2+real_Target(1)^2);
                    CumDist = CumDist + Dist;
                end
                ProcessedData(e,h,f,12) = CumDist;
            end
        end
        
%         Name = char(FileNames(f));
%         h = figure('Name',Name);
%         for h=1:NbEssais          
%             plot(figure1(h,:,1),figure1(h,:,2), 'Color', colors(h,:))
%             hold on
% 
%         end
%         axis([ -500 650 -500 550])

        % Sauvegarde au format Excel
%         excelfolder = strcat('.\Manip_2\', Subject, '\');
%         trajectory_folder = strcat(excelfolder,'\Plot_trajectory\');
%         if exist(excelfolder,'dir') == 0
%             mkdir(excelfolder);
%         end
%         filename = strcat(excelfolder, FileNames{f}(5:end-4),'.xlsx');
%         xlswrite(filename,ParametersNames,1,'A1');
%         xlswrite(filename,ProcessedData,1,'A2');
% 
%         if exist(trajectory_folder,'dir') == 0
%             mkdir(trajectory_folder);
%         end
%         saveas(h,strcat(excelfolder,'\Plot_trajectory\',FileNames{f}(5:end-4),'.png'))
    end
end


