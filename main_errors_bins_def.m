% Opening the file
clear all, clc;

ParametersNames = {'essai','Cible','deviation','dur�e essai','TR','TM','PicVTime','Absolute initial angular error','DevPicV','DistTR200','DistPicV','DistTot'};
conditions = {'F';'R1';'RR';'RJ';'R2'};
color = {'g','r','b'};
Path_name = './../Kinematic processing/';
load(Path_name + "processed_kin_adultes_post (essai x sujets x conditions x parametres).mat");
load(Path_name + "processed_kin_enfants_post (essai x sujets x conditions x parametres).mat");
    
%% Error bins definition adults (MEDIAN)
Type = 'PRE'; % Ou 'POST'
subjects = {'01AC1';'02MG1';'03CC1';'04NB1';'05NF1';'06LL1';'07TJ1';'08CD1';'09MA1';'10NM1';'11LP1';'12KN1';'13LD1';'14NG1';'15OB1';'16SA1';'17GC1';'18AC1';'19BM1'};
BadEpochsTable = readtable('Manip_EEG_2_bad_epochs.xlsx','Sheet',Type,'Range','A1:D20');
if(strcmp(Type,'PRE'))
    WasFirstEpochDeleted = readtable('Manip_EEG_2_bad_epochs.xlsx','Sheet','PRE_FIRST_DELETE','Range','A1:D20');
end
for cond = 2:3
    for sub = 1:size(processed_kinematics_adultes,2)
        BadEpochs = str2num(BadEpochsTable{ismember(BadEpochsTable.Var1, subjects(sub)), conditions{cond}}{1});
        if(strcmp(Type,'PRE'))
            if(WasFirstEpochDeleted{ismember(BadEpochsTable.Var1, subjects(sub)), conditions{cond}})
                processed_kinematics_adultes(1,sub,cond,2:end) = nan;
            end
        end
        for e = 1:length(processed_kinematics_adultes(:,sub,cond,8))
            if(ismember(e,BadEpochs))
                if(strcmp(Type,'PRE'))
                    if(WasFirstEpochDeleted{ismember(BadEpochsTable.Var1, subjects(sub)), conditions{cond}})
                        processed_kinematics_adultes(e + 1,sub,cond,2:end) = nan;
                    else
                        processed_kinematics_adultes(e,sub,cond,2:end) = nan;
                    end
                else
                    processed_kinematics_adultes(e,sub,cond,2:end) = nan;
                end
            end 
        end
    end
end

SmallErrorBadEpochsMatrix = cell(size(processed_kinematics_adultes,2),2);
LargeErrorBadEpochsMatrix = cell(size(processed_kinematics_adultes,2),2);
SmallError = zeros(size(processed_kinematics_adultes,2),2);
LargeError = zeros(size(processed_kinematics_adultes,2),2);
SmallErrorMatrix = cell(size(processed_kinematics_adultes,2),2);
LargeErrorMatrix = cell(size(processed_kinematics_adultes,2),2);
SmallErrorMatrix_abs = cell(size(processed_kinematics_adultes,2),2);
LargeErrorMatrix_abs = cell(size(processed_kinematics_adultes,2),2);
IgnoredMatrix = cell(size(processed_kinematics_adultes,2),2);
IgnoredMatrix_ = cell(size(processed_kinematics_adultes,2),2);

for cond = 2:3
    for sub = 1:size(processed_kinematics_adultes,2)
        MedianValue = nanmedian(abs(processed_kinematics_adultes(:,sub,cond,8)));
        ValueHighQuantile = quantile(abs(processed_kinematics_adultes(:,sub,cond,8)),45/80);
        ValueLowQuantile = quantile(abs(processed_kinematics_adultes(:,sub,cond,8)),35/80);
        LargeErrorBadEpochsVector = [];
        LargeErrorMean = 0;
        SmallErrorBadEpochsVector = [];
        SmallErrorMean = 0;
        LargeErrorVector = [];
        SmallErrorVector = [];
        LargeErrorVector_abs = [];
        SmallErrorVector_abs = [];
        IgnoredVector = '';
        IgnoredVector_ = [];
        NbLarge = 0;
        NbSmall = 0;
        relative_e = 0;
        for e = 1:length(processed_kinematics_adultes(:,sub,cond,8))
            if(not(isnan(processed_kinematics_adultes(e,sub,cond,2))))
                relative_e = relative_e + 1;
                if(abs(processed_kinematics_adultes(e,sub,cond,8)) > ValueHighQuantile)
                    SmallErrorBadEpochsVector = [SmallErrorBadEpochsVector,  relative_e];
                    LargeErrorVector = [LargeErrorVector,relative_e];
                    LargeErrorVector_abs = [LargeErrorVector_abs,e];
                    LargeErrorMean = LargeErrorMean + abs(processed_kinematics_adultes(e,sub,cond,8));
                    NbLarge = NbLarge + 1;
                elseif(abs(processed_kinematics_adultes(e,sub,cond,8)) < ValueLowQuantile)
                    LargeErrorBadEpochsVector = [LargeErrorBadEpochsVector,  relative_e];
                    SmallErrorVector = [SmallErrorVector,relative_e];
                    SmallErrorVector_abs = [SmallErrorVector_abs,e];
                    SmallErrorMean = SmallErrorMean + abs(processed_kinematics_adultes(e,sub,cond,8));
                    NbSmall = NbSmall + 1;
                else
                    SmallErrorBadEpochsVector = [SmallErrorBadEpochsVector,  relative_e];
                    LargeErrorBadEpochsVector = [LargeErrorBadEpochsVector,  relative_e];
                end
            else
                if(strcmp(Type,'PRE'))
                    if(WasFirstEpochDeleted{ismember(BadEpochsTable.Var1, subjects(sub)), conditions{cond}})
                        IgnoredVector = [IgnoredVector ', ' num2str(e-2)];
                    else
                        IgnoredVector = [IgnoredVector ', ' num2str(e-1)];
                    end
                else
                    IgnoredVector = [IgnoredVector ', ' num2str(e-1)];
                end
                IgnoredVector_= [IgnoredVector_, e];
            end 
        end
        LargeError(sub,cond-1) = LargeErrorMean/NbLarge;
        SmallError(sub,cond-1) = SmallErrorMean/NbSmall;
        LargeErrorBadEpochsMatrix{sub,cond-1} = LargeErrorBadEpochsVector;
        SmallErrorBadEpochsMatrix{sub,cond-1} = SmallErrorBadEpochsVector;
        SmallErrorMatrix{sub,cond-1} = SmallErrorVector;
        LargeErrorMatrix{sub,cond-1} = LargeErrorVector;
        SmallErrorMatrix_abs{sub,cond-1} = SmallErrorVector_abs;
        LargeErrorMatrix_abs{sub,cond-1} = LargeErrorVector_abs;
        if(not(isempty(IgnoredVector))), IgnoredVector(1:2) = ''; end
        IgnoredMatrix{sub,cond-1} = IgnoredVector;
        IgnoredMatrix_{sub,cond-1} = IgnoredVector_;
    end
end

for cond = 2:3
    for sub = 1:size(processed_kinematics_adultes,2)
        NbTrials(sub,cond-1) = 80 - length(IgnoredMatrix_{sub,cond-1});
    end
end

AverageTM_small = zeros(size(processed_kinematics_adultes,2),2);
AverageDist_small = zeros(size(processed_kinematics_adultes,2),2);
AverageAngError_small = zeros(size(processed_kinematics_adultes,2),2);
AverageTM_large = zeros(size(processed_kinematics_adultes,2),2);
AverageDist_large = zeros(size(processed_kinematics_adultes,2),2);
AverageAngError_large = zeros(size(processed_kinematics_adultes,2),2);

for cond = 2:3
    for sub = 1:size(processed_kinematics_adultes,2)
        AverageTM_small(sub,cond - 1) = mean(processed_kinematics_adultes(SmallErrorMatrix_abs{sub,cond-1},sub,cond,6),1);
        AverageDist_small(sub,cond - 1) = mean(processed_kinematics_adultes(SmallErrorMatrix_abs{sub,cond-1},sub,cond,12),1);
        AverageAngError_small(sub,cond - 1) = mean(abs(processed_kinematics_adultes(SmallErrorMatrix_abs{sub,cond-1},sub,cond,8)),1);
        
        AverageTM_large(sub,cond - 1) = mean(processed_kinematics_adultes(LargeErrorMatrix_abs{sub,cond-1},sub,cond,6),1);
        AverageDist_large(sub,cond - 1) = mean(processed_kinematics_adultes(LargeErrorMatrix_abs{sub,cond-1},sub,cond,12),1);
        AverageAngError_large(sub,cond - 1) = mean(abs(processed_kinematics_adultes(LargeErrorMatrix_abs{sub,cond-1},sub,cond,8)),1);
    end
end

MeanAverageAngError_small(1,:) = mean(AverageAngError_small,1);
MeanAverageAngError_small(2,:) = std(AverageAngError_small,1);
MeanAverageAngError_large(1,:) = mean(AverageAngError_large,1);
MeanAverageAngError_large(2,:) = std(AverageAngError_large,1);

%% Error bins definition children -- MEDIAN
Type = 'PRE'; % Ou 'POST'
subjects = {'01FE1';'02BN1';'03AN1';'04MC1';'05JK1';'06GC1';'07EG1';'08HM1';'09ED1';'10DT1';'11VH1';'13MC1';'14TG1';'15LG1';'16SB1';'17JL1';'18LF1';'19EV1';'20LB1';'21EF1';'22LK1';'23LM1'};
BadEpochsTable = readtable('Manip_EEG_Enfants_bad_epochs.xlsx','Sheet',Type,'Range','A1:D25');
if(strcmp(Type,'PRE'))
    WasFirstEpochDeleted = readtable('Manip_EEG_Enfants_bad_epochs.xlsx','Sheet','PRE_FIRST_DELETE','Range','A1:D25');
end
for cond = 2:3
    for sub = 1:size(processed_kinematics_enfants,2)
        BadEpochs = str2num(BadEpochsTable{ismember(BadEpochsTable.Var1, subjects(sub)), conditions{cond}}{1});
        if(strcmp(Type,'PRE'))
            if(WasFirstEpochDeleted{ismember(BadEpochsTable.Var1, subjects(sub)), conditions{cond}})
                processed_kinematics_enfants(1,sub,cond,2:end) = nan;
            end
        end
        for e = 1:length(processed_kinematics_enfants(:,sub,cond,8))
            if(ismember(e,BadEpochs))
                if(strcmp(Type,'PRE'))
                    if(WasFirstEpochDeleted{ismember(BadEpochsTable.Var1, subjects(sub)), conditions{cond}})
                        processed_kinematics_enfants(e + 1,sub,cond,2:end) = nan;
                    else
                        processed_kinematics_enfants(e,sub,cond,2:end) = nan;
                    end
                else
                    processed_kinematics_enfants(e,sub,cond,2:end) = nan;
                end
            end
        end
    end
end

SmallErrorBadEpochsMatrix = cell(size(processed_kinematics_enfants,2),2);
LargeErrorBadEpochsMatrix = cell(size(processed_kinematics_enfants,2),2);
SmallError = zeros(size(processed_kinematics_enfants,2),2);
LargeError = zeros(size(processed_kinematics_enfants,2),2);
SmallErrorMatrix = cell(size(processed_kinematics_enfants,2),2);
LargeErrorMatrix = cell(size(processed_kinematics_enfants,2),2);
SmallErrorMatrix_abs = cell(size(processed_kinematics_enfants,2),2);
LargeErrorMatrix_abs = cell(size(processed_kinematics_enfants,2),2);
for cond = 2:3
    for sub = 1:size(processed_kinematics_enfants,2)
        MedianValue = nanmedian(abs(processed_kinematics_enfants(:,sub,cond,8)));
        LargeErrorBadEpochsVector = [];
        LargeErrorMean = 0;
        SmallErrorBadEpochsVector = [];
        SmallErrorMean = 0;
        LargeErrorVector = [];
        SmallErrorVector = [];
        LargeErrorVector_abs = [];
        SmallErrorVector_abs = [];
        NbLarge = 0;
        NbSmall = 0;
        relative_e = 0;
        for e = 1:length(processed_kinematics_enfants(:,sub,cond,8))
            if(not(isnan(processed_kinematics_enfants(e,sub,cond,2))))
                relative_e = relative_e + 1;
                if(abs(processed_kinematics_enfants(e,sub,cond,8)) > MedianValue)
                    SmallErrorBadEpochsVector = [SmallErrorBadEpochsVector, relative_e];
                    LargeErrorVector = [LargeErrorVector,relative_e];
                    LargeErrorVector_abs = [LargeErrorVector_abs,e];
                    LargeErrorMean = LargeErrorMean + abs(processed_kinematics_enfants(e,sub,cond,8));
                    NbLarge = NbLarge + 1;
                else
                    LargeErrorBadEpochsVector = [LargeErrorBadEpochsVector, relative_e];
                    SmallErrorVector = [SmallErrorVector,relative_e];
                    SmallErrorVector_abs = [SmallErrorVector_abs,e];
                    SmallErrorMean = SmallErrorMean + abs(processed_kinematics_enfants(e,sub,cond,8));
                    NbSmall = NbSmall + 1;
                end
            end 
        end
        
        LargeError(sub,cond-1) = LargeErrorMean/NbLarge;
        SmallError(sub,cond-1) = SmallErrorMean/NbSmall;
        LargeErrorBadEpochsMatrix{sub,cond-1} = LargeErrorBadEpochsVector;
        SmallErrorBadEpochsMatrix{sub,cond-1} = SmallErrorBadEpochsVector;
        SmallErrorMatrix{sub,cond-1} = SmallErrorVector;
        LargeErrorMatrix{sub,cond-1} = LargeErrorVector;
        SmallErrorMatrix_abs{sub,cond-1} = SmallErrorVector_abs;
        LargeErrorMatrix_abs{sub,cond-1} = LargeErrorVector_abs;
    end
end

AverageTM_small = zeros(size(processed_kinematics_enfants,2),2);
AverageDist_small = zeros(size(processed_kinematics_enfants,2),2);
AverageAngError_small = zeros(size(processed_kinematics_enfants,2),2);
AverageTM_large = zeros(size(processed_kinematics_enfants,2),2);
AverageDist_large = zeros(size(processed_kinematics_enfants,2),2);
AverageAngError_large = zeros(size(processed_kinematics_enfants,2),2);

for cond = 2:3
    for sub = 1:size(processed_kinematics_enfants,2)
        AverageTM_small(sub,cond - 1) = mean(processed_kinematics_enfants(SmallErrorMatrix_abs{sub,cond-1},sub,cond,6),1);
        AverageDist_small(sub,cond - 1) = mean(processed_kinematics_enfants(SmallErrorMatrix_abs{sub,cond-1},sub,cond,12),1);
        AverageAngError_small(sub,cond - 1) = mean(abs(processed_kinematics_enfants(SmallErrorMatrix_abs{sub,cond-1},sub,cond,8)),1);
        
        AverageTM_large(sub,cond - 1) = mean(processed_kinematics_enfants(LargeErrorMatrix_abs{sub,cond-1},sub,cond,6),1);
        AverageDist_large(sub,cond - 1) = mean(processed_kinematics_enfants(LargeErrorMatrix_abs{sub,cond-1},sub,cond,12),1);
        AverageAngError_large(sub,cond - 1) = mean(abs(processed_kinematics_enfants(LargeErrorMatrix_abs{sub,cond-1},sub,cond,8)),1);
    end
end

MeanAverageAngError_small(1,:) = mean(AverageAngError_small,1);
MeanAverageAngError_small(2,:) = std(AverageAngError_small,1);
MeanAverageAngError_large(1,:) = mean(AverageAngError_large,1);
MeanAverageAngError_large(2,:) = std(AverageAngError_large,1);

%% Exec bins definition adults
Type = 'PRE'; % Ou 'POST'
subjects = {'01AC1';'02MG1';'03CC1';'04NB1';'05NF1';'06LL1';'07TJ1';'08CD1';'09MA1';'10NM1';'11LP1';'12KN1';'13LD1';'14NG1';'15OB1';'16SA1';'17GC1';'18AC1';'19BM1'};
BadEpochsTable = readtable('Manip_EEG_2_bad_epochs.xlsx','Sheet',Type,'Range','A1:F20');
if(strcmp(Type,'PRE'))
    WasFirstEpochDeleted = readtable('Manip_EEG_2_bad_epochs.xlsx','Sheet','PRE_FIRST_DELETE','Range','A1:F20');
end
for cond = 1:5
    for sub = 1:size(processed_kinematics_adultes,2)
        BadEpochs = str2num(BadEpochsTable{ismember(BadEpochsTable.Var1, subjects(sub)), conditions{cond}}{1});
        if(strcmp(Type,'PRE'))
            if(WasFirstEpochDeleted{ismember(BadEpochsTable.Var1, subjects(sub)), conditions{cond}})
                processed_kinematics_adultes(1,sub,cond,2:end) = nan;
            end
        end
        for e = 1:length(processed_kinematics_adultes(:,sub,cond,8))
            if(ismember(e,BadEpochs))
                if(strcmp(Type,'PRE'))
                    if(WasFirstEpochDeleted{ismember(BadEpochsTable.Var1, subjects(sub)), conditions{cond}})
                        processed_kinematics_adultes(e + 1,sub,cond,2:end) = nan;
                    else
                        processed_kinematics_adultes(e,sub,cond,2:end) = nan;
                    end
                else
                    processed_kinematics_adultes(e,sub,cond,2:end) = nan;
                end
            end 
        end
    end
end

LastErrorBadEpochsMatrix = cell(size(processed_kinematics_adultes,2),2);
FirstErrorBadEpochsMatrix = cell(size(processed_kinematics_adultes,2),2);
LastError = zeros(size(processed_kinematics_adultes,2),2);
FirstError = zeros(size(processed_kinematics_adultes,2),2);
LastErrorMatrix = cell(size(processed_kinematics_adultes,2),2);
FirstErrorMatrix = cell(size(processed_kinematics_adultes,2),2);
LastErrorMatrix_abs = cell(size(processed_kinematics_adultes,2),2);
FirstErrorMatrix_abs = cell(size(processed_kinematics_adultes,2),2);

for cond = 1:5
    for sub = 1:size(processed_kinematics_adultes,2)
        MedianValue = nanmedian(abs(processed_kinematics_adultes(:,sub,cond,8)));
        FirstErrorBadEpochsVector = [];
        FirstErrorMean = 0;
        LastErrorBadEpochsVector = [];
        LastErrorMean = 0;
        FirstErrorVector = [];
        LastErrorVector = [];
        FirstErrorVector_abs = [];
        LastErrorVector_abs = [];
        NbLast = 0;
        NbFirst = 0;
        relative_e = 0;
        for e = 1:length(processed_kinematics_adultes(:,sub,cond,8))
            if(not(isnan(processed_kinematics_adultes(e,sub,cond,2))))
                relative_e = relative_e + 1;
                if(e < 47)
                    LastErrorBadEpochsVector = [LastErrorBadEpochsVector,  relative_e];
                    FirstErrorVector = [FirstErrorVector,relative_e];
                    FirstErrorVector_abs = [FirstErrorVector_abs,e];
                    FirstErrorMean = FirstErrorMean + abs(processed_kinematics_adultes(e,sub,cond,8));
                    NbFirst = NbFirst + 1;
                end
                if(e > 33)
                    FirstErrorBadEpochsVector = [FirstErrorBadEpochsVector,  relative_e];
                    LastErrorVector = [LastErrorVector,relative_e];
                    LastErrorVector_abs = [LastErrorVector_abs,e];
                    LastErrorMean = LastErrorMean + abs(processed_kinematics_adultes(e,sub,cond,8));
                    NbLast = NbLast + 1;
                end
            end 
        end
        FirstError(sub,cond) = FirstErrorMean/NbFirst;
        LastError(sub,cond) = LastErrorMean/NbLast;
        FirstErrorBadEpochsMatrix{sub,cond} = FirstErrorBadEpochsVector;
        LastErrorBadEpochsMatrix{sub,cond} = LastErrorBadEpochsVector;
        LastErrorMatrix{sub,cond} = LastErrorVector;
        FirstErrorMatrix{sub,cond} = FirstErrorVector;
        LastErrorMatrix_abs{sub,cond} = LastErrorVector_abs;
        FirstErrorMatrix_abs{sub,cond} = FirstErrorVector_abs;
    end
end

AverageTM_last = zeros(size(processed_kinematics_adultes,2),2);
AverageDist_last = zeros(size(processed_kinematics_adultes,2),2);
AverageAngError_last = zeros(size(processed_kinematics_adultes,2),2);
AverageTM_first = zeros(size(processed_kinematics_adultes,2),2);
AverageDist_first = zeros(size(processed_kinematics_adultes,2),2);
AverageAngError_first = zeros(size(processed_kinematics_adultes,2),2);

for cond = 1:3
    for sub = 1:size(processed_kinematics_adultes,2)
        AverageTM_last(sub,cond) = mean(processed_kinematics_adultes(LastErrorMatrix_abs{sub,cond},sub,cond,6),1);
        AverageDist_last(sub,cond) = mean(processed_kinematics_adultes(LastErrorMatrix_abs{sub,cond},sub,cond,12),1);
        AverageAngError_last(sub,cond) = mean(abs(processed_kinematics_adultes(LastErrorMatrix_abs{sub,cond},sub,cond,8)),1);
        
        AverageTM_first(sub,cond) = mean(processed_kinematics_adultes(FirstErrorMatrix_abs{sub,cond},sub,cond,6),1);
        AverageDist_first(sub,cond) = mean(processed_kinematics_adultes(FirstErrorMatrix_abs{sub,cond},sub,cond,12),1);
        AverageAngError_first(sub,cond) = mean(abs(processed_kinematics_adultes(FirstErrorMatrix_abs{sub,cond},sub,cond,8)),1);
    end
end

MeanAverageAngError_last(1,:) = mean(AverageAngError_last,1);
MeanAverageAngError_last(2,:) = std(AverageAngError_last,1);
MeanAverageAngError_first(1,:) = mean(AverageAngError_first,1);
MeanAverageAngError_first(2,:) = std(AverageAngError_first,1);

%% Exec bins definition enfants
Type = 'POST'; % Ou 'POST'
subjects = {'01FE1';'02BN1';'03AN1';'04MC1';'05JK1';'06GC1';'07EG1';'08HM1';'09ED1';'10DT1';'11VH1';'13MC1';'14TG1';'15LG1';'16SB1';'17JL1';'18LF1';'19EV1';'20LB1';'21EF1';'22LK1';'23LM1'};
BadEpochsTable = readtable('Manip_EEG_Enfants_bad_epochs.xlsx','Sheet',Type,'Range','A1:D25');
if(strcmp(Type,'PRE'))
    WasFirstEpochDeleted = readtable('Manip_EEG_Enfants_bad_epochs.xlsx','Sheet','PRE_FIRST_DELETE','Range','A1:D25');
end
for cond = 1:3
    for sub = 1:size(processed_kinematics_enfants,2)
        BadEpochs = str2num(BadEpochsTable{ismember(BadEpochsTable.Var1, subjects(sub)), conditions{cond}}{1});
        if(strcmp(Type,'PRE'))
            if(WasFirstEpochDeleted{ismember(BadEpochsTable.Var1, subjects(sub)), conditions{cond}})
                processed_kinematics_enfants(1,sub,cond,2:end) = nan;
            end
        end
        for e = 1:length(processed_kinematics_enfants(:,sub,cond,8))
            if(ismember(e,BadEpochs))
                if(strcmp(Type,'PRE'))
                    if(WasFirstEpochDeleted{ismember(BadEpochsTable.Var1, subjects(sub)), conditions{cond}})
                        processed_kinematics_enfants(e + 1,sub,cond,2:end) = nan;
                    else
                        processed_kinematics_enfants(e,sub,cond,2:end) = nan;
                    end
                else
                    processed_kinematics_enfants(e,sub,cond,2:end) = nan;
                end
            end 
        end
    end
end

LastErrorBadEpochsMatrix = cell(size(processed_kinematics_enfants,2),2);
FirstErrorBadEpochsMatrix = cell(size(processed_kinematics_enfants,2),2);
LastError = zeros(size(processed_kinematics_enfants,2),2);
FirstError = zeros(size(processed_kinematics_enfants,2),2);
LastErrorMatrix = cell(size(processed_kinematics_enfants,2),2);
FirstErrorMatrix = cell(size(processed_kinematics_enfants,2),2);
LastErrorMatrix_abs = cell(size(processed_kinematics_enfants,2),2);
FirstErrorMatrix_abs = cell(size(processed_kinematics_enfants,2),2);

for cond = 1:3
    for sub = 1:size(processed_kinematics_enfants,2)
        MedianValue = nanmedian(abs(processed_kinematics_enfants(:,sub,cond,8)));
        FirstErrorBadEpochsVector = [];
        FirstErrorMean = 0;
        LastErrorBadEpochsVector = [];
        LastErrorMean = 0;
        FirstErrorVector = [];
        LastErrorVector = [];
        FirstErrorVector_abs = [];
        LastErrorVector_abs = [];
        NbLast = 0;
        NbFirst = 0;
        relative_e = 0;
        for e = 1:length(processed_kinematics_enfants(:,sub,cond,8))
            if(not(isnan(processed_kinematics_enfants(e,sub,cond,2))))
                relative_e = relative_e + 1;
                if(e < 47)
                    LastErrorBadEpochsVector = [LastErrorBadEpochsVector,  relative_e];
                    FirstErrorVector = [FirstErrorVector,relative_e];
                    FirstErrorVector_abs = [FirstErrorVector_abs,e];
                    FirstErrorMean = FirstErrorMean + abs(processed_kinematics_enfants(e,sub,cond,8));
                    NbFirst = NbFirst + 1;
                end
                if(e > 33)
                    FirstErrorBadEpochsVector = [FirstErrorBadEpochsVector,  relative_e];
                    LastErrorVector = [LastErrorVector,relative_e];
                    LastErrorVector_abs = [LastErrorVector_abs,e];
                    LastErrorMean = LastErrorMean + abs(processed_kinematics_enfants(e,sub,cond,8));
                    NbLast = NbLast + 1;
                end
            end 
        end
        FirstError(sub,cond) = FirstErrorMean/NbFirst;
        LastError(sub,cond) = LastErrorMean/NbLast;
        FirstErrorBadEpochsMatrix{sub,cond} = FirstErrorBadEpochsVector;
        LastErrorBadEpochsMatrix{sub,cond} = LastErrorBadEpochsVector;
        LastErrorMatrix{sub,cond} = LastErrorVector;
        FirstErrorMatrix{sub,cond} = FirstErrorVector;
        LastErrorMatrix_abs{sub,cond} = LastErrorVector_abs;
        FirstErrorMatrix_abs{sub,cond} = FirstErrorVector_abs;
    end
end

AverageTM_last = zeros(size(processed_kinematics_enfants,2),2);
AverageDist_last = zeros(size(processed_kinematics_enfants,2),2);
AverageAngError_last = zeros(size(processed_kinematics_enfants,2),2);
AverageTM_first = zeros(size(processed_kinematics_enfants,2),2);
AverageDist_first = zeros(size(processed_kinematics_enfants,2),2);
AverageAngError_first = zeros(size(processed_kinematics_enfants,2),2);

for cond = 1:3
    for sub = 1:size(processed_kinematics_enfants,2)
        AverageTM_last(sub,cond) = mean(processed_kinematics_enfants(LastErrorMatrix_abs{sub,cond},sub,cond,6),1);
        AverageDist_last(sub,cond) = mean(processed_kinematics_enfants(LastErrorMatrix_abs{sub,cond},sub,cond,12),1);
        AverageAngError_last(sub,cond) = mean(abs(processed_kinematics_enfants(LastErrorMatrix_abs{sub,cond},sub,cond,8)),1);
        
        AverageTM_first(sub,cond) = mean(processed_kinematics_enfants(FirstErrorMatrix_abs{sub,cond},sub,cond,6),1);
        AverageDist_first(sub,cond) = mean(processed_kinematics_enfants(FirstErrorMatrix_abs{sub,cond},sub,cond,12),1);
        AverageAngError_first(sub,cond) = mean(abs(processed_kinematics_enfants(FirstErrorMatrix_abs{sub,cond},sub,cond,8)),1);
    end
end

MeanAverageAngError_last(1,:) = mean(AverageAngError_last,1);
MeanAverageAngError_last(2,:) = std(AverageAngError_last,1);
MeanAverageAngError_first(1,:) = mean(AverageAngError_first,1);
MeanAverageAngError_first(2,:) = std(AverageAngError_first,1);