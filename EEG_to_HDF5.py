# -*- coding: utf-8 -*-
"""
Created on Mon Dec 14 14:43:39 2020

@author: ODaniel
"""
from Preprocessing import *
import mne
from numpy import ndarray, unique, diff , int64 , float64
import h5py 
import glob 
import os



""" 
Routines de sauvegarde hdf5
"""
def depth(d, level=1):
    if not isinstance(d, dict) or not d:
        return level
    return max(depth(d[k], level + 1) for k in d)
 
    
def save_dict_to_hdf5(dic, filename):
    """
    ....
    """
    with h5py.File(filename, 'w') as h5file:
        recursively_save_dict_contents_to_group(h5file, '/', dic)

def recursively_save_dict_contents_to_group(h5file, path, dic):
    """
    ....
    """
    for key, item in dic.items():
        if isinstance(key, int) :
            key = str(key)
      
        if isinstance(item, list) :
            item =item[0]

        if isinstance(item, (ndarray, int64, float64, float, int, str, bytes)):
            h5file[path+key] = item
        elif isinstance(item, dict):
            recursively_save_dict_contents_to_group(h5file, path + key + '/', item)
        else:
            raise ValueError('Cannot save %s type'%type(item))

def load_dict_from_hdf5(filename):
    """
    ....
    """
    with h5py.File(filename, 'r') as h5file:
        return recursively_load_dict_contents_from_group(h5file, '/')

def recursively_load_dict_contents_from_group(h5file, path):
    """
    ....
    """
    ans = {}
    for key, item in h5file[path].items():
        if isinstance(item, h5py._hl.dataset.Dataset):
            ans[key] = item.value
        elif isinstance(item, h5py._hl.group.Group):
            ans[key] = recursively_load_dict_contents_from_group(h5file, path + key + '/')
    return ans

""" FIN ROUTINES HDF5"""



""" 
LECTURE ET SAUVEGARDE HDF5
    
 1°\ lecture des fichiers fif (il peut y en avoir plusieurs)
 2°\ sauvegarde sous forme de dictionnaires
 
"""

filelist = glob.glob('E:\\Manip EEG_sign\\Epoched data\\adultes\\postmovement\\F\\02MG1-epo.fif') # liste tous le fichiers fif dasn un dossier
# filelist = glob.glob('E:\\Manip EEG_sign\\Raw data\\Manip_EEG_Enfants\\02BN1\\02BN1_EEG\\02BN1-F.bdf')
# fichier de sortie
fichout ="02MG1-F-epo.h5"
# chemin de sortie
pathout ="E:\\Manip EEG_sign\\Epoched data\\adultes\\postmovement\\R1\\"
# pathout = "E:\\Manip EEG_sign\\Raw data\\Manip_EEG_Enfants\\02BN1\\02BN1_EEG\\"

D =dict()

for f in filelist :
    # on nomme la cle par le nom du fichier
    key = f.split("\\")[-1].split(".fif")[0]
    # init nouvelle arborescence
    D[key] =dict()    

    # lecture du fichier
    # raw = mne.io.read_raw_fif(f)
    raw = mne.read_epochs(f)
    # raw = biosemi_prep('02BN1', 'F', 'children')
    
    # creation de cles pour chaque donnée qu'on veut exporter
    D[key]["data"] = raw.get_data()
    D[key]["channel_names"]= raw.ch_names
    D[key]["time"] = raw.times
    D[key]["fe"] = 1.0/unique(diff(raw.times))
    #@lucas  si tu veux rajouter des évennements ou autre ...
    

# verification du chemein de sauvegarde (création si besoin))
if not os.path.exists(pathout) :
        os.makedirs(pathout)

# sauvegarde au format hdf5 
save_dict_to_hdf5(D, pathout+"\\"+fichout)

