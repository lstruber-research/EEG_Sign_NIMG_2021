%% GLOBAL BETA POWER PLOT
% close all;
Manip = 'adultes'; % ou 'enfants'
generalPowerFolderName = './../Power data hilbert/pooled_baseline/';
Type = 'postmovement'; % ou postmovement
PowerUnit = 'percent';
FolderType = strcat('/',Type,'_',PowerUnit,'/');
SaveFigs = false;
FigsFolderName = './../Présentations/Figs/EEG/';

if strcmp(Manip,'adultes')
	conditions = {'R1'};
    if strcmp(Type(1:4),'post')
        timewin = [-200 2500]; % Global : -1500:5:5500
        timevec = timewin(1):5:timewin(2);
        yvec = (timevec+1500)/5 + 1;
    else
        timewin = [-4000 1500]; % Global : -5500:5:1500
        timevec = timewin(1):5:timewin(2);
        yvec = (timevec+5500)/5 + 1;
    end
    beta_scale = 50;
else
    conditions = {'F';'R1';'RR'};
    if strcmp(Type(1:4),'post')
        timewin = [-2500 3500]; % Global : -2500:5:5500
        timevec = timewin(1):5:timewin(2);
        yvec = (timevec+2500)/5 + 1;
    else
        timewin = [-4000 2500]; % Global : -5500:5:2500
        timevec = timewin(1):5:timewin(2);
        yvec = (timevec+5500)/5 + 1;
    end
    beta_scale = 25;
end

ElecVector = [1:6,19,110:115,124];
figure();
D_glob = zeros(128,1401,1);
for cond =1:length(conditions)
    Beta_File = strcat(generalPowerFolderName, Manip, FolderType, conditions{cond}, '/_', FolderType(2:end-1),'_',conditions{cond},'_AverageBetaPower.mat');
    D = spm_eeg_load(Beta_File);
    D_glob = D_glob + D(:,:,:);
end
D_glob = D_glob/length(conditions);

plot(timevec,permute(mean(D_glob(ElecVector,yvec,1),1),[2,3,1]),'r','LineWidth',5); % 113 = D17
axis([timewin(1) timewin(end) -beta_scale beta_scale]);

if(SaveFigs)
    savefig(strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_Global_Beta.fig'));
    saveas(gcf,strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_Global_Beta.png'));
end

%% PLOT per conditions
close all;
Manip = 'enfants'; % ou 'enfants'
generalPowerFolderName = './../Power data hilbert/pooled_baseline/';
Type = 'postmovement'; % ou postmovement
PowerUnit = 'percent';
FolderType = strcat('/',Type,'_',PowerUnit,'/');
SaveFigs = false;
FigsFolderName = './../Présentations/Figs/EEG/';

if strcmp(Manip,'adultes')
% 	conditions = {'F';'RR';'RJ';'R1';'R2'};
    conditions = {'F';'RR';'R1'};
    if strcmp(Type(1:4),'post')
        timewin = [-200 2500]; % Global : -1500:5:5500
        timevec = timewin(1):5:timewin(2);
        yvec = (timevec+1500)/5 + 1;
    else
        timewin = [-2000 700]; % Global : -5500:5:1500
        timevec = timewin(1):5:timewin(2);
        yvec = (timevec+5500)/5 + 1;
    end
    tf_scale = 80;
%     beta_scale = 50;
%     alpha_scale = 80;
%     theta_scale = 70;
%     gamma_scale = 30;
else
    conditions = {'F';'RR';'R1'};
    if strcmp(Type(1:4),'post')
        timewin = [-200 2500]; % Global : -2500:5:5500
        timevec = timewin(1):5:timewin(2);
        yvec = (timevec+2500)/5 + 1;
    else
        timewin = [-1500 0]; % Global : -5500:5:2500
        timevec = timewin(1):5:timewin(2);
        yvec = (timevec+5500)/5 + 1;
    end
    tf_scale = 30;
    beta_scale = 20;
    alpha_scale = 30;
    theta_scale = 50;
    gamma_scale = 20;
end

ElecVector = 64 + [11,12,13,20,21,22,24,25,26];

for cond =1:length(conditions)
    TF_File = strcat(generalPowerFolderName, Manip, FolderType, conditions{cond}, '/_', FolderType(2:end-1),'_',conditions{cond},'_AverageGlobalTF.mat');
    D = spm_eeg_load(TF_File);
    figure();
    imagesc(timevec,4:81,permute(mean(D(ElecVector,:,yvec,1),1),[2,3,1,4])); % 113 = D17
    axis normal xy;
    colormap jet;
    colorbar;
    ylabel('frequency (in Hz)');
    if(strcmp(Type(1:4),'post')), xlabel('time (in ms after mvt offset)');
    else, xlabel('time (in ms after mvt onset)'); end
    
    title(conditions{cond});
    caxis([-tf_scale tf_scale]);
    if(SaveFigs)
        savefig(strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_',conditions{cond},'_TF.fig'));
        saveas(gcf,strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_',conditions{cond},'_TF.png'));
    end
end

figure();
hold on;
ElecVector = [75,76,77,84,85,86,88,89,90]; % SMA
for cond =1:length(conditions)
    Beta_File = strcat(generalPowerFolderName, Manip, FolderType, conditions{cond}, '/_', FolderType(2:end-1),'_',conditions{cond},'_AverageBetaPower.mat');
    D = spm_eeg_load(Beta_File);
    plot(timevec,permute(mean(D(ElecVector,yvec,1),1),[2,3,1])); % 113 = D17
%     axis([timewin(1) timewin(end) -beta_scale beta_scale]);
    ylabel('power change (in %)');
    if(strcmp(Type(1:4),'post')), xlabel('time (in ms after mvt offset)');
    else, xlabel('time (in ms after mvt onset)'); end
    title('Beta');
    
end

% if(strcmp(Type(1:4),'post')), legend(conditions{1},conditions{2},conditions{3},conditions{4},conditions{5},'Location','southeast');
% else, legend(conditions{1},conditions{2},conditions{3},conditions{4},conditions{5}); end

hold off;
if(SaveFigs)
    savefig(strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_Beta.fig'));
    saveas(gcf,strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_Beta.png'));
end

% figure();
% hold on;
% for cond =1:length(conditions)
%     Beta_File = strcat(generalPowerFolderName, Manip, FolderType, conditions{cond}, '/_', FolderType(2:end-1),'_',conditions{cond},'_AverageAlphaPower.mat');
%     D = spm_eeg_load(Beta_File);
%     plot(timevec,permute(mean(D(ElecVector,yvec,1),1),[2,3,1])); % 113 = D17
% %     axis([timewin(1) timewin(end) -alpha_scale alpha_scale]);
%     ylabel('power change (in %)');
%     if(strcmp(Type(1:4),'post')), xlabel('time (in ms after mvt offset)');
%     else, xlabel('time (in ms after mvt onset)'); end
%     title('Alpha');
%     
% end
% if(strcmp(Type(1:4),'post')), legend(conditions{1},conditions{2},conditions{3},conditions{4},conditions{5},'Location','southeast');
% else, legend(conditions{1},conditions{2},conditions{3},conditions{4},conditions{5}); end
% 
% hold off;
% if(SaveFigs)
%     savefig(strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_Alpha.fig'));
%     saveas(gcf,strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_Alpha.png'));
% end

figure();
hold on;
ElecVector = [1,97,98,108,109,110,111,114,115,116,87]; % LM
for cond =1:length(conditions)
    Gamma_File = strcat(generalPowerFolderName, Manip, FolderType, conditions{cond}, '/_', FolderType(2:end-1),'_',conditions{cond},'_AverageGammaPower.mat');
    D = spm_eeg_load(Gamma_File);
    plot(timevec,permute(mean(D(ElecVector,yvec,1),1),[2,3,1])); % 113 = D17
%     axis([timewin(1) timewin(end) -gamma_scale gamma_scale]);
    ylabel('power change (in %)');
    if(strcmp(Type(1:4),'post')), xlabel('time (in ms after mvt offset)');
    else, xlabel('time (in ms after mvt onset)'); end
    title('Gamma');
end
% if(strcmp(Type(1:4),'post')), legend(conditions{1},conditions{2},conditions{3},conditions{4},conditions{5},'Location','southeast');
% else, legend(conditions{1},conditions{2},conditions{3},conditions{4},conditions{5}); end
% legend(conditions{1},conditions{2},conditions{3});
hold off;
if(SaveFigs)
    savefig(strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_Gamma.fig'));
    saveas(gcf,strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_Gamma.png'));
end

figure();
hold on;
ElecVector = [72,73,74,78,79,80,81,82,83,91,92,93,94,95,96]; % FP
for cond =1:length(conditions)
    Beta_File = strcat(generalPowerFolderName, Manip, FolderType, conditions{cond}, '/_', FolderType(2:end-1),'_',conditions{cond},'_AverageThetaPower.mat');
    D = spm_eeg_load(Beta_File);
    plot(timevec,permute(mean(D(ElecVector,yvec,1),1),[2,3,1])); % 113 = D17
%     axis([timewin(1) timewin(end) -theta_scale theta_scale]);
    ylabel('power change (in %)');
    if(strcmp(Type(1:4),'post')), xlabel('time (in ms after mvt offset)');
    else, xlabel('time (in ms after mvt onset)'); end
    title('Theta');
    
end
% if(strcmp(Type(1:4),'post')), legend(conditions{1},conditions{2},conditions{3},conditions{4},conditions{5},'Location','southeast');
% else, legend(conditions{1},conditions{2},conditions{3},conditions{4},conditions{5}); end

hold off;
if(SaveFigs)
    savefig(strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_Theta.fig'));
    saveas(gcf,strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_Theta.png'));
end

%% PLOT per execution order bins
close all;
Manip = 'enfants'; % ou 'enfants'
generalPowerFolderName = './../Power data hilbert/pooled_baseline/';
Type = 'postmovement'; % ou postmovement
PowerUnit = 'percent';
FolderType = strcat('/',Type,'_',PowerUnit,'/');
FolderType_first = strcat('/',Type,'_first_',PowerUnit,'/');
FolderType_last = strcat('/',Type,'_last_',PowerUnit,'/');
SaveFigs = false;
FigsFolderName = './../Présentations/Figs/EEG/';
ROI = 'SMA';

if strcmp(Manip,'adultes')
	conditions = {'R'};
    if strcmp(Type(1:4),'post')
        timewin = [-200 2500]; % Global : -1500:5:5500
        timevec = timewin(1):5:timewin(2);
        yvec = (timevec+1500)/5 + 1;
    else
        timewin = [-2000 700]; % Global : -5500:5:1500
        timevec = timewin(1):5:timewin(2);
        yvec = (timevec+5500)/5 + 1;
    end
    tf_scale = 80;
    beta_scale = 50;
    alpha_scale = 90;
    theta_scale = 50;
    gamma_scale = 30;
else
    conditions = {'R'};
    if strcmp(Type(1:4),'post')
        timewin = [-200 2500]; % Global : -2500:5:5500
        timevec = timewin(1):5:timewin(2);
        yvec = (timevec+2500)/5 + 1;
    else
        timewin = [-4000 2500]; % Global : -5500:5:2500
        timevec = timewin(1):5:timewin(2);
        yvec = (timevec+5500)/5 + 1;
    end
    tf_scale = 30;
    beta_scale = 25;
    alpha_scale = 40;
    theta_scale = 40;
    gamma_scale = 20;
end

if(strcmp(ROI,'SMA'))
    ElecVector = 64 + [11,12,13,20,21,22,24,25,26];
elseif(strcmp(ROI,'FP'))
    ElecVector = [59:62, 67:86, 88:96, 99:107];
elseif(strcmp(ROI,'LM'))
    ElecVector = [1,97,98,108,109,110,111,114,115,116,87];
end

for cond =1:length(conditions)
    TF_File = strcat(generalPowerFolderName, Manip, FolderType_first, conditions{cond}, '1/_', FolderType_first(2:end-1),'_',conditions{cond},'1_AverageGlobalTF.mat');
    D = spm_eeg_load(TF_File);
    figure();
    imagesc(timevec,4:81,permute(mean(D(ElecVector,:,yvec,1),1),[2,3,1,4])); % 113 = D17
    axis normal xy;
    colormap jet;
    colorbar;
    ylabel('frequency (in Hz)');
    if(strcmp(Type(1:4),'post')), xlabel('time (in ms after mvt offset)');
    else, xlabel('time (in ms after mvt onset)'); end
    title(strcat(conditions{cond},': first trials'));
    caxis([-tf_scale tf_scale]);
    if contains(TF_File,'PH'), caxis([0 0.3]); end
    if(SaveFigs)
        savefig(strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_',conditions{cond},'_first_TF.fig'));
        saveas(gcf,strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_',conditions{cond},'_first_TF.png'));
    end
    
%     TF_File = strcat(generalPowerFolderName, Manip, FolderType_last, conditions{cond}, '2/_', FolderType_last(2:end-1),'_',conditions{cond},'2_AverageGlobalTF.mat');
    TF_File = strcat(generalPowerFolderName, Manip, FolderType_last, conditions{cond}, '1/_', FolderType_last(2:end-1),'_',conditions{cond},'1_AverageGlobalTF.mat');
    D = spm_eeg_load(TF_File);
    figure();
    imagesc(timevec,4:81,permute(mean(D(ElecVector,:,yvec,1),1),[2,3,1,4])); % 113 = D17
    axis normal xy;
    colormap jet;
    colorbar;
    ylabel('frequency (in Hz)');
    if(strcmp(Type(1:4),'post')), xlabel('time (in ms after mvt offset)');
    else, xlabel('time (in ms after mvt onset)'); end
    title(strcat(conditions{cond},': last trials'));
    caxis([-tf_scale tf_scale]);
    if contains(TF_File,'PH'),caxis([0 0.3]); end
    if(SaveFigs)
        savefig(strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_',conditions{cond},'_last_TF.fig'));
        saveas(gcf,strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_',conditions{cond},'_last_TF.png'));
    end
end

figure();
% subplot(3,1,2)
hold on;
LineStyles = {'-.';'-'};
LineColors = [[0.8550 0.0780 0.0740]; [0.4660 0.6740 0.1880]];
LW = 2;
% ElecVector = [4,19,20,21,30,31,32,35,36,37,38,42,43,44,45]; % RP
% ElecVector = [72,73,74,78,79,80,81,82,83,91,92,93,94,95,96]; % FP
ElecVector = [75,76,77,84,85,86,88,89,90]; % SMA
% ElecVector = [1,97,98,108,109,110,111,114,115,116,87]; % LM
for cond =1:length(conditions) 
    Beta_File = strcat(generalPowerFolderName, Manip, FolderType_first, conditions{cond}, '1/_', FolderType_first(2:end-1),'_',conditions{cond},'1_AverageBetaPower.mat');
    D = spm_eeg_load(Beta_File);
    plot(timevec,permute(mean(D(ElecVector,yvec,1),1),[2,3,1]),'Color',LineColors(1,:),'LineWidth',LW); % 113 = D17

%     Beta_File = strcat(generalPowerFolderName, Manip, FolderType_last, conditions{cond}, '2/_', FolderType_last(2:end-1),'_',conditions{cond},'2_AverageBetaPower.mat');
    Beta_File = strcat(generalPowerFolderName, Manip, FolderType_last, conditions{cond}, '1/_', FolderType_last(2:end-1),'_',conditions{cond},'1_AverageBetaPower.mat');
    D = spm_eeg_load(Beta_File);
    plot(timevec,permute(mean(D(ElecVector,yvec,1),1),[2,3,1]),'Color',LineColors(2,:),'LineWidth',LW); % 113 = D17
end
% axis([timewin(1) timewin(end) -beta_scale beta_scale]);
ylabel('power change (in %)');
if(strcmp(Type(1:4),'post')), xlabel('time (in ms after mvt offset)');
else, xlabel('time (in ms after mvt onset)'); end
title('Beta');
% if(strcmp(Type(1:4),'post')), legend('R1 first bin','R1 last bin','RR first bin','RR last bin','Location','southeast');
% else, legend('R1 first bin','R1 last bin','RR first bin','RR last bin'); end
% legend('EarlyAdapt','LateAdapt');
hold off;

figure();
% subplot(3,1,3)
% [10,11,12,14,15,16,22,23,24,27,28,29,39,40,41]
ElecVector = [1,97,98,108,109,110,111,114,115,116,87]; % LM
hold on;
for cond =1:length(conditions) 
    Gamma_File = strcat(generalPowerFolderName, Manip, FolderType_first, conditions{cond}, '1/_', FolderType_first(2:end-1),'_',conditions{cond},'1_AverageGammaPower.mat');
    D = spm_eeg_load(Gamma_File);
    plot(timevec,permute(mean(D(ElecVector,yvec,1),1),[2,3,1]),'Color',LineColors(1,:),'LineWidth',LW);

%     Gamma_File = strcat(generalPowerFolderName, Manip, FolderType_last, conditions{cond}, '2/_', FolderType_last(2:end-1),'_',conditions{cond},'2_AverageGammaPower.mat');
    Gamma_File = strcat(generalPowerFolderName, Manip, FolderType_last, conditions{cond}, '1/_', FolderType_last(2:end-1),'_',conditions{cond},'1_AverageGammaPower.mat');
    D = spm_eeg_load(Gamma_File);
    plot(timevec,permute(mean(D(ElecVector,yvec,1),1),[2,3,1]),'Color',LineColors(2,:),'LineWidth',LW);
    
end
% axis([timewin(1) timewin(end) -alpha_scale alpha_scale]);
ylabel('power change (in %)');
if(strcmp(Type(1:4),'post')), xlabel('time (in ms after mvt offset)');
else, xlabel('time (in ms after mvt onset)'); end
title('Gamma');
% if(strcmp(Type(1:4),'post')), legend('R1 first bin','R1 last bin','RR first bin','RR last bin','Location','southeast');
% else, legend('R1 first bin','R1 last bin','RR first bin','RR last bin'); end
hold off;


figure();
% subplot(3,1,1)
ElecVector = [72,73,74,78,79,80,81,82,83,91,92,93,94,95,96]; % FP
hold on;
for cond =1:length(conditions) 
    Theta_File = strcat(generalPowerFolderName, Manip, FolderType_first, conditions{cond}, '1/_', FolderType_first(2:end-1),'_',conditions{cond},'1_AverageThetaPower.mat');
    D = spm_eeg_load(Theta_File);
    plot(timevec,permute(mean(D(ElecVector,yvec,1),1),[2,3,1]),'Color',LineColors(1,:),'LineWidth',LW);

%     Theta_File = strcat(generalPowerFolderName, Manip, FolderType_last, conditions{cond}, '2/_', FolderType_last(2:end-1),'_',conditions{cond},'2_AverageThetaPower.mat');
    Theta_File = strcat(generalPowerFolderName, Manip, FolderType_last, conditions{cond}, '1/_', FolderType_last(2:end-1),'_',conditions{cond},'1_AverageThetaPower.mat');
    D = spm_eeg_load(Theta_File);
    plot(timevec,permute(mean(D(ElecVector,yvec,1),1),[2,3,1]),'Color',LineColors(2,:),'LineWidth',LW);
    
end
% axis([timewin(1) timewin(end) -theta_scale theta_scale]);
ylabel('power change (in %)');
if(strcmp(Type(1:4),'post')), xlabel('time (in ms after mvt offset)');
else, xlabel('time (in ms after mvt onset)'); end
title('Theta');
% if(strcmp(Type(1:4),'post')), legend('R1 first bin','R1 last bin','RR first bin','RR last bin','Location','southeast');
% else, legend('R1 first bin','R1 last bin','RR first bin','RR last bin'); end
hold off;


% subplot(4,1,3)
% % ElecVector = [1,97,98,108,109,110,111,114,115,116,87]; % LM
% hold on;
% for cond =1:length(conditions) 
%     Gamma_File = strcat(generalPowerFolderName, Manip, FolderType_first, conditions{cond}, '/_', FolderType_first(2:end-1),'_',conditions{cond},'_AverageGammaPower.mat');
%     D = spm_eeg_load(Gamma_File);
%     plot(timevec,permute(mean(D(ElecVector,yvec,1),1),[2,3,1]),'Color',LineColors(1,:),'LineWidth',LW);
% 
%     Gamma_File = strcat(generalPowerFolderName, Manip, FolderType_last, conditions{cond}, '/_', FolderType_last(2:end-1),'_',conditions{cond},'_AverageGammaPower.mat');
%     D = spm_eeg_load(Gamma_File);
%     plot(timevec,permute(mean(D(ElecVector,yvec,1),1),[2,3,1]),'Color',LineColors(2,:),'LineWidth',LW);
%     
% end
% axis([timewin(1) timewin(end) -gamma_scale gamma_scale]);
% ylabel('power change (in %)');
% if(strcmp(Type(1:4),'post')), xlabel('time (in ms after mvt offset)');
% else, xlabel('time (in ms after mvt onset)'); end
% title('Gamma');
% if(strcmp(Type(1:4),'post')), legend('R1 first bin','R1 last bin','RR first bin','RR last bin','Location','southeast');
% else, legend('R1 first bin','R1 last bin','RR first bin','RR last bin'); end
% hold off;


%% PLOT per error bins
close all;
Manip = 'adultes'; % ou 'enfants'
generalPowerFolderName = './../Power data/';
Type = 'postmovement'; % ou postmovement
PowerUnit = 'percent';
FolderType = strcat('/',Type,'_',PowerUnit,'/');
FolderType_large = strcat('/',Type,'_large_',PowerUnit,'/');
FolderType_small = strcat('/',Type,'_small_',PowerUnit,'/');
SaveFigs = true;
FigsFolderName = './../Présentations/Figs/EEG/';


if strcmp(Manip,'adultes')
	conditions = {'R1';'RR'};
    if strcmp(Type(1:4),'post')
        timewin = [0 2500]; % Global : -1500:5:5500
        timevec = timewin(1):5:timewin(2);
        yvec = (timevec+1500)/5 + 1;
    else
        timewin = [-4000 1500]; % Global : -5500:5:1500
        timevec = timewin(1):5:timewin(2);
        yvec = (timevec+5500)/5 + 1;
    end
    tf_scale = 80;
    beta_scale = 50;
    alpha_scale = 90;
    theta_scale = 50;
    gamma_scale = 30;
else
    conditions = {'R1';'RR'};
    if strcmp(Type(1:4),'post')
        timewin = [-2500 3500]; % Global : -2500:5:5500
        timevec = timewin(1):5:timewin(2);
        yvec = (timevec+2500)/5 + 1;
    else
        timewin = [-4000 2500]; % Global : -5500:5:2500
        timevec = timewin(1):5:timewin(2);
        yvec = (timevec+5500)/5 + 1;
    end
    tf_scale = 30;
    beta_scale = 25;
    alpha_scale = 40;
    theta_scale = 40;
    gamma_scale = 20;
end

ElecVector = [1:6,19,110:115,124];
for cond =1:length(conditions)
    TF_File = strcat(generalPowerFolderName, Manip, FolderType_large, conditions{cond}, '/_', FolderType_large(2:end-1),'_',conditions{cond},'_AverageGlobalTF.mat');
    D = spm_eeg_load(TF_File);
    figure();
    imagesc(timevec,4:81,permute(mean(D(ElecVector,:,yvec,1),1),[2,3,1,4])); % 113 = D17
    axis normal xy;
    colormap jet;
    colorbar;
    ylabel('frequency (in Hz)');
    if(strcmp(Type(1:4),'post')), xlabel('time (in ms after mvt offset)');
    else, xlabel('time (in ms after mvt onset)'); end
    title(strcat(conditions{cond},': large errors'));
    caxis([-tf_scale tf_scale]);
    if(SaveFigs)
        savefig(strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_',conditions{cond},'_large_TF.fig'));
        saveas(gcf,strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_',conditions{cond},'_large_TF.png'));
    end
    
    TF_File = strcat(generalPowerFolderName, Manip, FolderType_small, conditions{cond}, '/_', FolderType_small(2:end-1),'_',conditions{cond},'_AverageGlobalTF.mat');
    D = spm_eeg_load(TF_File);
    figure();
    imagesc(timevec,4:81,permute(mean(D(ElecVector,:,yvec,1),1),[2,3,1,4])); % 113 = D17
    axis normal xy;
    colormap jet;
    colorbar;
    ylabel('frequency (in Hz)');
    if(strcmp(Type(1:4),'post')), xlabel('time (in ms after mvt offset)');
    else, xlabel('time (in ms after mvt onset)'); end
    title(strcat(conditions{cond},': small errors'));
    caxis([-tf_scale tf_scale]);
    if(SaveFigs)
        savefig(strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_',conditions{cond},'_small_TF.fig'));
        saveas(gcf,strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_',conditions{cond},'_small_TF.png'));
    end
end

figure();
hold on;
LineStyles = {'-.';'-'};
LineColors = [[0 0.4470 0.7410];[0.8500 0.3250 0.0980];[0.4660 0.6740 0.1880]];
for cond =1:length(conditions) 
    Beta_File = strcat(generalPowerFolderName, Manip, FolderType_large, conditions{cond}, '/_', FolderType_large(2:end-1),'_',conditions{cond},'_AverageBetaPower.mat');
    D = spm_eeg_load(Beta_File);
    plot(timevec,permute(mean(D(ElecVector,yvec,1),1),[2,3,1]),'Color',LineColors(cond,:),'LineStyle',LineStyles{1}); % 113 = D17

    Beta_File = strcat(generalPowerFolderName, Manip, FolderType_small, conditions{cond}, '/_', FolderType_small(2:end-1),'_',conditions{cond},'_AverageBetaPower.mat');
    D = spm_eeg_load(Beta_File);
    plot(timevec,permute(mean(D(ElecVector,yvec,1),1),[2,3,1]),'Color',LineColors(cond,:),'LineStyle',LineStyles{2}); % 113 = D17
    
end
% axis([timewin(1) timewin(end) -beta_scale beta_scale]);
ylabel('power change (in %)');
if(strcmp(Type(1:4),'post')), xlabel('time (in ms after mvt offset)');
else, xlabel('time (in ms after mvt onset)'); end
title('Beta');
if(strcmp(Type(1:4),'post')), legend('R1 large error bin','R1 small error bin','RR large error bin','RR small error bin','Location','southeast');
else, legend('R1 large error bin','R1 small error bin','RR large error bin','RR small error bin'); end
hold off;

if(SaveFigs)
    savefig(strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_error_bins_Beta.fig'));
    saveas(gcf,strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_error_bins_Beta.png'));
end

figure();
hold on;
for cond =1:length(conditions) 
    Alpha_File = strcat(generalPowerFolderName, Manip, FolderType_large, conditions{cond}, '/_', FolderType_large(2:end-1),'_',conditions{cond},'_AverageAlphaPower.mat');
    D = spm_eeg_load(Alpha_File);
    plot(timevec,permute(mean(D(ElecVector,yvec,1),1),[2,3,1]),'Color',LineColors(cond,:),'LineStyle',LineStyles{1});

    Alpha_File = strcat(generalPowerFolderName, Manip, FolderType_small, conditions{cond}, '/_', FolderType_small(2:end-1),'_',conditions{cond},'_AverageAlphaPower.mat');
    D = spm_eeg_load(Alpha_File);
    plot(timevec,permute(mean(D(ElecVector,yvec,1),1),[2,3,1]),'Color',LineColors(cond,:),'LineStyle',LineStyles{2});
    
end
% axis([timewin(1) timewin(end) -alpha_scale alpha_scale]);
ylabel('power change (in %)');
if(strcmp(Type(1:4),'post')), xlabel('time (in ms after mvt offset)');
else, xlabel('time (in ms after mvt onset)'); end
title('Alpha');
if(strcmp(Type(1:4),'post')), legend('R1 large error bin','R1 small error bin','RR large error bin','RR small error bin','Location','southeast');
else, legend('R1 large error bin','R1 small error bin','RR large error bin','RR small error bin'); end
hold off;

if(SaveFigs)
    savefig(strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_error_bins_Alpha.fig'));
    saveas(gcf,strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_error_bins_Alpha.png'));
end

figure();
hold on;
for cond =1:length(conditions) 
    Theta_File = strcat(generalPowerFolderName, Manip, FolderType_large, conditions{cond}, '/_', FolderType_large(2:end-1),'_',conditions{cond},'_AverageThetaPower.mat');
    D = spm_eeg_load(Theta_File);
    plot(timevec,permute(mean(D(ElecVector,yvec,1),1),[2,3,1]),'Color',LineColors(cond,:),'LineStyle',LineStyles{1});

    Theta_File = strcat(generalPowerFolderName, Manip, FolderType_small, conditions{cond}, '/_', FolderType_small(2:end-1),'_',conditions{cond},'_AverageThetaPower.mat');
    D = spm_eeg_load(Theta_File);
    plot(timevec,permute(mean(D(ElecVector,yvec,1),1),[2,3,1]),'Color',LineColors(cond,:),'LineStyle',LineStyles{2});
    
end
% axis([timewin(1) timewin(end) -theta_scale theta_scale]);
ylabel('power change (in %)');
if(strcmp(Type(1:4),'post')), xlabel('time (in ms after mvt offset)');
else, xlabel('time (in ms after mvt onset)'); end
title('Theta');
if(strcmp(Type(1:4),'post')), legend('R1 large error bin','R1 small error bin','RR large error bin','RR small error bin','Location','southeast');
else, legend('R1 large error bin','R1 small error bin','RR large error bin','RR small error bin'); end
hold off;

if(SaveFigs)
    savefig(strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_error_bins_Theta.fig'));
    saveas(gcf,strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_error_bins_Theta.png'));
end

figure();
hold on;
for cond =1:length(conditions) 
    Gamma_File = strcat(generalPowerFolderName, Manip, FolderType_large, conditions{cond}, '/_', FolderType_large(2:end-1),'_',conditions{cond},'_AverageGammaPower.mat');
    D = spm_eeg_load(Gamma_File);
    plot(timevec,permute(mean(D(ElecVector,yvec,1),1),[2,3,1]),'Color',LineColors(cond,:),'LineStyle',LineStyles{1});

    Gamma_File = strcat(generalPowerFolderName, Manip, FolderType_small, conditions{cond}, '/_', FolderType_small(2:end-1),'_',conditions{cond},'_AverageGammaPower.mat');
    D = spm_eeg_load(Gamma_File);
    plot(timevec,permute(mean(D(ElecVector,yvec,1),1),[2,3,1]),'Color',LineColors(cond,:),'LineStyle',LineStyles{2});
    
end
% axis([timewin(1) timewin(end) -gamma_scale gamma_scale]);
ylabel('power change (in %)');
if(strcmp(Type(1:4),'post')), xlabel('time (in ms after mvt offset)');
else, xlabel('time (in ms after mvt onset)'); end
title('Gamma');
if(strcmp(Type(1:4),'post')), legend('R1 large error bin','R1 small error bin','RR large error bin','RR small error bin','Location','southeast');
else, legend('R1 large error bin','R1 small error bin','RR large error bin','RR small error bin'); end
hold off;

if(SaveFigs)
    savefig(strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_error_bins_Gamma.fig'));
    saveas(gcf,strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_error_bins_Gamma.png'));
end

%% PLOT per execution order
close all;
Manip = 'adultes'; % ou 'enfants'
generalPowerFolderName = './../Power data/';
Type = 'postmovement'; % ou postmovement
PowerUnit = 'percent';
FolderType = strcat('/',Type,'_',PowerUnit,'/');
FolderType_first = strcat('/',Type,'_first_',PowerUnit,'/');
FolderType_last = strcat('/',Type,'_last_',PowerUnit,'/');
SaveFigs = false;
FigsFolderName = './../Présentations/Figs/EEG/';


if strcmp(Manip,'adultes')
	conditions = {'R1';'RR'};
    if strcmp(Type(1:4),'post')
        timewin = [-1500 5500]; % Global : -1500:5:5500
        timevec = timewin(1):5:timewin(2);
        yvec = (timevec+1500)/5 + 1;
    else
        timewin = [-4000 1500]; % Global : -5500:5:1500
        timevec = timewin(1):5:timewin(2);
        yvec = (timevec+5500)/5 + 1;
    end
%     tf_scale = 80;
%     beta_scale = 55;
%     alpha_scale = 100;
%     theta_scale = 50;
%     gamma_scale = 30;
else
    conditions = {'R1';'RR'};
    if strcmp(Type(1:4),'post')
        timewin = [-2500 3500]; % Global : -2500:5:5500
        timevec = timewin(1):5:timewin(2);
        yvec = (timevec+2500)/5 + 1;
    else
        timewin = [-4000 2500]; % Global : -5500:5:2500
        timevec = timewin(1):5:timewin(2);
        yvec = (timevec+5500)/5 + 1;
    end
%     tf_scale = 30;
%     beta_scale = 25;
%     alpha_scale = 40;
%     theta_scale = 40;
%     gamma_scale = 20;
end

ElecVector = [1:6,19,110:115,124];
for cond =1:length(conditions)
    TF_File = strcat(generalPowerFolderName, Manip, FolderType_first, conditions{cond}, '/_', FolderType_first(2:end-1),'_',conditions{cond},'_AverageGlobalTF.mat');
    D = spm_eeg_load(TF_File);
    figure();
    imagesc(timevec,4:81,permute(mean(D(ElecVector,:,yvec,1),1),[2,3,1,4])); % 113 = D17
    axis normal xy;
    colormap jet;
    colorbar;
    ylabel('frequency (in Hz)');
    if(strcmp(Type(1:4),'post')), xlabel('time (in ms after mvt offset)');
    else, xlabel('time (in ms after mvt onset)'); end
    title(strcat(conditions{cond},': first bin'));
%     caxis([-tf_scale tf_scale]);
    if(SaveFigs)
        savefig(strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_',conditions{cond},'_first_TF.fig'));
        saveas(gcf,strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_',conditions{cond},'_first_TF.png'));
    end
    
    TF_File = strcat(generalPowerFolderName, Manip, FolderType_last, conditions{cond}, '/_', FolderType_last(2:end-1),'_',conditions{cond},'_AverageGlobalTF.mat');
    D = spm_eeg_load(TF_File);
    figure();
    imagesc(timevec,4:81,permute(mean(D(ElecVector,:,yvec,1),1),[2,3,1,4])); % 113 = D17
    axis normal xy;
    colormap jet;
    colorbar;
    ylabel('frequency (in Hz)');
    if(strcmp(Type(1:4),'post')), xlabel('time (in ms after mvt offset)');
    else, xlabel('time (in ms after mvt onset)'); end
    title(strcat(conditions{cond},': last bin'));
%     caxis([-tf_scale tf_scale]);
    if(SaveFigs)
        savefig(strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_',conditions{cond},'_last_TF.fig'));
        saveas(gcf,strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_',conditions{cond},'_last_TF.png'));
    end
end

for cond =1:length(conditions)
    figure();
    hold on;
    Beta_File = strcat(generalPowerFolderName, Manip, FolderType_first, conditions{cond}, '/_', FolderType_first(2:end-1),'_',conditions{cond},'_AverageBetaPower.mat');
    D = spm_eeg_load(Beta_File);
    plot(timevec,permute(mean(D(ElecVector,yvec,1),1),[2,3,1])); % 113 = D17

    Beta_File = strcat(generalPowerFolderName, Manip, FolderType_last, conditions{cond}, '/_', FolderType_last(2:end-1),'_',conditions{cond},'_AverageBetaPower.mat');
    D = spm_eeg_load(Beta_File);
    plot(timevec,permute(mean(D(ElecVector,yvec,1),1),[2,3,1])); % 113 = D17
%     axis([timewin(1) timewin(end) -beta_scale beta_scale]);
    ylabel('power change (in %)');
    if(strcmp(Type(1:4),'post')), xlabel('time (in ms after mvt offset)');
    else, xlabel('time (in ms after mvt onset)'); end
    title(strcat('Beta : ',conditions{cond}));
    
    if(strcmp(Type(1:4),'post')), legend('first bin','last bin','Location','southeast');
    else, legend('first bin','last bin'); end
    hold off;
    
    if(SaveFigs)
        savefig(strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_',conditions{cond},'_exec_bins_Beta.fig'));
        saveas(gcf,strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_',conditions{cond},'_exec_bins_Beta.png'));
    end
end

for cond =1:length(conditions)
    figure();
    hold on;
    Alpha_File = strcat(generalPowerFolderName, Manip, FolderType_first, conditions{cond}, '/_', FolderType_first(2:end-1),'_',conditions{cond},'_AverageAlphaPower.mat');
    D = spm_eeg_load(Alpha_File);
    plot(timevec,permute(mean(D(ElecVector,yvec,1),1),[2,3,1])); % 113 = D17

    Alpha_File = strcat(generalPowerFolderName, Manip, FolderType_last, conditions{cond}, '/_', FolderType_last(2:end-1),'_',conditions{cond},'_AverageAlphaPower.mat');
    D = spm_eeg_load(Alpha_File);
    plot(timevec,permute(mean(D(ElecVector,yvec,1),1),[2,3,1])); % 113 = D17
%     axis([timewin(1) timewin(end) -alpha_scale alpha_scale]);
    ylabel('power change (in %)');
    if(strcmp(Type(1:4),'post')), xlabel('time (in ms after mvt offset)');
    else, xlabel('time (in ms after mvt onset)'); end
    title(strcat('Alpha : ',conditions{cond}));
    
    if(strcmp(Type(1:4),'post')), legend('first bin','last bin','Location','southeast');
    else, legend('first bin','last bin'); end
    hold off;
    
    if(SaveFigs)
        savefig(strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_',conditions{cond},'_exec_bins_Alpha.fig'));
        saveas(gcf,strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_',conditions{cond},'_exec_bins_Alpha.png'));
    end
end

for cond =1:length(conditions)
    figure();
    hold on;
    Theta_File = strcat(generalPowerFolderName, Manip, FolderType_first, conditions{cond}, '/_', FolderType_first(2:end-1),'_',conditions{cond},'_AverageThetaPower.mat');
    D = spm_eeg_load(Theta_File);
    plot(timevec,permute(mean(D(ElecVector,yvec,1),1),[2,3,1])); % 113 = D17

    Theta_File = strcat(generalPowerFolderName, Manip, FolderType_last, conditions{cond}, '/_', FolderType_last(2:end-1),'_',conditions{cond},'_AverageThetaPower.mat');
    D = spm_eeg_load(Theta_File);
    plot(timevec,permute(mean(D(ElecVector,yvec,1),1),[2,3,1])); % 113 = D17
%     axis([timewin(1) timewin(end) -theta_scale theta_scale]);
    ylabel('power change (in %)');
    if(strcmp(Type(1:4),'post')), xlabel('time (in ms after mvt offset)');
    else, xlabel('time (in ms after mvt onset)'); end
    title(strcat('Theta : ',conditions{cond}));
    
    if(strcmp(Type(1:4),'post')), legend('first bin','last bin','Location','southeast');
    else, legend('first bin','last bin'); end
    hold off;
    
    if(SaveFigs)
        savefig(strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_',conditions{cond},'_exec_bins_Theta.fig'));
        saveas(gcf,strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_',conditions{cond},'_exec_bins_Theta.png'));
    end
end

for cond =1:length(conditions)
    figure();
    hold on;
    Gamma_File = strcat(generalPowerFolderName, Manip, FolderType_first, conditions{cond}, '/_', FolderType_first(2:end-1),'_',conditions{cond},'_AverageGammaPower.mat');
    D = spm_eeg_load(Gamma_File);
    plot(timevec,permute(mean(D(ElecVector,yvec,1),1),[2,3,1])); % 113 = D17

    Gamma_File = strcat(generalPowerFolderName, Manip, FolderType_last, conditions{cond}, '/_', FolderType_last(2:end-1),'_',conditions{cond},'_AverageGammaPower.mat');
    D = spm_eeg_load(Gamma_File);
    plot(timevec,permute(mean(D(ElecVector,yvec,1),1),[2,3,1])); % 113 = D17
%     axis([timewin(1) timewin(end) -gamma_scale gamma_scale]);
    ylabel('power change (in %)');
    if(strcmp(Type(1:4),'post')), xlabel('time (in ms after mvt offset)');
    else, xlabel('time (in ms after mvt onset)'); end
    title(strcat('Gamma : ',conditions{cond}));
    
    if(strcmp(Type(1:4),'post')), legend('first bin','last bin','Location','southeast');
    else, legend('first bin','last bin'); end
    hold off;
    
    if(SaveFigs)
        savefig(strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_',conditions{cond},'_exec_bins_Gamma.fig'));
        saveas(gcf,strcat(FigsFolderName,Manip,FolderType,'/',Manip,'_',Type,'_',conditions{cond},'_exec_bins_Gamma.png'));
    end
end

