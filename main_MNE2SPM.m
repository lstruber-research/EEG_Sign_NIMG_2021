%% MNE to SPM
% Convert epoched FIF EEG files saved with MNE to SPM-MEEG files.
% Header for BioSemi 128
% Lucas Struber - 05/03/2020
load('Biosemi128_SPM_header.mat');
Manip = 'adultes'; % ou 'enfants'
generalFolderName = './../Epoched data/';
Type = '/postmovement/'; % ou postmovement

if strcmp(Manip,'adultes')
	subjects = {'01AC1';'02MG1';'03CC1';'04NB1';'05NF1';'06LL1';'07TJ1';'08CD1';'09MA1';'10NM1';'11LP1';'12KN1';'13LD1';'14NG1';'15OB1';'16SA1';'17GC1';'18AC1';'19BM1'};
	conditions = {'F';'R1';'RR';'RJ';'R2'};
    if strcmp(Type(1:13),'/postmovement')
        timeonset = -1.7;
    else
        timeonset = -5.7;
    end
else
	subjects = {'01FE1';'02BN1';'03AN1';'04MC1';'05JK1';'06GC1';'07EG1';'08HM1';'09ED1';'10DT1';'11VH1';'13MC1';'14TG1';'15LG1';'16SB1';'17JL1';'18LF1';'19EV1';'20LB1';'21EF1';'22LK1';'23LM1'};
	conditions = {'F';'R1';'RR'};
    if strcmp(Type(1:13),'/postmovement')
        timeonset = -2.7;
    else
        timeonset = -5.7;
    end
end
   
% Boucler sur tous les fichiers + toutes les conditions (postmouvement)
for i = 1:length(subjects)
    for j = 1:length(conditions)
        tic
        convert_opt =[];
        FolderName = strcat(generalFolderName, Manip, Type, conditions{j}, '/');
        
        convert_opt.dataset = strcat(FolderName, subjects{i}, '-epo.fif');
        convert_opt.mode = 'epoched';
        convert_opt.channels = {'all'};
        convert_opt.eventpadding = 0;
        convert_opt.blocksize = 3276800;
        convert_opt.checkboundary = 1;
        convert_opt.saveorigheader = 0;
        
        D = spm_eeg_convert(convert_opt);
        
        % Déplacer les deux fichiers (.dat et .mat) dans le bon dossier
        movefile(strcat('spmeeg_', subjects{i}, '-epo.mat'), FolderName)
        movefile(strcat('spmeeg_', subjects{i}, '-epo.dat'), FolderName)

        load(strcat(FolderName, 'spmeeg_', subjects{i}, '-epo.mat'));
        D.channels = Header.channels;
        D.sensors = Header.sensors;
        D.fiducials = Header.fiducials;
        for trial = 1:length(D.trials)
            D.trials(trial).label = conditions{j};
        end
        D.timeOnset = timeonset; % - 1.2s for post, -3.2s for pre
        save(strcat(FolderName, 'spmeeg_', subjects{i}, '-epo.mat'), 'D'); % (a généraliser)
        toc
    end
end