"""
EEG Power calculation (MNE) 
Created on Thu Feb 20 10:33:55 2020
@author: struberl
power"""

import Preprocessing as pp
import numpy as np
import os.path
import os
import mne

Manip = "adultes" #ou "adultes"
generalFolderName = "./../"
channels = None #['D19','D20','D21','D22','D1']
SavePower = False

all_bands = np.arange(4,81,1)


if Manip == "adultes":
	groupRawFolderName = "Preprocessed data/processed-raw/processed_raw_"
	subjects = ['01AC1','02MG1','03CC1','04NB1','05NF1','06LL1','07TJ1','08CD1','09MA1','10NM1','11LP1','12KN1','13LD1','14NG1','15OB1','16SA1','17GC1','18AC1','19BM1']
	conditions = ['F','R1','R2','RJ','RR'] # ['F','R1','R2','RJ','RR','wR1','wR2']  
	badEpochsFile = "Manip_EEG_2_bad_epochs.xlsx"
	t_pre = [-5.5, 1.5]
	t_post = [-1.5, 5.5]
    
else:
	groupRawFolderName = "Preprocessed data/processed-raw-enfants/"
	subjects = ['01FE1','02BN1','03AN1','04MC1','05JK1','06GC1','07EG1','08HM1','09ED1','10DT1','11VH1','13MC1','14TG1','15LG1','16SB1','17JL1','18LF1','19EV1','20LB1','21EF1','22LK1','23LM1']
	conditions = ['F','R1','RR'] # ['RR']
	badEpochsFile = "Manip_EEG_Enfants_bad_epochs.xlsx"
	t_pre = [-5.5, 2.5]
	t_post = [-2.5, 5.5]


list_bad_epochs_pre = []
list_bad_epochs_post = []
was_first_epoch_deleted = []	
nbTrials_pre_before = []	
nbTrials_pre_after = []	
nbTrials_post_before = []	
nbTrials_post_after = []	
for sub in subjects:
    list_bad_epochs_pre.append([])
    list_bad_epochs_post.append([])
    was_first_epoch_deleted.append([])
    nbTrials_pre_before.append([])
    nbTrials_pre_after.append([])
    nbTrials_post_before.append([])
    nbTrials_post_after.append([])
    for cond in conditions:
        list_bad_epochs_pre[-1].append([])
        list_bad_epochs_post[-1].append([])
        was_first_epoch_deleted[-1].append([])
        nbTrials_pre_before[-1].append([])
        nbTrials_pre_after[-1].append([])
        nbTrials_post_before[-1].append([])
        nbTrials_post_after[-1].append([])
        rawFileName = generalFolderName + groupRawFolderName + sub + "-" + cond + ".fif"
        epochFolderName = generalFolderName + "Epoched data/" + Manip
        if os.path.isfile(rawFileName):
            
            print("subject : " + sub + " -- condition :" + cond)
            raw = mne.io.read_raw_fif(rawFileName, preload=True)
            folderNamePre = epochFolderName  + "/premovement/" + cond 
            folderNamePost = epochFolderName  + "/postmovement/" + cond 
#            # pre movement (epoch 3 fig + power 1 fig)
            bad_epochs_eeg_pre, bad_epochs_kin_pre = pp.read_bad_epochs(sub, cond, badEpochsFile, 257)
            epochs_pre, toDrop_pre = pp.epoch_creation_new(raw, 257, t_pre[0] - 0.2, t_pre[1] + 0.2, bad_epochs_eeg=bad_epochs_eeg_pre, bad_epochs_kin=bad_epochs_kin_pre, plot = False)
            if not os.path.isdir(folderNamePre):
                os.makedirs(folderNamePre)
            epochs_pre.save(folderNamePre + "/" + sub + "-epo.fif", overwrite=True)
            list_bad_epochs_pre[-1][-1] = toDrop_pre
            if epochs_pre.drop_log[2] == ['NO_DATA']: 
                was_first_epoch_deleted[-1][-1] = 1
            else:
                was_first_epoch_deleted[-1][-1] = 0
            nbTrials_pre_before[-1][-1] = len(epochs_pre) + len(toDrop_pre)
            nbTrials_pre_after[-1][-1] = len(epochs_pre)
                
            # post movement (epoch 3 fig + power 1 fig)
            bad_epochs_eeg_post, bad_epochs_kin_post = pp.read_bad_epochs(sub, cond, badEpochsFile, 254)
            epochs_post, toDrop_post = pp.epoch_creation_new(raw, 254, t_post[0] - 0.2, t_post[1]  + 0.2, bad_epochs_eeg=bad_epochs_eeg_post, bad_epochs_kin=bad_epochs_kin_post, plot = False)
            list_bad_epochs_post[-1][-1] = toDrop_post
            nbTrials_post_before[-1][-1] = len(epochs_post) + len(toDrop_post)
            nbTrials_post_after[-1][-1] = len(epochs_post)
            
            if not os.path.isdir(folderNamePost):
                os.makedirs(folderNamePost)
            epochs_post.save(folderNamePost + "/" + sub + "-epo.fif", overwrite=True)
                
        else:
            print("The preprocessed file " + rawFileName + " does not exist")