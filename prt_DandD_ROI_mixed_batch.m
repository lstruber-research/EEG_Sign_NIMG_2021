% List of open inputs
% Data & Design: Directory - cfg_files
% Data & Design: Masks - cfg_repeat
global Manip Type Files generalImagesFolderName BandsSimple BandsPAC Channels subjects conditions_per_bins conditions_undivided BinsType PowerUnit

Manip = 'adultes'; % ou 'enfants'
Type = 'postmovement'; % ou postmovement
BinsType = 'order'; % or 'errors'
PowerUnit = 'percent'; % or 'dB'
Files = {'power';}; % {'phase';'power'};
conditions_per_bins = {'F';'RR';'R1';'R2'};
conditions_undivided = {'F';'RR';'R1';'R2'};
% conditions_per_bins = {'F';'RR';'R1'};
% conditions_undivided = {'F';'RR';'R1'};

generalImagesFolderName = './../Pronto ROI images/';
prontoFolderName = './../Pronto ROI data/';
BandsSimple = {'theta';'alpha';'beta';'gamma'};
% BandsPAC = {'theta_beta';'theta_gamma';'alpha_beta';'alpha_gamma'};
BandsPAC = {'theta_beta';'theta_gamma'};

Channels = {'FP';'SM';'RPM';'LPM';'RM';'LM';'RSS';'LSS';'RP';'LP';'OP'};

if strcmp(Manip,'adultes')
    subjects = {'01AC1';'02MG1';'03CC1';'04NB1';'05NF1';'06LL1';'07TJ1';'08CD1';'09MA1';'10NM1';'11LP1';'12KN1';'13LD1';'14NG1';'15OB1';'16SA1';'17GC1';'18AC1';'19BM1'};
else
    subjects = {'01FE1';'02BN1';'03AN1';'04MC1';'05JK1';'07EG1';'08HM1';'09ED1';'10DT1';'11VH1';'13MC1';'14TG1';'16SB1';'17JL1';'18LF1';'19EV1';'20LB1';'21EF1';'22LK1';'23LM1'};
end

jobfile = {'prt_DandD_ROI_mixed_batch_job.m'};
jobs = repmat(jobfile, 1, 1);
inputs = cell(1, 1);
if length(Files) == 1, inputs{1, 1} = cellstr(strcat(prontoFolderName, Manip, '/', PowerUnit, '/', Type, '_', BinsType, '_', Files{1},'_lowgamma')); % Data & Design: Directory - cfg_files
else, inputs{1, 1} = cellstr(strcat(prontoFolderName, Manip, '/', PowerUnit, '/', Type, '_', BinsType, '_all')); % Data & Design: Directory - cfg_files
end

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);
