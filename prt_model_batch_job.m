%-----------------------------------------------------------------------
% Job saved on 03-Jun-2020 21:36:09 by cfg_util (rev $Rev: 7345 $)
% spm SPM - SPM12 (7771)
% cfg_basicio BasicIO - Unknown
% prt PRoNTo - Unknown
%-----------------------------------------------------------------------
matlabbatch{1}.prt.model.infile = '<UNDEFINED>';
matlabbatch{1}.prt.model.model_name = '<UNDEFINED>';
matlabbatch{1}.prt.model.use_kernel = 1;
matlabbatch{1}.prt.model.fsets = '<UNDEFINED>';
matlabbatch{1}.prt.model.model_type.classification.class(1).class_name = '<UNDEFINED>';
matlabbatch{1}.prt.model.model_type.classification.class(1).group.gr_name = '<UNDEFINED>';
matlabbatch{1}.prt.model.model_type.classification.class(1).group.subj_nums = '<UNDEFINED>';
matlabbatch{1}.prt.model.model_type.classification.class(1).group.conditions.all_scans = 1;
matlabbatch{1}.prt.model.model_type.classification.class(2).class_name = '<UNDEFINED>';
matlabbatch{1}.prt.model.model_type.classification.class(2).group.gr_name = '<UNDEFINED>';
matlabbatch{1}.prt.model.model_type.classification.class(2).group.subj_nums = '<UNDEFINED>';
matlabbatch{1}.prt.model.model_type.classification.class(2).group.conditions.all_scans = 1;
matlabbatch{1}.prt.model.model_type.classification.machine_cl.sMKL_cla.sMKL_cla_opt = 0;
matlabbatch{1}.prt.model.model_type.classification.machine_cl.sMKL_cla.sMKL_cla_args = 1;
matlabbatch{1}.prt.model.model_type.classification.machine_cl.sMKL_cla.cv_type_nested.cv_losgo = 1;
matlabbatch{1}.prt.model.cv_type.cv_losgo = 1;
matlabbatch{1}.prt.model.include_allscans = 0;
matlabbatch{1}.prt.model.sel_ops.data_op_mc = 1;
matlabbatch{1}.prt.model.sel_ops.use_other_ops.data_op = {4};
matlabbatch{2}.prt.cv_model.infile = '<UNDEFINED>';
matlabbatch{2}.prt.cv_model.model_name = '<UNDEFINED>';
matlabbatch{2}.prt.cv_model.perm_test.no_perm = 1;
