PRT_File = {'.\..\Pronto ROI data\adultes\percent\postmovement_order_power_lowgamma\PRT.mat'};
Feature_set = 'adultes_postmovement_order_kernels';
NbSubjects = 19;

if(contains(PRT_File,'cov')), jobfile = {'prt_model_cov_batch_job.m'};
else, jobfile = {'prt_model_batch_job.m'};
end

jobs = repmat(jobfile, 1, 1);
inputs = cell(11, 1);
inputs{1, 1} = PRT_File; % Specify model: Load PRT.mat - cfg_files
inputs{3, 1} = Feature_set; % Specify model: Feature sets - cfg_entry
inputs{6, 1} = [1:NbSubjects]; % Specify model: Subjects - cfg_entry
inputs{9, 1} = [1:NbSubjects]; % Specify model: Subjects - cfg_entry
inputs{10, 1} = PRT_File; % Specify model: Load PRT.mat - cfg_files

% MKL F VS R1
inputs{2, 1} = 'mkl_F_R1'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'F'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'F'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'R1'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'R1'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% MKL F VS RR
inputs{2, 1} = 'mkl_F_RR'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'F'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'F'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'RR'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'RR'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% MKL RR VS R1
inputs{2, 1} = 'mkl_RR_R1'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'RR'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'RR'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'R1'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'R1'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% MKL F VS R2
inputs{2, 1} = 'mkl_F_R2'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'F'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'F'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'R2'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'R2'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% MKL R1 VS R2
inputs{2, 1} = 'mkl_R1_R2'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'R1'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'R1'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'R2'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'R2'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% MKL RR VS R2
inputs{2, 1} = 'mkl_RR_R2'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'RR'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'RR'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'R2'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'R2'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% MKL F VS RJ
inputs{2, 1} = 'mkl_F_RJ'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'F'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'F'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'RJ'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'RJ'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% MKL RJ VS R1
inputs{2, 1} = 'mkl_RJ_R1'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'RJ'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'RJ'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'R1'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'R1'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% MKL RR VS RJ
inputs{2, 1} = 'mkl_RR_RJ'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'RR'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'RR'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'RJ'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'RJ'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% MKL R2 VS RJ
inputs{2, 1} = 'mkl_R2_RJ'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'R2'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'R2'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'RJ'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'RJ'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% First Bin
% MKL F VS R1
inputs{2, 1} = 'mkl_F_R1_first'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'F_first'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'F_first'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'R1_first'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'R1_first'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% MKL F VS RR
inputs{2, 1} = 'mkl_F_RR_first'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'F_first'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'F_first'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'RR_first'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'RR_first'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% MKL RR VS R1
inputs{2, 1} = 'mkl_RR_R1_first'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'RR_first'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'RR_first'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'R1_first'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'R1_first'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% MKL F VS R2
inputs{2, 1} = 'mkl_F_R2_first'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'F_first'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'F_first'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'R2_first'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'R2_first'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% MKL R1 VS R2
inputs{2, 1} = 'mkl_R1_R2_first'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'R1_first'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'R1_first'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'R2_first'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'R2_first'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% MKL RR VS R2
inputs{2, 1} = 'mkl_RR_R2_first'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'RR_first'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'RR_first'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'R2_first'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'R2_first'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% MKL F VS RJ
inputs{2, 1} = 'mkl_F_RJ_first'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'F_first'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'F_first'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'RJ_first'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'RJ_first'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% MKL RJ VS R1
inputs{2, 1} = 'mkl_RJ_R1_first'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'RJ_first'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'RJ_first'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'R1_first'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'R1_first'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% MKL RR VS RJ
inputs{2, 1} = 'mkl_RR_RJ_first'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'RR_first'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'RR_first'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'RJ_first'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'RJ_first'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% MKL R2 VS RJ
inputs{2, 1} = 'mkl_R2_RJ_first'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'R2_first'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'R2_first'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'RJ_first'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'RJ_first'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% Last Bin
% MKL F VS R1
inputs{2, 1} = 'mkl_F_R1_last'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'F_last'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'F_last'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'R1_last'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'R1_last'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% MKL F VS RR
inputs{2, 1} = 'mkl_F_RR_last'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'F_last'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'F_last'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'RR_last'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'RR_last'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% MKL RR VS R1
inputs{2, 1} = 'mkl_RR_R1_last'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'RR_last'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'RR_last'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'R1_last'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'R1_last'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% MKL F VS R2
inputs{2, 1} = 'mkl_F_R2_last'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'F_last'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'F_last'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'R2_last'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'R2_last'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% MKL R1 VS R2
inputs{2, 1} = 'mkl_R1_R2_last'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'R1_last'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'R1_last'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'R2_last'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'R2_last'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% MKL RR VS R2
inputs{2, 1} = 'mkl_RR_R2_last'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'RR_last'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'RR_last'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'R2_last'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'R2_last'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% MKL F VS RJ
inputs{2, 1} = 'mkl_F_RJ_last'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'F_last'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'F_last'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'RJ_last'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'RJ_last'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% MKL RJ VS R1
inputs{2, 1} = 'mkl_RJ_R1_last'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'RJ_last'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'RJ_last'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'R1_last'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'R1_last'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% MKL RR VS RJ
inputs{2, 1} = 'mkl_RR_RJ_last'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'RR_last'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'RR_last'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'RJ_last'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'RJ_last'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% MKL R2 VS RJ
inputs{2, 1} = 'mkl_R2_RJ_last'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'R2_last'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'R2_last'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'RJ_last'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'RJ_last'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% Adaptation
% MKL R1 first VS R2 last
inputs{2, 1} = 'mkl_R1_first_R2_last'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'R1_first'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'R1_first'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'R2_last'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'R2_last'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% MKL R1 first VS R1 last
inputs{2, 1} = 'mkl_R1_first_R1_last'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'R1_first'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'R1_first'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'R1_last'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'R1_last'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% MKL R2 first VS R2 last
inputs{2, 1} = 'mkl_R2_first_R2_last'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'R2_first'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'R2_first'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'R2_last'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'R2_last'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% MKL F first VS F last
inputs{2, 1} = 'mkl_F_first_F_last'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'F_first'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'F_first'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'F_last'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'F_last'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);

% MKL RR first VS RR last
inputs{2, 1} = 'mkl_RR_first_RR_last'; % Specify model: Model name - cfg_entry
inputs{4, 1} = 'RR_first'; % Specify model: Name - cfg_entry
inputs{5, 1} = 'RR_first'; % Specify model: Group name - cfg_entry
inputs{7, 1} = 'RR_last'; % Specify model: Name - cfg_entry
inputs{8, 1} = 'RR_last'; % Specify model: Group name - cfg_entry
inputs{11, 1} = inputs{2, 1}; % Specify model: Feature sets - cfg_entry

job_id = cfg_util('initjob', jobs);
sts    = cfg_util('filljob', job_id, inputs{:});
if sts
    cfg_util('run', job_id);
end
cfg_util('deljob', job_id);