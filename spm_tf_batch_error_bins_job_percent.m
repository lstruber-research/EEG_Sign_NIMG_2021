%-----------------------------------------------------------------------
% Job saved on 17-Mar-2020 10:41:23 by cfg_util (rev $Rev: 7345 $)
% spm SPM - SPM12 (7771)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
matlabbatch{1}.spm.meeg.averaging.average.D = '<UNDEFINED>';
matlabbatch{1}.spm.meeg.averaging.average.userobust.standard = false;
matlabbatch{1}.spm.meeg.averaging.average.plv = false;
matlabbatch{1}.spm.meeg.averaging.average.prefix = 'av_';

matlabbatch{2}.spm.meeg.preproc.crop.D = '<UNDEFINED>';
matlabbatch{2}.spm.meeg.preproc.crop.timewin = '<UNDEFINED>';
matlabbatch{2}.spm.meeg.preproc.crop.freqwin = [4 80];
matlabbatch{2}.spm.meeg.preproc.crop.channels{1}.all = 'all';
matlabbatch{2}.spm.meeg.preproc.crop.prefix = 'c_';

matlabbatch{3}.spm.meeg.tf.avgfreq.D = '<UNDEFINED>';
matlabbatch{3}.spm.meeg.tf.avgfreq.freqwin = [4 8];
matlabbatch{3}.spm.meeg.tf.avgfreq.prefix = 'theta_';

matlabbatch{4}.spm.meeg.tf.avgfreq.D = '<UNDEFINED>';
matlabbatch{4}.spm.meeg.tf.avgfreq.freqwin = [9 12];
matlabbatch{4}.spm.meeg.tf.avgfreq.prefix = 'alpha_';

matlabbatch{5}.spm.meeg.tf.avgfreq.D = '<UNDEFINED>';
matlabbatch{5}.spm.meeg.tf.avgfreq.freqwin = [13 30];
matlabbatch{5}.spm.meeg.tf.avgfreq.prefix = 'beta_';

matlabbatch{6}.spm.meeg.tf.avgfreq.D = '<UNDEFINED>';
matlabbatch{6}.spm.meeg.tf.avgfreq.freqwin = [31 80];
matlabbatch{6}.spm.meeg.tf.avgfreq.prefix = 'gamma_';

matlabbatch{7}.spm.meeg.other.delete.D = '<UNDEFINED>';
matlabbatch{8}.spm.meeg.other.delete.D = '<UNDEFINED>';
matlabbatch{9}.spm.meeg.other.delete.D = '<UNDEFINED>';

matlabbatch{10}.spm.meeg.averaging.average.D = '<UNDEFINED>';
matlabbatch{10}.spm.meeg.averaging.average.userobust.standard = false;
matlabbatch{10}.spm.meeg.averaging.average.plv = false;
matlabbatch{10}.spm.meeg.averaging.average.prefix = 'av_';

matlabbatch{11}.spm.meeg.preproc.crop.D = '<UNDEFINED>';
matlabbatch{11}.spm.meeg.preproc.crop.timewin = '<UNDEFINED>';
matlabbatch{11}.spm.meeg.preproc.crop.freqwin = [4 80];
matlabbatch{11}.spm.meeg.preproc.crop.channels{1}.all = 'all';
matlabbatch{11}.spm.meeg.preproc.crop.prefix = 'c_';

matlabbatch{12}.spm.meeg.tf.avgfreq.D = '<UNDEFINED>';
matlabbatch{12}.spm.meeg.tf.avgfreq.freqwin = [4 8];
matlabbatch{12}.spm.meeg.tf.avgfreq.prefix = 'theta_';

matlabbatch{13}.spm.meeg.tf.avgfreq.D = '<UNDEFINED>';
matlabbatch{13}.spm.meeg.tf.avgfreq.freqwin = [9 12];
matlabbatch{13}.spm.meeg.tf.avgfreq.prefix = 'alpha_';

matlabbatch{14}.spm.meeg.tf.avgfreq.D = '<UNDEFINED>';
matlabbatch{14}.spm.meeg.tf.avgfreq.freqwin = [13 30];
matlabbatch{14}.spm.meeg.tf.avgfreq.prefix = 'beta_';

matlabbatch{15}.spm.meeg.tf.avgfreq.D = '<UNDEFINED>';
matlabbatch{15}.spm.meeg.tf.avgfreq.freqwin = [31 80];
matlabbatch{15}.spm.meeg.tf.avgfreq.prefix = 'gamma_';

matlabbatch{16}.spm.meeg.other.delete.D = '<UNDEFINED>';
matlabbatch{17}.spm.meeg.other.delete.D = '<UNDEFINED>';
matlabbatch{18}.spm.meeg.other.delete.D = '<UNDEFINED>';