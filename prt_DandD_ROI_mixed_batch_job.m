%-----------------------------------------------------------------------
% Job saved on 14-Apr-2020 10:37:29 by cfg_util (rev $Rev: 7345 $)
% spm SPM - SPM12 (7771)
% cfg_basicio BasicIO - Unknown
% prt PRoNTo - Unknown
%-----------------------------------------------------------------------        
global Manip Type Files generalImagesFolderName BandsSimple BandsPAC Channels subjects conditions_per_bins conditions_undivided BinsType PowerUnit

matlabbatch{1}.prt.data.dir_name = '<UNDEFINED>';

if(strcmp(BinsType,'order'))
    Bins = {'first','last'};
else %if(strcmp(BinsType,'errors'))
    Bins = {'large', 'small'};
end

disp("Creating PRT struct : ");
for c = 1:length(conditions_undivided)
    disp("	Cond : " + conditions_undivided{c});
    matlabbatch{1}.prt.data.group(c).gr_name = conditions_undivided{c};
    for i = 1:length(subjects)
        disp("		Subject : " + subjects{i});
        for file = 1:length(Files)
			if(contains(Files{file},'pac')), Bands = BandsPAC;
			else, Bands = BandsSimple; end
            for band = 1:length(Bands)
                for chan = 1:length(Channels)
                    matlabbatch{1}.prt.data.group(c).select.subject{i}(chan + length(Channels)*(band - 1) + length(Channels)*length(Bands)*(file - 1)).mod_name = strcat(Type,'_',BinsType,'_', Files{file},'_',Channels{chan},'_',Bands{band});
                    matlabbatch{1}.prt.data.group(c).select.subject{i}(chan + length(Channels)*(band - 1) + length(Channels)*length(Bands)*(file - 1)).TR = 2.5;
                    matlabbatch{1}.prt.data.group(c).select.subject{i}(chan + length(Channels)*(band - 1) + length(Channels)*length(Bands)*(file - 1)).scans = cellstr(strcat(generalImagesFolderName, Manip, '/',Type, '_', PowerUnit,'/', subjects{i},'/', Channels{chan},'/', Bands{band}, '_', Files{file}, '_',conditions_undivided{c},'.nii'));
                    matlabbatch{1}.prt.data.group(c).select.subject{i}(chan + length(Channels)*(band - 1) + length(Channels)*length(Bands)*(file - 1)).design.no_design = 0;
                end
            end
        end
    end
end
    
for j = 1:length(Bins)
    disp("Bins : " + Bins{j});
    for c = 1:length(conditions_per_bins)
        disp("	Cond : " + conditions_per_bins{c});
        matlabbatch{1}.prt.data.group(length(conditions_undivided) + c + (j-1)*length(conditions_per_bins)).gr_name = strcat(conditions_per_bins{c},'_',Bins{j});
        for i = 1:length(subjects)
            disp("		Subject : " + subjects{i});
            for file = 1:length(Files)
				if(contains(Files{file},'pac')), Bands = BandsPAC;
                else, Bands = BandsSimple; end
                for band = 1:length(Bands)
                    for chan = 1:length(Channels)
                        matlabbatch{1}.prt.data.group(length(conditions_undivided) + c + (j-1)*length(conditions_per_bins)).select.subject{i}(chan + length(Channels)*(band - 1) + length(Channels)*length(Bands)*(file - 1)).mod_name = strcat(Type,'_',BinsType,'_', Files{file},'_',Channels{chan},'_',Bands{band});
                        matlabbatch{1}.prt.data.group(length(conditions_undivided) + c + (j-1)*length(conditions_per_bins)).select.subject{i}(chan + length(Channels)*(band - 1) + length(Channels)*length(Bands)*(file - 1)).TR = 2.5;
                        matlabbatch{1}.prt.data.group(length(conditions_undivided) + c + (j-1)*length(conditions_per_bins)).select.subject{i}(chan + length(Channels)*(band - 1) + length(Channels)*length(Bands)*(file - 1)).scans = cellstr(strcat(generalImagesFolderName, Manip, '/',Type,'_', Bins{j},'_',PowerUnit, '/', subjects{i},'/', Channels{chan},'/', Bands{band}, '_', Files{file}, '_',conditions_per_bins{c},'.nii'));
                        matlabbatch{1}.prt.data.group(length(conditions_undivided) + c + (j-1)*length(conditions_per_bins)).select.subject{i}(chan + length(Channels)*(band - 1) + length(Channels)*length(Bands)*(file - 1)).design.no_design = 0;
                    end
                end
            end
        end
    end
end

disp("Masks");
for file = 1:length(Files)
	if(contains(Files{file},'pac')), Bands = BandsPAC;
    else, Bands = BandsSimple; end
    for band = 1:length(Bands)
        for chan = 1:length(Channels)
            matlabbatch{1}.prt.data.mask(chan + length(Channels)*(band - 1) + length(Channels)*length(Bands)*(file - 1)).mod_name =  strcat(Type,'_',BinsType,'_', Files{file},'_',Channels{chan},'_',Bands{band});
            if(strcmp(Files{file},'tpac'))
                matlabbatch{1}.prt.data.mask(chan + length(Channels)*(band - 1) + length(Channels)*length(Bands)*(file - 1)).fmask = cellstr(strcat(generalImagesFolderName, Manip, '/', Type,'_',PowerUnit, '/Mask_',Type,'_tpac.img'));
            elseif(strcmp(Files{file},'mvlpac'))
                matlabbatch{1}.prt.data.mask(chan + length(Channels)*(band - 1) + length(Channels)*length(Bands)*(file - 1)).fmask = cellstr(strcat(generalImagesFolderName, Manip, '/', Type,'_',PowerUnit, '/Mask_',Type,'_mvlpac.img'));
            elseif(strcmp(Files{file},'PCpac'))
                matlabbatch{1}.prt.data.mask(chan + length(Channels)*(band - 1) + length(Channels)*length(Bands)*(file - 1)).fmask = cellstr(strcat(generalImagesFolderName, Manip, '/', Type,'_',PowerUnit, '/Mask_',Type,'_PCpac.img'));
            else
                matlabbatch{1}.prt.data.mask(chan + length(Channels)*(band - 1) + length(Channels)*length(Bands)*(file - 1)).fmask = cellstr(strcat(generalImagesFolderName, Manip, '/', Type,'_',PowerUnit, '/Mask_',Type,'.img'));
            end
            matlabbatch{1}.prt.data.mask(chan + length(Channels)*(band - 1) + length(Channels)*length(Bands)*(file - 1)).hrfover = 0;
            matlabbatch{1}.prt.data.mask(chan + length(Channels)*(band - 1) + length(Channels)*length(Bands)*(file - 1)).hrfdel = 0;
        end
    end
end

matlabbatch{1}.prt.data.review = 0;

disp("Struct created.")
disp("Starting job...");
